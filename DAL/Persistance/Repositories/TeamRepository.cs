using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Domain.Repositories;
using DAL.Persistance.Context;
using DAL.Domain.Models;

namespace DAL.Persistance.Repositories
{
    public class TeamRepository : BaseRepository, IRepository<TeamModel>
    {
        public TeamRepository(AppDbContext context): base(context)
        {
            
        }
        public TeamModel Create(TeamModel data)
        {
            dbContext.Teams.Add(data);
            dbContext.SaveChanges();
            return data;
        }

        public TeamModel Delete(int id)
        {
            var deleteObj = dbContext.Teams.FirstOrDefault(p=>p.Id == id);
            dbContext.Teams.Remove(deleteObj);
            dbContext.SaveChanges();
            return deleteObj;
        }

        public TeamModel Read(int id)
        {
            return dbContext.Teams.FirstOrDefault(p=>p.Id == id);
        }

        public IList<TeamModel> ReadAll()
        {
            return dbContext.Teams.ToList();
        }

        public void Update(TeamModel data)
        {
            dbContext.Update(data);
            dbContext.SaveChanges();
        }
    }
}