using System;
using System.Collections.Generic;
using Common.DTO.User;

namespace Common.DTO.Team
{
    public class TeamDTO
    {
        public int Id { get; set; }

        public string Name { get; set; }
        public IList<UserDTO> Members { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
