using System;
using System.Collections.Generic;

namespace Common.DTO.Team
{
    public class CreateTeamDTO
    {
        public string Name { get; set; }
    }
}