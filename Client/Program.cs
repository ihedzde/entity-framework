﻿using System;
using Client.Services;
using System.Threading.Tasks;
using System.Collections.Generic;
using Client.Domain.Models;
using Client.DTOs;
using System.Text;
using Client.Domain.Interfaces;
using Client.Domain.Networking;

namespace Client
{
    class Program
    {
        static char PrintMenu()
        {
            var menu = "Choose action:\n"
            + "q - to quit.\n"
            + "0 - get tasks entities by id.\n"
            + "1 - get tasks less than 45 symbols long\n"
            + "2 - projects finished in 2021.\n"
            + "3 - teams with members older than 10 years sorted.\n"
            + "4 - users in alphabetic order with task by length sorted.\n"
            + "5 - complex select one.\n"
            + "6 - complex select two.\n"
            + "7 - show CRUD menu.";
            Console.WriteLine(menu);
            return Console.ReadLine()[0];
        }
        static string PrintCRUDMenu()
        {
            var menu = "Choose action:\n"
            + "q - to quit.\n"
            + "[create user] - Create User\n"
            + "[read users] - Read Users"
            + "[update user] - Update User\n"
            + "[delete user] - Delete User\n"
            + "[create team] - Create Team\n"
            + "[update team] - Update Team\n"
            + "[delete team] - Delete Team\n"
            + "[create project] - Create Project\n"
            + "[read projects] - Read Projects\n"
            + "[update project] - Update Project\n"
            + "[delete project] - Delete Project\n"
            + "[create task] - Create Task\n"
            + "[read tasks] - Read Tasks\n"
            + "[update task] - Update Task\n"
            + "[delete task] - Delete Task\n"
            ;
            Console.WriteLine(menu);
            return Console.ReadLine();
        }
        static int GetUserId()
        {
            Console.WriteLine("Input user id:");
            return int.Parse(Console.ReadLine());
        }
        static void PrintList<T>(IList<T> data)
        {
            foreach (var obj in data)
            {
                Console.WriteLine(obj);
            }
        }
        static Dictionary<string, Func<Task>> CRUDActions(CRUDService crudServices)
        {
            Dictionary<string, Func<Task>> dict = new Dictionary<string, Func<Task>>{
                {
                    "create user",  async () => Console.WriteLine((await crudServices.CreateUser()).ToString())
                },
                {
                    "update user", async() => await crudServices.UpdateUser()
                },
                {
                    "read users", async() => PrintList<UserModel>(await crudServices.ReadUser())
                },
                {
                    "delete user", async() => await crudServices.DeleteUser()
                },
                {
                    "create team", async() => Console.WriteLine(await crudServices.CreateTeam())
                },
                 {
                    "read teams", async() => PrintList<TeamModel>(await crudServices.ReadTeam())
                },
                {
                    "update team", async() => await crudServices.UpdateTeam()
                },
                {
                    "delete team", async() => await crudServices.DeleteTeam()
                },
                {
                    "create project", async() => Console.WriteLine(await crudServices.CreateProject())
                },
                {
                    "read projects", async() => PrintList<ProjectModel>(await crudServices.ReadProject())
                },
                {
                    "update project", async() => await crudServices.UpdateProject()
                },
                {
                    "delete project", async() => await crudServices.DeleteProject()
                },
                {
                    "create task",  async() => Console.WriteLine(await crudServices.CreateTask())
                },
                {
                    "read tasks", async() => PrintList<TaskModel>(await crudServices.ReadTask())
                },
                {
                    "update task", async() => await crudServices.UpdateTask()
                },
                {
                    "delete task", async() => await crudServices.DeleteTask()
                },
             };
            return dict;
        }
        static Dictionary<char, Func<Task>> DefineActions(SelectServices selectServices, CRUDService crudService)
        {
            Dictionary<char, Func<Task>> dict = new Dictionary<char, Func<Task>>{
                {
                    '0',  async ()=>
                    {
                        var id = GetUserId();
                        var result = await selectServices.GetTasksEntitiesByIdAsync(id);
                        foreach(var taskDict in result)
                        {
                            Console.WriteLine($"Project Name: {taskDict.Key}. Task Count: {taskDict.Value}");
                        }
                    }
                },
                {
                    '1',  async ()=>
                    {
                        var id = GetUserId();
                        var result = await selectServices.GetLessThan45Async(id);
                        PrintList<TaskModel>(result);
                    }
                },
                 {
                    '2',  async ()=>
                    {
                        var id = GetUserId();
                        var result = await selectServices.FinishedIn2021Async(id);
                        PrintList<IdAndName>(result);
                    }
                },
                 {
                    '3',  async ()=>
                    {
                        var result = await selectServices.TeamsOlder10SortedAsync();
                        foreach(var item in result){
                            Console.WriteLine();
                            PrintList<IdTeamNameMembers>(item);
                        }
                    }
                },
                 {
                    '4',  async ()=>
                    {
                        var result = await selectServices.UsersAlphabeticByTasksLength();
                        PrintList<IdUserAndTasks>(result);
                    }
                },
                {
                    '5',  async ()=>
                    {
                        var id = GetUserId();
                        try{
                            var result = await selectServices.ComplexUserEntity(id);
                            Console.WriteLine(result);
                        }catch(ArgumentException e){
                            Console.WriteLine(e.Message);
                        }

                    }
                },
                {
                    '6',  async ()=>
                    {
                        var result = await selectServices.OddlyProjectTaskUserCountMix();
                        PrintList<ProjectTaskUserCountMix>(result);
                    }
                },
                {
                    '7', async () =>
                    {
                        var crudChoice = PrintCRUDMenu();
                        var actions = CRUDActions(crudService);
                        while (crudChoice != "q")
                        {
                            await actions[crudChoice]();
                            crudChoice = PrintCRUDMenu();
                        }
                    }
                }
             };
            return dict;
        }
        static async Task Main(string[] args)
        {
            SelectServices selectServices = new SelectServices();
            CRUDService crudService = new CRUDService(
                new UserRepository(), new TaskRepository(), new ProjectRepository(),
                new TeamRepository());
            var choice = PrintMenu();
            var actions = DefineActions(selectServices, crudService);
            while (choice != 'q')
            {
                await actions[choice]();
                choice = PrintMenu();
            }
        }
    }
}
