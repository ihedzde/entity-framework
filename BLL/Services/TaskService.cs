using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.DTO.Task;
using Common.DTO.User;
using DAL.Domain.Models;
using DAL.Domain.Repositories;

namespace BLL.Services
{
    public class TaskService
    {
        private readonly IRepository<TaskModel> _taskRepo;
        private readonly IRepository<UserModel> _userRepo;
        private readonly IMapper _mapper;
        public TaskService(IMapper mapper, IRepository<TaskModel> taskRepo,
         IRepository<UserModel> userRepo)
        {
            _taskRepo = taskRepo;
            _userRepo = userRepo;
            _mapper = mapper;
        }
        private TaskDTO ModelToTaskDTO(TaskModel model)
        {
            var performer = _userRepo.ReadAll().FirstOrDefault(user => user.Id == model.PerformerId);
            return new TaskDTO
            {
                Id = model.Id,
                Name = model.Name,
                PerformerId = model.PerformerId,
                Performer = _mapper.Map<UserModel, UserDTO>(performer),
                ProjectId = model.ProjectId,
                Description = model.TLDR,
                State = (TaskState)model.State,
                FinishedAt = model.FinishedAt,
                CreatedAt = model.CreatedAt
            };
        }
        private TaskModel TaskDTOToModel(TaskDTO model)
        {
            return new TaskModel
            {
                Id = model.Id,
                Name = model.Name,
                ProjectId = model.ProjectId,
                PerformerId = model.PerformerId,
                TLDR = model.Description,
                State = (short)model.State,
                FinishedAt = model.FinishedAt,
                CreatedAt = model.CreatedAt
            };
        }
        private TaskDTO CreateModelToTaskDTO(CreateTaskDTO model) =>
        new TaskDTO
        {
            Name = model.Name,
            PerformerId = model.PerformerId,
            ProjectId = model.ProjectId,
            Description = model.Description,
            State = model.State,
            CreatedAt = DateTime.Now
        };

        public IEnumerable<TaskDTO> GetAllTasks()
        {
            return _taskRepo.ReadAll().Select(task => ModelToTaskDTO(task));
        }
        public TaskDTO CreateTask(CreateTaskDTO createTaskDTO)
        {
            return ModelToTaskDTO(_taskRepo.Create(TaskDTOToModel(CreateModelToTaskDTO(createTaskDTO))));
        }
        public TaskDTO GetById(int id){
            return ModelToTaskDTO(_taskRepo.Read(id));
        }
        public void UpdateTask(TaskDTO updateDto)
        {
            _taskRepo.Update(TaskDTOToModel(updateDto));
        }
        public void DeleteTask(int id)
        {
            _taskRepo.Delete(id);
        }
    }
}