using System;
using System.Collections.Generic;
using System.Linq;
using DAL.Domain.Repositories;
using DAL.Persistance.Context;
using DAL.Domain.Models;

namespace DAL.Persistance.Repositories
{
    public class ProjectRepository : BaseRepository, IRepository<ProjectModel>
    {
        public ProjectRepository(AppDbContext dbContext): base(dbContext)
        {
        }

        public ProjectModel Create(ProjectModel data)
        {
            dbContext.Projects.Add(data);
            dbContext.SaveChanges();
            return data;
        }

        public ProjectModel Delete(int id)
        {
            var deleteObj = dbContext.Projects.FirstOrDefault(p => p.Id == id);
            dbContext.Projects.Remove(deleteObj);
            dbContext.SaveChanges();
            return deleteObj;
        }

        public ProjectModel Read(int id)
        {
            return dbContext.Projects.FirstOrDefault(p => p.Id == id);
        }

        public IList<ProjectModel> ReadAll()
        {
            return dbContext.Projects.ToList();
        }

        public void Update(ProjectModel data)
        {
            dbContext.Projects.Update(data);
            dbContext.SaveChanges();
        }
    }
}