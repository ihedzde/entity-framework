using System;
using System.Linq;
using System.Collections.Generic;
using DAL.Domain.Repositories;
using DAL.Persistance.Context;
using DAL.Domain.Models;

namespace DAL.Persistance.Repositories
{
    public class TaskRepository : BaseRepository, IRepository<TaskModel>
    {
        public TaskRepository(AppDbContext context) : base(context)
        {

        }
        public TaskModel Create(TaskModel data)
        {
            dbContext.Tasks.Add(data);
            dbContext.SaveChanges();
            return data;
        }

        public TaskModel Delete(int id)
        {
            var deleteObj = dbContext.Tasks.FirstOrDefault(p => p.Id == id);
            dbContext.Tasks.Remove(deleteObj);
            dbContext.SaveChanges();
            return deleteObj;
        }

        public TaskModel Read(int id)
        {
            return dbContext.Tasks.FirstOrDefault(p => p.Id == id);
        }

        public IList<TaskModel> ReadAll()
        {
            return dbContext.Tasks.ToList();
        }

        public void Update(TaskModel data)
        {
            dbContext.Update(data);
            dbContext.SaveChanges();
        }
    }
}