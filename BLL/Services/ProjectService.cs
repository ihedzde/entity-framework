using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.DTO.Project;
using Common.DTO.User;
using DAL.Domain.Repositories;
using DAL.Domain.Models;

namespace BLL.Services
{
    public class ProjectService
    {
        private readonly IRepository<ProjectModel> _projectRepo;
        private readonly IRepository<UserModel> _userRepo;
        private readonly TaskService _taskService;
        private readonly TeamService _teamService;
        private readonly IMapper _mapper;
        public ProjectService(IMapper mapper, IRepository<ProjectModel> projectRepo, IRepository<UserModel> userRepo,
         TaskService taskService, TeamService teamService)
        {
            _mapper = mapper;
            _projectRepo = projectRepo;
            _userRepo = userRepo;
            _taskService = taskService;
            _teamService = teamService;
        }
         private ProjectDTO ModelToProjectDTO(ProjectModel model)
        {
            return new ProjectDTO
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                AuthorId = model.AuthorId,
                TeamId = model.TeamId,
                Author = _mapper.Map<UserDTO>(_userRepo.Read(model.AuthorId)),
                Team = _teamService.GetById(model.TeamId),
                Tasks = _taskService.GetAllTasks().Where(task=>task.PerformerId == model.Id),
                CreatedAt = model.CreatedAt,
                Deadline = model.Deadline,
            };
        }
        private ProjectModel ProjectDTOToModel(ProjectDTO model)
        {
            return new ProjectModel
            {
                Id = model.Id,
                Name = model.Name,
                Description = model.Description,
                AuthorId = model.AuthorId,
                TeamId = model.TeamId,
                CreatedAt = model.CreatedAt,
                Deadline = model.Deadline
            };
        }
        private ProjectDTO CreateModelToProjectDTO(CreateProjectDTO model) =>
        new ProjectDTO
        {
            Name = model.Name,
            Description = model.Description,
            AuthorId = model.AuthorId,
            TeamId = model.TeamId,
            Author = _mapper.Map<UserModel, UserDTO>(_userRepo.Read(model.AuthorId)),
            Team = _teamService.GetById(model.TeamId),
            CreatedAt = DateTime.Now,
            Deadline = model.Deadline,
        };

        public IEnumerable<ProjectDTO> GetAllProjects()
        {
            return _projectRepo.ReadAll().Select(task => ModelToProjectDTO(task));
        }
        public ProjectDTO CreateProject(CreateProjectDTO createProjectDTO)
        {
            return ModelToProjectDTO(_projectRepo.Create(ProjectDTOToModel(CreateModelToProjectDTO(createProjectDTO))));
        }
        public ProjectDTO GetById(int id){
            return ModelToProjectDTO(_projectRepo.Read(id));
        }
        public void UpdateProject(ProjectDTO updateDto)
        {
            _projectRepo.Update(ProjectDTOToModel(updateDto));
        }
        public void DeleteProject(int id)
        {
            _projectRepo.Delete(id);
        }

    }
}