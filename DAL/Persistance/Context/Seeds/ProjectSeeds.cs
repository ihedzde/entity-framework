using System;
using System.Collections.Generic;
using DAL.Domain.Models;

namespace DAL.Context.Seeds
{
    public static class ProjectSeeds
    {
        #region Seeds
       public static IList<ProjectModel> Projects { get; set; } = new List<ProjectModel>{
        new ProjectModel{
            Id = 1,
            AuthorId = 22,
            TeamId = 1,
            Name = "backing up Handcrafted Fresh Shoes challenge",
            Description = "Et doloribus et temporibus.",
            Deadline = DateTime.Parse("2021-09-12T19:17:47.2335223+00:00"),
            CreatedAt = DateTime.Parse("2020-08-25T17:49:50.4518054+00:00")
        },
        new ProjectModel{
            Id = 2,
            AuthorId = 31,
            TeamId = 1,
            Name = "Village",
            Description = "Non voluptatem voluptas libero.",
            Deadline = DateTime.Parse("2021-07-24T12:07:31.9357846+00:00"),
            CreatedAt = DateTime.Parse("2021-01-30T14:38:53.8838745+00:00")
        },
        new ProjectModel{
            Id = 3,
            AuthorId = 31,
            TeamId = 1,
            Name = "Dam",
            Description = "Quia et tempora hic pariatur voluptatem doloribus sunt.",
            Deadline = DateTime.Parse("2021-07-21T08:32:51.3354654+00:00"),
            CreatedAt = DateTime.Parse("2020-03-15T20:33:15.6731141+00:00")
        },
        new ProjectModel{
            Id = 4,
            AuthorId = 29,
            TeamId = 1,
            Name = "iterate project",
            Description = "Soluta non sed assumenda.",
            Deadline = DateTime.Parse("2021-06-25T11:25:00.7111264+00:00"),
            CreatedAt = DateTime.Parse("2019-07-03T03:39:01.9978679+00:00")
        },
        new ProjectModel{
            Id = 5,
            AuthorId = 29,
            TeamId = 1,
            Name = "Libyan Dinar Netherlands Antilles",
            Description = "Voluptatibus error ut id libero quam natus molestias natus.",
            Deadline = DateTime.Parse("2021-09-14T02:53:20.8003038+00:00"),
            CreatedAt = DateTime.Parse("2021-03-15T19:47:35.9335401+00:00")
        },
        new ProjectModel{
            Id = 6,
            AuthorId = 25,
            TeamId = 1,
            Name = "New Jersey capacitor program",
            Description = "Quas fuga qui eaque et corporis.",
            Deadline = DateTime.Parse("2021-09-23T03:11:42.1994133+00:00"),
            CreatedAt = DateTime.Parse("2020-03-22T16:51:40.1965166+00:00")
        },
        new ProjectModel{
            Id = 7,
            AuthorId = 25,
            TeamId = 1,
            Name = "connect",
            Description = "Voluptate et quae error est ut.",
            Deadline = DateTime.Parse("2021-11-29T21:13:16.9692138+00:00"),
            CreatedAt = DateTime.Parse("2021-01-03T03:55:46.0129331+00:00")
        },
        new ProjectModel{
            Id = 8,
            AuthorId = 31,
            TeamId = 1,
            Name = "matrix",
            Description = "Incidunt quam consequatur eos maxime qui.",
            Deadline = DateTime.Parse("2021-10-15T21:14:10.4493204+00:00"),
            CreatedAt = DateTime.Parse("2021-01-09T06:15:17.9611807+00:00")
        },
        new ProjectModel{
            Id = 9,
            AuthorId = 24,
            TeamId = 1,
            Name = "Unbranded Soft Pants Chief",
            Description = "Ea optio et et.",
            Deadline = DateTime.Parse("2021-08-30T02:27:10.3776173+00:00"),
            CreatedAt = DateTime.Parse("2019-08-25T12:59:27.7949549+00:00")
        },
        new ProjectModel{
            Id = 10,
            AuthorId = 28,
            TeamId = 1,
            Name = "Sleek Frozen Mouse",
            Description = "Molestias et inventore totam architecto explicabo dolorum et esse.",
            Deadline = DateTime.Parse("2021-06-29T12:43:45.0381411+00:00"),
            CreatedAt = DateTime.Parse("2019-10-17T07:00:17.8452998+00:00")
        },
        new ProjectModel{
            Id = 11,
            AuthorId = 38,
            TeamId = 1,
            Name = "THX",
            Description = "Officiis maiores exercitationem.",
            Deadline = DateTime.Parse("2021-10-03T14:57:28.9774796+00:00"),
            CreatedAt = DateTime.Parse("2020-01-16T07:05:05.69777+00:00")
        },
        new ProjectModel{
            Id = 12,
            AuthorId = 36,
            TeamId = 1,
            Name = "Cambridgeshire",
            Description = "Praesentium est similique velit libero inventore totam.",
            Deadline = DateTime.Parse("2021-11-22T00:45:47.2056714+00:00"),
            CreatedAt = DateTime.Parse("2020-06-09T17:10:47.9077465+00:00")
        },
        new ProjectModel{
            Id = 13,
            AuthorId = 49,
            TeamId = 1,
            Name = "protocol",
            Description = "Est doloribus expedita vel distinctio.",
            Deadline = DateTime.Parse("2021-08-02T02:31:39.3283064+00:00"),
            CreatedAt = DateTime.Parse("2020-03-29T20:50:09.0468406+00:00")
        },
        new ProjectModel{
            Id = 14,
            AuthorId = 46,
            TeamId = 1,
            Name = "Metal Facilitator",
            Description = "Autem id qui ut.",
            Deadline = DateTime.Parse("2021-08-02T09:18:48.6612819+00:00"),
            CreatedAt = DateTime.Parse("2020-04-19T05:51:04.8726359+00:00")
        },
        new ProjectModel{
            Id = 15,
            AuthorId = 38,
            TeamId = 1,
            Name = "transmitting Intelligent Metal Table unleash",
            Description = "Qui quia reiciendis molestiae est reprehenderit sint fugiat dolorum.",
            Deadline = DateTime.Parse("2021-11-30T03:13:22.9359388+00:00"),
            CreatedAt = DateTime.Parse("2019-11-22T13:41:23.7300052+00:00")
        },
        new ProjectModel{
            Id = 16,
            AuthorId = 55,
            TeamId = 2,
            Name = "Cuban Peso 6th generation",
            Description = "Ratione corporis inventore modi nesciunt quo qui illum rerum voluptatum.",
            Deadline = DateTime.Parse("2021-09-27T02:47:45.2074038+00:00"),
            CreatedAt = DateTime.Parse("2021-02-21T06:05:06.1524385+00:00")
        },
        new ProjectModel{
            Id = 17,
            AuthorId = 54,
            TeamId = 2,
            Name = "optimize solid state actuating",
            Description = "Non dolores id eligendi.",
            Deadline = DateTime.Parse("2021-08-27T23:42:54.2199855+00:00"),
            CreatedAt = DateTime.Parse("2020-02-27T14:41:57.8777711+00:00")
        },
        new ProjectModel{
            Id = 18,
            AuthorId = 55,
            TeamId = 2,
            Name = "Avon",
            Description = "Reprehenderit qui et praesentium cupiditate hic.",
            Deadline = DateTime.Parse("2021-11-17T00:36:46.8000523+00:00"),
            CreatedAt = DateTime.Parse("2021-04-18T08:47:30.8216119+00:00")
        },
        new ProjectModel{
            Id = 19,
            AuthorId = 65,
            TeamId = 3,
            Name = "revolutionary sensor integrate",
            Description = "Et nostrum aut doloribus optio recusandae.",
            Deadline = DateTime.Parse("2021-07-22T11:08:53.8455228+00:00"),
            CreatedAt = DateTime.Parse("2020-07-26T02:26:08.0088803+00:00")
        },
        new ProjectModel{
            Id = 20,
            AuthorId = 65,
            TeamId = 3,
            Name = "lime Cameroon Avon",
            Description = "Vero neque et perspiciatis eos architecto adipisci deleniti rerum.",
            Deadline = DateTime.Parse("2021-08-21T21:47:02.109966+00:00"),
            CreatedAt = DateTime.Parse("2019-12-01T00:15:00.670585+00:00")
        },
        new ProjectModel{
            Id = 21,
            AuthorId = 65,
            TeamId = 3,
            Name = "SQL revolutionize Credit Card Account",
            Description = "Ea facere soluta nostrum architecto quam.",
            Deadline = DateTime.Parse("2021-06-28T06:48:42.4297028+00:00"),
            CreatedAt = DateTime.Parse("2019-08-27T14:50:36.1312271+00:00")
        },
        new ProjectModel{
            Id = 22,
            AuthorId = 67,
            TeamId = 3,
            Name = "circuit",
            Description = "Tenetur et non consequuntur molestiae alias numquam omnis molestias inventore.",
            Deadline = DateTime.Parse("2021-10-07T20:01:12.6515298+00:00"),
            CreatedAt = DateTime.Parse("2020-09-11T13:33:10.5941776+00:00")
        },
        new ProjectModel{
            Id = 23,
            AuthorId = 67,
            TeamId = 3,
            Name = "Human cross-media",
            Description = "Assumenda facilis quis aut deleniti et rem.",
            Deadline = DateTime.Parse("2021-09-10T00:35:14.15384+00:00"),
            CreatedAt = DateTime.Parse("2021-05-23T06:30:39.1836259+00:00")
        },
        new ProjectModel{
            Id = 24,
            AuthorId = 63,
            TeamId = 3,
            Name = "Plain Global",
            Description = "Aliquam quasi dolore.",
            Deadline = DateTime.Parse("2021-11-30T18:26:25.8983258+00:00"),
            CreatedAt = DateTime.Parse("2020-03-24T11:40:48.3948371+00:00")
        },
        new ProjectModel{
            Id = 25,
            AuthorId = 64,
            TeamId = 3,
            Name = "SMS Unbranded Fresh",
            Description = "Perferendis molestias ex sed.",
            Deadline = DateTime.Parse("2021-07-12T07:18:06.848387+00:00"),
            CreatedAt = DateTime.Parse("2020-10-21T20:23:08.0273707+00:00")
        },
        new ProjectModel{
            Id = 26,
            AuthorId = 65,
            TeamId = 3,
            Name = "sensor Park",
            Description = "Quos rerum rem maiores.",
            Deadline = DateTime.Parse("2021-09-18T06:06:50.7148434+00:00"),
            CreatedAt = DateTime.Parse("2020-09-12T10:40:13.0643475+00:00")
        },
        new ProjectModel{
            Id = 27,
            AuthorId = 65,
            TeamId = 3,
            Name = "bypassing solid state",
            Description = "Sapiente deleniti voluptate qui est.",
            Deadline = DateTime.Parse("2021-08-21T01:17:40.0455707+00:00"),
            CreatedAt = DateTime.Parse("2020-04-20T10:36:41.3518888+00:00")
        },
        new ProjectModel{
            Id = 28,
            AuthorId = 67,
            TeamId = 3,
            Name = "Concrete",
            Description = "Temporibus non molestiae qui laboriosam placeat.",
            Deadline = DateTime.Parse("2021-09-10T03:07:42.5653935+00:00"),
            CreatedAt = DateTime.Parse("2021-05-08T12:36:00.2389615+00:00")
        },
        new ProjectModel{
            Id = 29,
            AuthorId = 63,
            TeamId = 3,
            Name = "Music",
            Description = "Facere nemo atque amet in iste.",
            Deadline = DateTime.Parse("2021-08-15T19:36:32.6649445+00:00"),
            CreatedAt = DateTime.Parse("2019-10-20T12:08:31.8732982+00:00")
        },
        new ProjectModel{
            Id = 30,
            AuthorId = 64,
            TeamId = 3,
            Name = "Plastic",
            Description = "Quia aut voluptatem eius est et culpa cumque quia.",
            Deadline = DateTime.Parse("2021-07-14T15:01:23.1195733+00:00"),
            CreatedAt = DateTime.Parse("2019-08-23T13:07:35.5461833+00:00")
        },
        new ProjectModel{
            Id = 31,
            AuthorId = 71,
            TeamId = 4,
            Name = "parsing connect",
            Description = "Ullam enim in minima.",
            Deadline = DateTime.Parse("2021-09-06T19:47:13.1332692+00:00"),
            CreatedAt = DateTime.Parse("2021-04-20T10:22:21.5842329+00:00")
        },
        new ProjectModel{
            Id = 32,
            AuthorId = 74,
            TeamId = 4,
            Name = "Tasty copying Checking Account",
            Description = "Voluptatem dicta illo ut ut reiciendis sint porro in sit.",
            Deadline = DateTime.Parse("2021-10-03T02:06:35.3996098+00:00"),
            CreatedAt = DateTime.Parse("2020-01-26T16:39:03.6478616+00:00")
        },
        new ProjectModel{
            Id = 33,
            AuthorId = 68,
            TeamId = 4,
            Name = "Trail",
            Description = "Harum non aperiam praesentium perspiciatis optio voluptas.",
            Deadline = DateTime.Parse("2021-07-16T07:27:46.844206+00:00"),
            CreatedAt = DateTime.Parse("2021-05-12T00:58:10.841638+00:00")
        },
        new ProjectModel{
            Id = 34,
            AuthorId = 73,
            TeamId = 4,
            Name = "array virtual",
            Description = "Vel eaque facere et et labore.",
            Deadline = DateTime.Parse("2021-07-30T10:58:06.1055667+00:00"),
            CreatedAt = DateTime.Parse("2019-11-28T01:21:45.8521989+00:00")
        },
        new ProjectModel{
            Id = 35,
            AuthorId = 73,
            TeamId = 4,
            Name = "Refined whiteboard Borders",
            Description = "Dolore est nobis.",
            Deadline = DateTime.Parse("2021-12-14T10:10:42.8476515+00:00"),
            CreatedAt = DateTime.Parse("2020-06-09T16:43:03.8964789+00:00")
        },
        new ProjectModel{
            Id = 36,
            AuthorId = 74,
            TeamId = 4,
            Name = "gold Fantastic Steel Chips",
            Description = "Vel enim modi libero.",
            Deadline = DateTime.Parse("2021-07-21T02:58:43.7487443+00:00"),
            CreatedAt = DateTime.Parse("2020-01-20T22:22:20.8769856+00:00")
        },
        new ProjectModel{
            Id = 37,
            AuthorId = 76,
            TeamId = 4,
            Name = "mindshare systemic New Jersey",
            Description = "Placeat iure sunt quo libero sunt aut et molestias eos.",
            Deadline = DateTime.Parse("2021-08-08T08:45:49.5475316+00:00"),
            CreatedAt = DateTime.Parse("2020-09-29T17:04:16.0958935+00:00")
        },
        new ProjectModel{
            Id = 38,
            AuthorId = 74,
            TeamId = 4,
            Name = "Licensed Soft Soap synthesize Divide",
            Description = "Voluptatem recusandae aut.",
            Deadline = DateTime.Parse("2021-09-11T20:37:07.9353832+00:00"),
            CreatedAt = DateTime.Parse("2020-11-18T12:07:46.1917041+00:00")
        },
        new ProjectModel{
            Id = 39,
            AuthorId = 76,
            TeamId = 4,
            Name = "deposit open-source Licensed",
            Description = "Sed corporis sequi enim et.",
            Deadline = DateTime.Parse("2021-10-29T06:39:46.801621+00:00"),
            CreatedAt = DateTime.Parse("2019-11-10T15:49:22.9841897+00:00")
        },
        new ProjectModel{
            Id = 40,
            AuthorId = 76,
            TeamId = 4,
            Name = "solution-oriented ubiquitous",
            Description = "Dolore temporibus aut in qui eum assumenda.",
            Deadline = DateTime.Parse("2021-12-02T01:06:39.1104838+00:00"),
            CreatedAt = DateTime.Parse("2020-05-02T13:48:57.751252+00:00")
        },
        new ProjectModel{
            Id = 41,
            AuthorId = 78,
            TeamId = 5,
            Name = "override Health & Outdoors",
            Description = "Deleniti ut voluptatem id ducimus.",
            Deadline = DateTime.Parse("2021-12-06T22:21:02.291536+00:00"),
            CreatedAt = DateTime.Parse("2020-07-31T05:30:37.838524+00:00")
        },
        new ProjectModel{
            Id = 42,
            AuthorId = 77,
            TeamId = 5,
            Name = "Stravenue Personal Loan Account",
            Description = "Numquam in odio.",
            Deadline = DateTime.Parse("2021-12-03T14:44:13.4923418+00:00"),
            CreatedAt = DateTime.Parse("2019-10-28T10:19:59.0235124+00:00")
        },
        new ProjectModel{
            Id = 43,
            AuthorId = 78,
            TeamId = 5,
            Name = "Kansas firewall",
            Description = "Aut non deserunt et est illum.",
            Deadline = DateTime.Parse("2021-12-10T19:11:13.5724739+00:00"),
            CreatedAt = DateTime.Parse("2021-05-06T11:53:00.6882002+00:00")
        },
        new ProjectModel{
            Id = 44,
            AuthorId = 77,
            TeamId = 5,
            Name = "Dynamic Loop Concrete",
            Description = "Voluptatem placeat perferendis at deserunt.",
            Deadline = DateTime.Parse("2021-10-03T08:31:46.8908489+00:00"),
            CreatedAt = DateTime.Parse("2019-11-07T02:20:52.3280647+00:00")
        },
        new ProjectModel{
            Id = 45,
            AuthorId = 78,
            TeamId = 5,
            Name = "Money Market Account JSON",
            Description = "Officia dicta asperiores et.",
            Deadline = DateTime.Parse("2021-09-23T02:46:08.360406+00:00"),
            CreatedAt = DateTime.Parse("2020-04-09T00:43:00.0787715+00:00")
        },
        new ProjectModel{
            Id = 46,
            AuthorId = 78,
            TeamId = 5,
            Name = "Outdoors, Kids & Computers Bedfordshire Berkshire",
            Description = "Numquam sed voluptatibus voluptatibus quas.",
            Deadline = DateTime.Parse("2021-10-08T02:07:40.9504841+00:00"),
            CreatedAt = DateTime.Parse("2020-08-21T05:02:20.3375031+00:00")
        },
        new ProjectModel{
            Id = 47,
            AuthorId = 77,
            TeamId = 5,
            Name = "background Awesome Concrete Hat array",
            Description = "Iusto unde fuga.",
            Deadline = DateTime.Parse("2021-10-06T11:58:37.4008817+00:00"),
            CreatedAt = DateTime.Parse("2019-11-18T07:38:49.4159192+00:00")
        },
        new ProjectModel{
            Id = 48,
            AuthorId = 78,
            TeamId = 5,
            Name = "compelling Director virtual",
            Description = "Quis consequatur est qui.",
            Deadline = DateTime.Parse("2021-08-07T07:30:51.1850889+00:00"),
            CreatedAt = DateTime.Parse("2020-04-06T12:08:44.8584885+00:00")
        },
        new ProjectModel{
            Id = 49,
            AuthorId = 78,
            TeamId = 5,
            Name = "Cape Verde virtual",
            Description = "Dolorem sunt optio sed excepturi dolores sequi ipsa.",
            Deadline = DateTime.Parse("2021-11-14T12:32:00.4158865+00:00"),
            CreatedAt = DateTime.Parse("2019-12-29T19:48:06.8822589+00:00")
        },
        new ProjectModel{
            Id = 50,
            AuthorId = 78,
            TeamId = 5,
            Name = "Bedfordshire Generic Cotton Chair",
            Description = "Recusandae itaque necessitatibus sunt unde est.",
            Deadline = DateTime.Parse("2021-09-16T01:51:20.392354+00:00"),
            CreatedAt = DateTime.Parse("2020-01-02T00:56:00.9687162+00:00")
        },
        new ProjectModel{
            Id = 51,
            AuthorId = 78,
            TeamId = 5,
            Name = "Practical Soft Hat",
            Description = "Et dolore quaerat pariatur illum sed amet dolores deleniti.",
            Deadline = DateTime.Parse("2021-08-09T15:14:25.0180162+00:00"),
            CreatedAt = DateTime.Parse("2021-06-04T00:57:33.9342261+00:00")
        },
        new ProjectModel{
            Id = 52,
            AuthorId = 77,
            TeamId = 5,
            Name = "strategize bypass Cotton",
            Description = "Possimus itaque occaecati ratione nisi.",
            Deadline = DateTime.Parse("2021-06-28T21:27:44.9469928+00:00"),
            CreatedAt = DateTime.Parse("2020-09-03T04:32:37.7820963+00:00")
        },
        new ProjectModel{
            Id = 53,
            AuthorId = 78,
            TeamId = 5,
            Name = "compress",
            Description = "Rerum nam ipsam repudiandae vel animi numquam et placeat.",
            Deadline = DateTime.Parse("2021-11-13T22:44:10.2230396+00:00"),
            CreatedAt = DateTime.Parse("2020-08-27T06:52:57.1003543+00:00")
        },
        new ProjectModel{
            Id = 54,
            AuthorId = 81,
            TeamId = 6,
            Name = "Bhutanese Ngultrum Gorgeous",
            Description = "Nisi qui quia inventore maiores qui aut quas dolor et.",
            Deadline = DateTime.Parse("2021-09-28T21:50:08.4696444+00:00"),
            CreatedAt = DateTime.Parse("2021-03-16T17:32:02.1857021+00:00")
        },
        new ProjectModel{
            Id = 55,
            AuthorId = 82,
            TeamId = 6,
            Name = "Practical Wooden Car Haiti Unions",
            Description = "Mollitia tenetur non numquam.",
            Deadline = DateTime.Parse("2021-10-12T16:58:34.9770054+00:00"),
            CreatedAt = DateTime.Parse("2019-09-08T01:47:23.7032528+00:00")
        },
        new ProjectModel{
            Id = 56,
            AuthorId = 80,
            TeamId = 6,
            Name = "Oklahoma",
            Description = "Non blanditiis quo est sit amet enim explicabo iusto.",
            Deadline = DateTime.Parse("2021-09-09T13:06:48.0394059+00:00"),
            CreatedAt = DateTime.Parse("2021-01-26T19:43:20.5975913+00:00")
        },
        new ProjectModel{
            Id = 57,
            AuthorId = 82,
            TeamId = 6,
            Name = "forecast Home & Toys Global",
            Description = "Ut culpa aut blanditiis sint corrupti.",
            Deadline = DateTime.Parse("2021-10-10T12:44:09.2885111+00:00"),
            CreatedAt = DateTime.Parse("2020-08-03T22:47:08.4585349+00:00")
        },
        new ProjectModel{
            Id = 58,
            AuthorId = 82,
            TeamId = 6,
            Name = "web services Handmade Metal Hat card",
            Description = "Commodi voluptatum eligendi.",
            Deadline = DateTime.Parse("2021-09-01T12:51:00.1651103+00:00"),
            CreatedAt = DateTime.Parse("2020-11-27T12:26:18.2547011+00:00")
        },
        new ProjectModel{
            Id = 59,
            AuthorId = 83,
            TeamId = 6,
            Name = "system-worthy input Data",
            Description = "Et consectetur dolores quas.",
            Deadline = DateTime.Parse("2021-11-26T13:32:57.4526908+00:00"),
            CreatedAt = DateTime.Parse("2020-02-18T03:51:08.6368736+00:00")
        },
        new ProjectModel{
            Id = 60,
            AuthorId = 92,
            TeamId = 7,
            Name = "deposit radical Proactive",
            Description = "Architecto qui ut aut neque.",
            Deadline = DateTime.Parse("2021-12-10T21:33:22.144638+00:00"),
            CreatedAt = DateTime.Parse("2020-04-28T02:15:52.9595246+00:00")
        },
        new ProjectModel{
            Id = 61,
            AuthorId = 87,
            TeamId = 7,
            Name = "Home Loan Account Re-engineered Australian Dollar",
            Description = "Laborum est similique.",
            Deadline = DateTime.Parse("2021-09-24T13:40:02.6100459+00:00"),
            CreatedAt = DateTime.Parse("2020-04-18T05:46:30.8905128+00:00")
        },
        new ProjectModel{
            Id = 62,
            AuthorId = 84,
            TeamId = 7,
            Name = "withdrawal XML Books",
            Description = "Nam harum officia.",
            Deadline = DateTime.Parse("2021-10-22T23:37:45.3334507+00:00"),
            CreatedAt = DateTime.Parse("2021-03-12T18:41:07.0705771+00:00")
        },
        new ProjectModel{
            Id = 63,
            AuthorId = 96,
            TeamId = 7,
            Name = "Awesome Personal Loan Account Ethiopian Birr",
            Description = "Error placeat nemo.",
            Deadline = DateTime.Parse("2021-10-13T00:17:35.1531305+00:00"),
            CreatedAt = DateTime.Parse("2021-02-23T09:54:04.107478+00:00")
        },
        new ProjectModel{
            Id = 64,
            AuthorId = 95,
            TeamId = 7,
            Name = "deposit",
            Description = "Quasi minus et atque minima totam iste doloribus.",
            Deadline = DateTime.Parse("2021-11-11T16:14:45.28131+00:00"),
            CreatedAt = DateTime.Parse("2020-05-23T11:36:23.9187435+00:00")
        },
        new ProjectModel{
            Id = 65,
            AuthorId = 101,
            TeamId = 7,
            Name = "Small Fresh Pants Home Loan Account",
            Description = "Et quidem quia.",
            Deadline = DateTime.Parse("2021-10-08T11:16:55.9369968+00:00"),
            CreatedAt = DateTime.Parse("2020-04-17T13:07:39.1858192+00:00")
        },
        new ProjectModel{
            Id = 66,
            AuthorId = 97,
            TeamId = 7,
            Name = "Fantastic Steel Shirt",
            Description = "Asperiores sint rerum.",
            Deadline = DateTime.Parse("2021-10-10T03:03:02.2721778+00:00"),
            CreatedAt = DateTime.Parse("2020-08-28T22:03:46.2782746+00:00")
        },
        new ProjectModel{
            Id = 67,
            AuthorId = 85,
            TeamId = 7,
            Name = "primary Tasty Fresh Fish",
            Description = "Voluptatibus id corporis.",
            Deadline = DateTime.Parse("2021-10-29T21:13:56.2944597+00:00"),
            CreatedAt = DateTime.Parse("2019-10-30T13:42:19.4328527+00:00")
        },
        new ProjectModel{
            Id = 68,
            AuthorId = 84,
            TeamId = 7,
            Name = "deposit Handmade Wooden Chair",
            Description = "Qui nostrum nesciunt a in cupiditate hic quasi.",
            Deadline = DateTime.Parse("2021-08-07T22:37:48.7163495+00:00"),
            CreatedAt = DateTime.Parse("2020-09-07T11:44:25.5610007+00:00")
        },
        new ProjectModel{
            Id = 69,
            AuthorId = 92,
            TeamId = 7,
            Name = "Licensed",
            Description = "Nulla tempore molestiae necessitatibus voluptatum omnis provident accusamus cum non.",
            Deadline = DateTime.Parse("2021-08-26T07:02:13.2279163+00:00"),
            CreatedAt = DateTime.Parse("2020-05-14T08:24:50.3065639+00:00")
        },
        new ProjectModel{
            Id = 70,
            AuthorId = 90,
            TeamId = 7,
            Name = "optical",
            Description = "Illo temporibus nam repellendus ut.",
            Deadline = DateTime.Parse("2021-06-23T15:53:06.4660717+00:00"),
            CreatedAt = DateTime.Parse("2020-06-28T14:01:02.3053978+00:00")
        },
        new ProjectModel{
            Id = 71,
            AuthorId = 87,
            TeamId = 7,
            Name = "National",
            Description = "Eius error recusandae libero cumque commodi reprehenderit voluptatem et omnis.",
            Deadline = DateTime.Parse("2021-12-10T16:01:24.4601643+00:00"),
            CreatedAt = DateTime.Parse("2020-03-31T22:44:05.8525593+00:00")
        },
        new ProjectModel{
            Id = 72,
            AuthorId = 91,
            TeamId = 7,
            Name = "driver Orchestrator",
            Description = "Odio sed modi sed nemo magni neque rerum nam.",
            Deadline = DateTime.Parse("2021-12-08T08:16:37.4182642+00:00"),
            CreatedAt = DateTime.Parse("2020-01-26T17:06:00.345533+00:00")
        },
        new ProjectModel{
            Id = 73,
            AuthorId = 89,
            TeamId = 7,
            Name = "lavender Port e-business",
            Description = "Rerum molestiae ut aperiam quas quo ab commodi.",
            Deadline = DateTime.Parse("2021-09-05T04:47:57.7733646+00:00"),
            CreatedAt = DateTime.Parse("2020-12-13T15:45:34.1257351+00:00")
        },
        new ProjectModel{
            Id = 74,
            AuthorId = 92,
            TeamId = 7,
            Name = "Delaware",
            Description = "Autem repellat ea fuga ut ut esse est eum ut.",
            Deadline = DateTime.Parse("2021-08-04T16:24:13.6789294+00:00"),
            CreatedAt = DateTime.Parse("2021-04-20T10:27:55.9581408+00:00")
        },
        new ProjectModel{
            Id = 75,
            AuthorId = 91,
            TeamId = 7,
            Name = "Kids Tasty",
            Description = "Perferendis asperiores consectetur cumque rem beatae quibusdam non ea.",
            Deadline = DateTime.Parse("2021-09-18T06:28:02.2803136+00:00"),
            CreatedAt = DateTime.Parse("2020-07-02T08:57:03.0350417+00:00")
        },
        new ProjectModel{
            Id = 76,
            AuthorId = 109,
            TeamId = 8,
            Name = "Lodge",
            Description = "Aut ut odit non cumque.",
            Deadline = DateTime.Parse("2021-07-20T16:58:36.4553333+00:00"),
            CreatedAt = DateTime.Parse("2020-06-28T11:17:11.8131449+00:00")
        },
        new ProjectModel{
            Id = 77,
            AuthorId = 104,
            TeamId = 8,
            Name = "Personal Loan Account orchestrate hacking",
            Description = "Voluptas saepe eum aut distinctio voluptatem praesentium.",
            Deadline = DateTime.Parse("2021-11-02T21:10:35.1705719+00:00"),
            CreatedAt = DateTime.Parse("2019-09-13T02:33:24.4765452+00:00")
        },
        new ProjectModel{
            Id = 78,
            AuthorId = 105,
            TeamId = 8,
            Name = "Sleek Cotton Keyboard COM neural",
            Description = "At nostrum enim quo cumque iusto quisquam qui.",
            Deadline = DateTime.Parse("2021-11-30T11:21:03.2314281+00:00"),
            CreatedAt = DateTime.Parse("2021-03-08T18:10:57.4094461+00:00")
        },
        new ProjectModel{
            Id = 79,
            AuthorId = 104,
            TeamId = 8,
            Name = "orchestrate disintermediate",
            Description = "Et et expedita rerum.",
            Deadline = DateTime.Parse("2021-08-26T22:27:26.4839892+00:00"),
            CreatedAt = DateTime.Parse("2020-03-10T19:03:12.1246224+00:00")
        },
        new ProjectModel{
            Id = 80,
            AuthorId = 106,
            TeamId = 8,
            Name = "Springs",
            Description = "Deleniti accusantium vel aliquid doloremque iure.",
            Deadline = DateTime.Parse("2021-11-03T07:52:30.9351581+00:00"),
            CreatedAt = DateTime.Parse("2021-05-26T01:34:28.6378602+00:00")
        },
        new ProjectModel{
            Id = 81,
            AuthorId = 108,
            TeamId = 8,
            Name = "Intelligent synergies",
            Description = "Quod beatae et optio sit reiciendis omnis cum sint.",
            Deadline = DateTime.Parse("2021-07-25T03:22:53.344656+00:00"),
            CreatedAt = DateTime.Parse("2021-03-03T14:25:25.7224981+00:00")
        },
        new ProjectModel{
            Id = 82,
            AuthorId = 111,
            TeamId = 8,
            Name = "invoice Fort",
            Description = "Laboriosam aut accusamus sit saepe et ut velit.",
            Deadline = DateTime.Parse("2021-08-24T20:41:20.0109658+00:00"),
            CreatedAt = DateTime.Parse("2019-11-12T21:52:24.5769742+00:00")
        },
        new ProjectModel{
            Id = 83,
            AuthorId = 109,
            TeamId = 8,
            Name = "communities EXE",
            Description = "Ullam voluptatem id dignissimos ea porro ut eveniet ipsum aut.",
            Deadline = DateTime.Parse("2021-09-25T15:16:36.5300856+00:00"),
            CreatedAt = DateTime.Parse("2020-12-18T10:55:57.9299305+00:00")
        },
        new ProjectModel{
            Id = 84,
            AuthorId = 106,
            TeamId = 8,
            Name = "Assistant California",
            Description = "Reiciendis velit amet.",
            Deadline = DateTime.Parse("2021-10-13T01:43:02.5133689+00:00"),
            CreatedAt = DateTime.Parse("2020-01-08T05:03:25.9219474+00:00")
        },
        new ProjectModel{
            Id = 85,
            AuthorId = 112,
            TeamId = 8,
            Name = "Solutions drive",
            Description = "Reiciendis aut facere harum omnis non eius recusandae aliquam occaecati.",
            Deadline = DateTime.Parse("2021-09-08T14:16:02.345795+00:00"),
            CreatedAt = DateTime.Parse("2020-01-11T12:12:40.8230334+00:00")
        },
        new ProjectModel{
            Id = 86,
            AuthorId = 110,
            TeamId = 8,
            Name = "Tasty Wooden Chicken Peso Uruguayo",
            Description = "Voluptas ut corrupti fugit.",
            Deadline = DateTime.Parse("2021-09-01T07:14:07.8562266+00:00"),
            CreatedAt = DateTime.Parse("2021-06-14T22:07:25.100923+00:00")
        },
        new ProjectModel{
            Id = 87,
            AuthorId = 107,
            TeamId = 8,
            Name = "copy Hong Kong Infrastructure",
            Description = "Quidem perferendis esse enim ut.",
            Deadline = DateTime.Parse("2021-07-19T17:14:22.9708238+00:00"),
            CreatedAt = DateTime.Parse("2019-11-29T05:55:20.958841+00:00")
        },
        new ProjectModel{
            Id = 88,
            AuthorId = 107,
            TeamId = 8,
            Name = "Cambridgeshire real-time invoice",
            Description = "Expedita itaque repellendus eos et.",
            Deadline = DateTime.Parse("2021-08-08T22:23:57.2325977+00:00"),
            CreatedAt = DateTime.Parse("2020-06-07T10:29:37.1398476+00:00")
        },
        new ProjectModel{
            Id = 89,
            AuthorId = 103,
            TeamId = 8,
            Name = "Swiss Franc Bedfordshire",
            Description = "Sit dolor saepe officiis voluptas eos enim in dolores maiores.",
            Deadline = DateTime.Parse("2021-07-03T04:35:10.2712948+00:00"),
            CreatedAt = DateTime.Parse("2021-04-21T04:12:37.6297882+00:00")
        },
        new ProjectModel{
            Id = 90,
            AuthorId = 107,
            TeamId = 8,
            Name = "Reunion hack",
            Description = "Repellat officiis blanditiis ipsum velit pariatur quis dolores quaerat autem.",
            Deadline = DateTime.Parse("2021-10-17T05:20:10.2509511+00:00"),
            CreatedAt = DateTime.Parse("2019-06-25T07:41:55.6955478+00:00")
        },
        new ProjectModel{
            Id = 91,
            AuthorId = 115,
            TeamId = 9,
            Name = "Wooden",
            Description = "Saepe sit distinctio veritatis aliquam odio.",
            Deadline = DateTime.Parse("2021-11-12T19:17:54.1738048+00:00"),
            CreatedAt = DateTime.Parse("2021-04-26T02:25:29.1987347+00:00")
        },
        new ProjectModel{
            Id = 92,
            AuthorId = 116,
            TeamId = 9,
            Name = "THX solid state Awesome Concrete Chair",
            Description = "Est atque voluptatem a facilis aut est nulla doloribus.",
            Deadline = DateTime.Parse("2021-11-08T08:39:31.1117124+00:00"),
            CreatedAt = DateTime.Parse("2019-10-30T19:53:28.5986128+00:00")
        },
        new ProjectModel{
            Id = 93,
            AuthorId = 117,
            TeamId = 9,
            Name = "action-items PNG",
            Description = "Recusandae et perferendis autem sit.",
            Deadline = DateTime.Parse("2021-09-21T01:20:56.6077298+00:00"),
            CreatedAt = DateTime.Parse("2020-03-27T18:56:06.3159036+00:00")
        },
        new ProjectModel{
            Id = 94,
            AuthorId = 119,
            TeamId = 9,
            Name = "Developer Mobility",
            Description = "Eaque voluptatem et.",
            Deadline = DateTime.Parse("2021-12-03T00:46:03.8284801+00:00"),
            CreatedAt = DateTime.Parse("2019-09-26T16:47:04.383587+00:00")
        },
        new ProjectModel{
            Id = 95,
            AuthorId = 116,
            TeamId = 9,
            Name = "Generic Rubber Bacon",
            Description = "Cupiditate ipsam ex quaerat blanditiis.",
            Deadline = DateTime.Parse("2021-07-06T22:53:44.6891399+00:00"),
            CreatedAt = DateTime.Parse("2020-12-24T01:50:44.9600861+00:00")
        },
        new ProjectModel{
            Id = 96,
            AuthorId = 115,
            TeamId = 9,
            Name = "metrics",
            Description = "Voluptatem occaecati laboriosam voluptatem.",
            Deadline = DateTime.Parse("2021-09-03T23:58:34.6912828+00:00"),
            CreatedAt = DateTime.Parse("2020-04-02T22:59:29.1267673+00:00")
        }
            };
        #endregion
    }
}