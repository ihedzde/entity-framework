using System;
using System.Collections.Generic;
using DAL.Domain.Models;

namespace DAL.Context.Seeds
{
    public static class TaskSeeds
    {
        #region Seeds
        public static IList<TaskModel> Tasks { get; set; } = new List<TaskModel>{
        new TaskModel{
            Id = 1,
            ProjectId = 2,
            PerformerId = 51,
            Name = "real-time",
            TLDR = "Quo sint aut et ea voluptatem omnis ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-15T22:50:46.0860832+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 2,
            ProjectId = 2,
            PerformerId = 66,
            Name = "product Direct utilize",
            TLDR = "Eum a eum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-15T15:06:52.0600666+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 3,
            ProjectId = 2,
            PerformerId = 75,
            Name = "bypass",
            TLDR = "Sint voluptatem quas.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-16T06:13:44.5773845+00:00"),
            FinishedAt = DateTime.Parse("2020-09-09T06:34:47.460216+00:00")
        },
        new TaskModel{
            Id = 4,
            ProjectId = 2,
            PerformerId = 85,
            Name = "withdrawal contextually-based",
            TLDR = "Delectus quibusdam id quia iure neque maiores molestias sed aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-19T00:58:34.8045103+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 5,
            ProjectId = 2,
            PerformerId = 81,
            Name = "mobile Organized",
            TLDR = "Earum blanditiis repellendus qui magni aliquam quisquam consequatur odio ducimus.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-06-15T06:03:48.0732466+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 6,
            ProjectId = 2,
            PerformerId = 109,
            Name = "world-class Circles",
            TLDR = "Reiciendis iusto rerum non et aut eaque.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-21T14:56:53.8117818+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 7,
            ProjectId = 2,
            PerformerId = 87,
            Name = "Automotive & Tools transitional bifurcated",
            TLDR = "Et rerum ad.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-10T16:21:12.0886153+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 8,
            ProjectId = 2,
            PerformerId = 68,
            Name = "payment methodologies",
            TLDR = "Voluptas nostrum sint.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-07T20:29:10.058295+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 9,
            ProjectId = 2,
            PerformerId = 26,
            Name = "Borders Mountain",
            TLDR = "Rerum iure soluta consequatur velit aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-06-19T11:42:55.0738847+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 10,
            ProjectId = 2,
            PerformerId = 59,
            Name = "navigate",
            TLDR = "Iure sequi unde.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-14T12:55:04.4031688+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 11,
            ProjectId = 3,
            PerformerId = 74,
            Name = "online incentivize",
            TLDR = "Explicabo illo sed qui cupiditate sapiente ut eligendi repellat.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-23T06:49:08.5212649+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 12,
            ProjectId = 3,
            PerformerId = 76,
            Name = "mesh Assimilated Fundamental",
            TLDR = "Vel sed ipsam.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-30T14:58:07.2887592+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 13,
            ProjectId = 3,
            PerformerId = 82,
            Name = "Auto Loan Account Cambridgeshire",
            TLDR = "Aut tenetur voluptas quasi esse.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-05T04:59:48.1013258+00:00"),
            FinishedAt = DateTime.Parse("2020-12-04T05:46:02.0821778+00:00")
        },
        new TaskModel{
            Id = 14,
            ProjectId = 3,
            PerformerId = 51,
            Name = "Intelligent Granite Mouse",
            TLDR = "Aliquam laboriosam consequatur qui.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-23T23:28:04.7786122+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 15,
            ProjectId = 3,
            PerformerId = 89,
            Name = "online",
            TLDR = "Optio eum aut sunt cum nam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-27T20:15:57.9869782+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 16,
            ProjectId = 3,
            PerformerId = 105,
            Name = "Knoll Principal redundant",
            TLDR = "Accusantium ea magni exercitationem et perferendis temporibus.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-05T13:59:24.0553315+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 17,
            ProjectId = 3,
            PerformerId = 97,
            Name = "Liaison upward-trending",
            TLDR = "Atque occaecati officiis non rerum.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-23T16:16:09.1404668+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 18,
            ProjectId = 3,
            PerformerId = 83,
            Name = "Sleek",
            TLDR = "Debitis quia odio doloremque error dicta quia natus rerum aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-19T18:06:29.1767226+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 19,
            ProjectId = 4,
            PerformerId = 110,
            Name = "Frozen",
            TLDR = "Enim molestiae id explicabo quisquam enim sint fuga.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-04T15:49:26.7733234+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 20,
            ProjectId = 4,
            PerformerId = 115,
            Name = "AI",
            TLDR = "Consequuntur porro voluptas dolor molestiae harum et.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-11T21:26:09.551986+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 21,
            ProjectId = 5,
            PerformerId = 91,
            Name = "Rustic Fresh Towels",
            TLDR = "Rem nisi nemo exercitationem dolorem unde consequuntur porro et ipsum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-15T02:20:10.1949772+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 22,
            ProjectId = 5,
            PerformerId = 53,
            Name = "infrastructures",
            TLDR = "Iure nostrum officiis suscipit et rerum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-24T01:06:14.3215285+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 23,
            ProjectId = 5,
            PerformerId = 43,
            Name = "Handmade Frozen Sausages gold infomediaries",
            TLDR = "Et repudiandae labore deserunt magnam est eum explicabo.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-12T13:48:53.0195273+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 24,
            ProjectId = 5,
            PerformerId = 65,
            Name = "payment",
            TLDR = "Et et quam.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-16T07:24:02.3214674+00:00"),
            FinishedAt = DateTime.Parse("2021-01-31T11:09:24.5612654+00:00")
        },
        new TaskModel{
            Id = 25,
            ProjectId = 5,
            PerformerId = 61,
            Name = "teal",
            TLDR = "Non fugit commodi reiciendis praesentium qui.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-16T03:42:38.6161575+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 26,
            ProjectId = 5,
            PerformerId = 122,
            Name = "Customer innovate Cambridgeshire",
            TLDR = "Omnis magnam voluptatem quia placeat assumenda aliquam.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-07T20:37:08.6264819+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 27,
            ProjectId = 5,
            PerformerId = 49,
            Name = "Berkshire",
            TLDR = "Aliquam assumenda omnis perspiciatis.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-17T01:10:49.2184558+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 28,
            ProjectId = 5,
            PerformerId = 46,
            Name = "Libyan Dinar",
            TLDR = "Voluptatem qui molestiae odio qui.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-06-29T09:38:34.572117+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 29,
            ProjectId = 5,
            PerformerId = 83,
            Name = "green",
            TLDR = "Voluptas quibusdam praesentium non.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-17T21:51:55.6490646+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 30,
            ProjectId = 5,
            PerformerId = 118,
            Name = "website Strategist matrix",
            TLDR = "Quasi possimus corrupti iure et repudiandae nulla deleniti.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-09T12:09:46.7449678+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 31,
            ProjectId = 6,
            PerformerId = 78,
            Name = "Massachusetts Directives",
            TLDR = "Ratione modi voluptatibus pariatur repudiandae consectetur tempora quia mollitia.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-22T00:41:22.2846408+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 32,
            ProjectId = 7,
            PerformerId = 34,
            Name = "South Carolina e-business",
            TLDR = "Ducimus distinctio veniam accusamus omnis et magni.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-13T17:30:05.516886+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 33,
            ProjectId = 7,
            PerformerId = 39,
            Name = "Suriname",
            TLDR = "Quidem rem porro nobis est rerum quia accusantium corporis.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-02T04:51:45.2341543+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 34,
            ProjectId = 7,
            PerformerId = 122,
            Name = "Iowa magenta",
            TLDR = "Optio reiciendis sequi reprehenderit aspernatur qui dolorem.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-21T20:38:26.3507675+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 35,
            ProjectId = 7,
            PerformerId = 62,
            Name = "invoice",
            TLDR = "Nihil adipisci necessitatibus error ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-10T19:27:54.6944614+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 36,
            ProjectId = 7,
            PerformerId = 91,
            Name = "Views",
            TLDR = "Voluptates rerum quo veniam beatae expedita.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-08T23:27:59.1093219+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 37,
            ProjectId = 7,
            PerformerId = 93,
            Name = "open-source South Dakota Concrete",
            TLDR = "Ullam voluptas sunt.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-24T19:47:12.2327505+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 38,
            ProjectId = 7,
            PerformerId = 62,
            Name = "redundant Handmade Concrete Fish Refined Rubber Chair",
            TLDR = "Enim alias vel est corporis nulla iste.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-26T06:22:39.3128783+00:00"),
            FinishedAt = DateTime.Parse("2021-02-24T23:46:51.1669257+00:00")
        },
        new TaskModel{
            Id = 39,
            ProjectId = 7,
            PerformerId = 100,
            Name = "PCI Home Loan Account",
            TLDR = "Rerum impedit aliquam facilis dolore perspiciatis occaecati amet provident.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-04T00:56:55.1014795+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 40,
            ProjectId = 7,
            PerformerId = 89,
            Name = "Rubber",
            TLDR = "Ullam praesentium deleniti repellat veniam totam asperiores sunt rerum.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-25T21:43:14.55256+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 41,
            ProjectId = 7,
            PerformerId = 77,
            Name = "24/7",
            TLDR = "Facere hic accusantium blanditiis eos quam est vero est.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-22T05:05:08.2230902+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 42,
            ProjectId = 8,
            PerformerId = 46,
            Name = "strategic turn-key",
            TLDR = "Repellendus est sequi animi quos ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-22T14:01:42.4918181+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 43,
            ProjectId = 8,
            PerformerId = 98,
            Name = "seize invoice",
            TLDR = "Animi veritatis tempora nihil eligendi harum et temporibus deleniti.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-27T04:15:22.5077319+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 44,
            ProjectId = 8,
            PerformerId = 119,
            Name = "Lights Specialist IB",
            TLDR = "Architecto voluptate modi omnis voluptate.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-27T23:32:00.890471+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 45,
            ProjectId = 8,
            PerformerId = 80,
            Name = "Borders fresh-thinking Books",
            TLDR = "Vel perferendis recusandae ducimus accusamus.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-06-18T09:08:26.5019879+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 46,
            ProjectId = 9,
            PerformerId = 61,
            Name = "JBOD Visionary",
            TLDR = "Sed qui tempora qui et corporis ea quia earum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-24T09:11:45.2063265+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 47,
            ProjectId = 9,
            PerformerId = 65,
            Name = "logistical Incredible Wooden Chips real-time",
            TLDR = "Cumque aut et est eius non.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-16T04:50:51.9619749+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 48,
            ProjectId = 9,
            PerformerId = 102,
            Name = "solid state monitor Creek",
            TLDR = "Veniam officia ab distinctio dolores non sunt nemo hic aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-12-08T00:12:26.1914069+00:00"),
            FinishedAt = DateTime.Parse("2020-08-13T03:54:17.5313629+00:00")
        },
        new TaskModel{
            Id = 49,
            ProjectId = 9,
            PerformerId = 80,
            Name = "Cuban Peso Handmade Wooden Chair Cloned",
            TLDR = "Qui rerum non corrupti est exercitationem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-20T07:50:43.7025354+00:00"),
            FinishedAt = DateTime.Parse("2020-11-26T09:01:53.6359258+00:00")
        },
        new TaskModel{
            Id = 50,
            ProjectId = 9,
            PerformerId = 63,
            Name = "quantifying",
            TLDR = "Placeat sed magni dolorum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-22T21:52:32.9724208+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 51,
            ProjectId = 9,
            PerformerId = 117,
            Name = "Officer North Carolina",
            TLDR = "Sit sint et molestias dolorum architecto at animi animi.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-13T03:31:55.4071035+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 52,
            ProjectId = 9,
            PerformerId = 22,
            Name = "holistic Tasty Plastic Soap",
            TLDR = "Iure distinctio rerum doloribus nulla optio similique molestias quos id.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-01T00:08:01.9861707+00:00"),
            FinishedAt = DateTime.Parse("2020-10-05T22:00:50.7807284+00:00")
        },
        new TaskModel{
            Id = 53,
            ProjectId = 9,
            PerformerId = 72,
            Name = "algorithm",
            TLDR = "Saepe occaecati non alias.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-08T06:29:20.6330371+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 54,
            ProjectId = 9,
            PerformerId = 72,
            Name = "Liaison",
            TLDR = "Suscipit voluptatem ipsa non voluptate et harum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-11T08:54:34.3447046+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 55,
            ProjectId = 9,
            PerformerId = 32,
            Name = "Organic azure Metal",
            TLDR = "Vel esse architecto.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-16T06:16:45.5749804+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 56,
            ProjectId = 10,
            PerformerId = 33,
            Name = "Montana",
            TLDR = "Eos expedita ducimus est reiciendis.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-05T21:19:57.7072606+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 57,
            ProjectId = 10,
            PerformerId = 96,
            Name = "Trail Representative",
            TLDR = "Molestias officia voluptates sit magni temporibus et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-28T04:44:00.6532117+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 58,
            ProjectId = 10,
            PerformerId = 63,
            Name = "Tasty Borders transmit",
            TLDR = "Sed iusto rerum suscipit non dolorum eveniet.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-06-09T17:52:17.4031764+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 59,
            ProjectId = 10,
            PerformerId = 99,
            Name = "Pre-emptive Plaza",
            TLDR = "Eius ut minus aliquam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-10T06:11:47.0833196+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 60,
            ProjectId = 10,
            PerformerId = 80,
            Name = "parse bypassing pink",
            TLDR = "Quasi rerum in quia molestias necessitatibus.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-30T23:51:06.5439548+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 61,
            ProjectId = 10,
            PerformerId = 110,
            Name = "Manors transition interfaces",
            TLDR = "Eos eos quaerat odit consectetur dolor.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-20T15:37:00.0138642+00:00"),
            FinishedAt = DateTime.Parse("2021-05-22T00:19:10.1683851+00:00")
        },
        new TaskModel{
            Id = 62,
            ProjectId = 10,
            PerformerId = 81,
            Name = "interfaces",
            TLDR = "Quod omnis tenetur illo.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-24T21:09:45.8520258+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 63,
            ProjectId = 10,
            PerformerId = 87,
            Name = "black Small",
            TLDR = "Iure consequuntur dignissimos sed est.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-30T01:42:07.5968725+00:00"),
            FinishedAt = DateTime.Parse("2020-11-08T15:32:39.8456543+00:00")
        },
        new TaskModel{
            Id = 64,
            ProjectId = 10,
            PerformerId = 74,
            Name = "Applications virtual",
            TLDR = "Esse eius nemo.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-11T14:06:46.5167097+00:00"),
            FinishedAt = DateTime.Parse("2020-08-17T20:14:38.1142835+00:00")
        },
        new TaskModel{
            Id = 65,
            ProjectId = 11,
            PerformerId = 103,
            Name = "parsing aggregate",
            TLDR = "Eveniet molestiae architecto accusantium possimus cupiditate rem porro.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-06T20:49:18.922592+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 66,
            ProjectId = 11,
            PerformerId = 96,
            Name = "deposit Profound",
            TLDR = "Est dolor doloribus a.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-09T10:29:40.420691+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 67,
            ProjectId = 11,
            PerformerId = 51,
            Name = "Turkish Lira",
            TLDR = "Velit ratione harum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-06T11:21:03.980873+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 68,
            ProjectId = 11,
            PerformerId = 102,
            Name = "Forint Burkina Faso",
            TLDR = "Sit repellat ratione dolore fuga voluptatem quia iste.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-12T16:20:44.6878614+00:00"),
            FinishedAt = DateTime.Parse("2020-07-25T06:36:43.441822+00:00")
        },
        new TaskModel{
            Id = 69,
            ProjectId = 12,
            PerformerId = 109,
            Name = "Delaware Savings Account",
            TLDR = "Cupiditate quo mollitia temporibus non ut omnis id ea fugit.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-12T23:56:27.9647509+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 70,
            ProjectId = 12,
            PerformerId = 64,
            Name = "Officer",
            TLDR = "Veritatis atque sint vero libero est quos sed qui.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-26T22:59:15.4153545+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 71,
            ProjectId = 12,
            PerformerId = 35,
            Name = "Checking Account Ferry",
            TLDR = "Aut nulla quo nulla odio alias voluptatem occaecati in iusto.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-20T12:59:10.9273488+00:00"),
            FinishedAt = DateTime.Parse("2020-11-18T14:23:56.911634+00:00")
        },
        new TaskModel{
            Id = 72,
            ProjectId = 12,
            PerformerId = 100,
            Name = "Mozambique",
            TLDR = "Non et quisquam sed adipisci repudiandae sed quod ea.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-01T21:17:19.6005276+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 73,
            ProjectId = 12,
            PerformerId = 84,
            Name = "Streamlined Robust Practical",
            TLDR = "Praesentium aut perspiciatis voluptatem omnis et vel exercitationem.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-24T09:57:23.9425773+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 74,
            ProjectId = 12,
            PerformerId = 87,
            Name = "cyan",
            TLDR = "Quibusdam est cumque mollitia est deserunt aut aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-09T23:00:32.5333322+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 75,
            ProjectId = 12,
            PerformerId = 114,
            Name = "Japan mission-critical hard drive",
            TLDR = "Commodi totam ea.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-22T11:31:02.7628221+00:00"),
            FinishedAt = DateTime.Parse("2021-01-04T00:34:34.7200403+00:00")
        },
        new TaskModel{
            Id = 76,
            ProjectId = 12,
            PerformerId = 77,
            Name = "Orchestrator",
            TLDR = "Rerum natus ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-04T18:07:53.5022409+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 77,
            ProjectId = 13,
            PerformerId = 74,
            Name = "Licensed Rubber Pants",
            TLDR = "Ad deleniti porro.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-04-15T07:10:45.8703821+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 78,
            ProjectId = 13,
            PerformerId = 82,
            Name = "encompassing Bedfordshire",
            TLDR = "Qui maxime ut modi maxime repellat aliquid voluptas.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-09T15:41:47.0383547+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 79,
            ProjectId = 13,
            PerformerId = 57,
            Name = "National",
            TLDR = "Id possimus aliquid quidem architecto unde in quasi.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-27T20:20:04.1564513+00:00"),
            FinishedAt = DateTime.Parse("2020-11-03T15:14:13.2892928+00:00")
        },
        new TaskModel{
            Id = 80,
            ProjectId = 13,
            PerformerId = 75,
            Name = "engage",
            TLDR = "Optio vitae qui et mollitia.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-06T10:02:33.9487883+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 81,
            ProjectId = 14,
            PerformerId = 122,
            Name = "Checking Account",
            TLDR = "Nihil aut laborum id est.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-05T15:52:20.0956044+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 82,
            ProjectId = 14,
            PerformerId = 119,
            Name = "grey",
            TLDR = "Incidunt dicta ex animi perspiciatis libero.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-03T14:57:00.0903991+00:00"),
            FinishedAt = DateTime.Parse("2020-12-18T15:15:16.643133+00:00")
        },
        new TaskModel{
            Id = 83,
            ProjectId = 14,
            PerformerId = 25,
            Name = "e-markets Soft Maine",
            TLDR = "Qui ea dolore nihil voluptatum voluptas.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-21T15:05:40.5120822+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 84,
            ProjectId = 15,
            PerformerId = 74,
            Name = "Zambian Kwacha morph",
            TLDR = "Harum id architecto et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-18T02:04:53.2453073+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 85,
            ProjectId = 15,
            PerformerId = 96,
            Name = "cultivate",
            TLDR = "Harum mollitia cumque maxime alias voluptatem aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-23T03:01:12.5792432+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 86,
            ProjectId = 17,
            PerformerId = 51,
            Name = "Estates",
            TLDR = "Tempora pariatur iste molestiae voluptas autem.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-31T11:03:03.0738514+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 87,
            ProjectId = 17,
            PerformerId = 114,
            Name = "Armenia",
            TLDR = "Explicabo similique itaque maxime itaque doloribus non rem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-30T12:31:54.0164555+00:00"),
            FinishedAt = DateTime.Parse("2021-06-07T14:27:19.6009829+00:00")
        },
        new TaskModel{
            Id = 88,
            ProjectId = 18,
            PerformerId = 62,
            Name = "JBOD",
            TLDR = "Natus sint qui quasi ab impedit fugiat voluptas est tenetur.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-04T04:51:58.8634833+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 89,
            ProjectId = 18,
            PerformerId = 41,
            Name = "challenge",
            TLDR = "Ex vero et quam sit autem tenetur rerum.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-09T21:08:10.1077559+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 90,
            ProjectId = 18,
            PerformerId = 36,
            Name = "Applications",
            TLDR = "Ut distinctio quae eos aut quam numquam.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-14T11:36:52.1346243+00:00"),
            FinishedAt = DateTime.Parse("2021-05-13T19:37:19.0371412+00:00")
        },
        new TaskModel{
            Id = 91,
            ProjectId = 18,
            PerformerId = 113,
            Name = "invoice Fresh",
            TLDR = "Quidem aut ipsam et.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-09T09:06:24.7908631+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 92,
            ProjectId = 18,
            PerformerId = 116,
            Name = "Frozen Operations bandwidth",
            TLDR = "Sit et sed debitis aliquam iusto doloremque.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-04-29T15:38:16.274333+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 93,
            ProjectId = 18,
            PerformerId = 62,
            Name = "Meadows",
            TLDR = "Aut molestias at.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-12T13:59:34.047388+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 94,
            ProjectId = 18,
            PerformerId = 117,
            Name = "Sleek Wooden Car",
            TLDR = "Nostrum ipsum ea quasi.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-06-13T05:45:12.5893908+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 95,
            ProjectId = 18,
            PerformerId = 59,
            Name = "attitude yellow",
            TLDR = "Voluptate eligendi quia.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-09T19:50:44.8465259+00:00"),
            FinishedAt = DateTime.Parse("2020-08-18T02:39:29.9477512+00:00")
        },
        new TaskModel{
            Id = 96,
            ProjectId = 18,
            PerformerId = 20,
            Name = "Applications bandwidth Refined Steel Salad",
            TLDR = "Corporis quod ad officiis illo quia eveniet repellat nisi id.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-04T21:30:38.0471332+00:00"),
            FinishedAt = DateTime.Parse("2021-04-13T12:40:07.5191458+00:00")
        },
        new TaskModel{
            Id = 97,
            ProjectId = 18,
            PerformerId = 122,
            Name = "programming Investment Account",
            TLDR = "Deleniti ut et veniam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-14T16:52:38.6884275+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 98,
            ProjectId = 19,
            PerformerId = 81,
            Name = "pricing structure engineer",
            TLDR = "At dolor ex occaecati molestiae fuga.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-19T13:17:59.2579394+00:00"),
            FinishedAt = DateTime.Parse("2021-04-12T22:54:27.2505945+00:00")
        },
        new TaskModel{
            Id = 99,
            ProjectId = 19,
            PerformerId = 91,
            Name = "Buckinghamshire",
            TLDR = "Ad eius id quo ducimus et et asperiores officia.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-07T15:58:36.7203735+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 100,
            ProjectId = 19,
            PerformerId = 96,
            Name = "indexing",
            TLDR = "Amet aspernatur voluptatibus quisquam enim sapiente omnis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-27T22:47:24.8729588+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 101,
            ProjectId = 19,
            PerformerId = 81,
            Name = "Cotton",
            TLDR = "Quia numquam eligendi.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-09T20:01:09.3074858+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 102,
            ProjectId = 19,
            PerformerId = 108,
            Name = "Palladium online",
            TLDR = "Necessitatibus illo magnam mollitia quis minus nulla iste rerum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-16T11:43:29.1381798+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 103,
            ProjectId = 19,
            PerformerId = 66,
            Name = "Director Estates",
            TLDR = "Quam sunt magni in.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-15T11:25:54.8909093+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 104,
            ProjectId = 19,
            PerformerId = 33,
            Name = "Tools, Industrial & Games virtual",
            TLDR = "Quo impedit nesciunt assumenda ut ab odit voluptatem.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-28T21:35:48.3231459+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 105,
            ProjectId = 19,
            PerformerId = 56,
            Name = "Money Market Account impactful",
            TLDR = "Pariatur aut pariatur id quisquam nihil vel fuga sapiente consectetur.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-23T19:09:15.1561863+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 106,
            ProjectId = 19,
            PerformerId = 86,
            Name = "Avon invoice",
            TLDR = "Exercitationem enim accusantium.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-26T23:19:50.9419889+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 107,
            ProjectId = 20,
            PerformerId = 56,
            Name = "Money Market Account",
            TLDR = "Corrupti optio doloribus aut iusto ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-24T18:15:26.0596376+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 108,
            ProjectId = 20,
            PerformerId = 25,
            Name = "Clothing, Outdoors & Games",
            TLDR = "Est reiciendis voluptas iste quod.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-05T01:30:46.6979142+00:00"),
            FinishedAt = DateTime.Parse("2020-06-23T04:21:57.3653253+00:00")
        },
        new TaskModel{
            Id = 109,
            ProjectId = 20,
            PerformerId = 110,
            Name = "Generic",
            TLDR = "Qui non qui.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-21T13:16:59.620059+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 110,
            ProjectId = 20,
            PerformerId = 79,
            Name = "Branding",
            TLDR = "Eos reiciendis autem dolorem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-24T02:19:24.5645633+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 111,
            ProjectId = 20,
            PerformerId = 107,
            Name = "incubate Usability Ergonomic Rubber Shoes",
            TLDR = "Voluptas sed inventore quia ullam sed voluptas placeat.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-27T17:39:12.839299+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 112,
            ProjectId = 22,
            PerformerId = 61,
            Name = "generating Gorgeous Metal Table",
            TLDR = "Modi repellendus unde labore ut repellat.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-05T07:41:11.928253+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 113,
            ProjectId = 23,
            PerformerId = 54,
            Name = "Handcrafted Generic",
            TLDR = "Omnis velit iusto sequi iste nisi in provident ex.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-03T05:06:45.7555804+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 114,
            ProjectId = 23,
            PerformerId = 27,
            Name = "Fresh Pennsylvania Operations",
            TLDR = "Repellendus molestias cum unde dolorem pariatur repellendus occaecati consectetur.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-04T00:52:00.8559837+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 115,
            ProjectId = 25,
            PerformerId = 26,
            Name = "fuchsia",
            TLDR = "Rerum dolores dolor dolores facilis qui dignissimos voluptatum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-27T01:00:59.4431438+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 116,
            ProjectId = 25,
            PerformerId = 52,
            Name = "Personal Loan Account",
            TLDR = "Consequatur sint omnis maxime quo dolores.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-26T21:27:46.8922867+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 117,
            ProjectId = 25,
            PerformerId = 108,
            Name = "copy",
            TLDR = "Consequuntur eum fugit et est sit et.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-06-03T00:57:41.3175228+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 118,
            ProjectId = 25,
            PerformerId = 27,
            Name = "hack",
            TLDR = "In aperiam officia modi nobis quis a et alias voluptas.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-04-16T19:56:33.0081552+00:00"),
            FinishedAt = DateTime.Parse("2020-08-07T02:39:33.7412623+00:00")
        },
        new TaskModel{
            Id = 119,
            ProjectId = 25,
            PerformerId = 79,
            Name = "Buckinghamshire",
            TLDR = "Sed atque saepe aut sed exercitationem aspernatur fugiat qui.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-03T06:00:14.5658776+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 120,
            ProjectId = 25,
            PerformerId = 54,
            Name = "implementation Intelligent Steel Pizza",
            TLDR = "Officiis enim est rerum temporibus doloremque est eum aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-01T23:20:30.2430536+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 121,
            ProjectId = 25,
            PerformerId = 84,
            Name = "applications attitude hacking",
            TLDR = "Labore rerum quia illo eius et fugit.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-07T15:54:05.9136495+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 122,
            ProjectId = 25,
            PerformerId = 91,
            Name = "Heights installation",
            TLDR = "Odit ipsam nihil.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-02T03:41:48.9370988+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 123,
            ProjectId = 25,
            PerformerId = 22,
            Name = "Incredible Fresh Chips",
            TLDR = "Eligendi aliquid architecto.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-15T02:34:45.6256254+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 124,
            ProjectId = 25,
            PerformerId = 27,
            Name = "XML Nebraska Andorra",
            TLDR = "Aspernatur molestias temporibus illo placeat eum.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-07T09:13:52.184586+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 125,
            ProjectId = 26,
            PerformerId = 39,
            Name = "Refined users",
            TLDR = "Consequatur quaerat molestias fuga iste est aliquam aperiam animi error.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-27T04:02:09.3224354+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 126,
            ProjectId = 26,
            PerformerId = 20,
            Name = "Data Unbranded systematic",
            TLDR = "Id ut repudiandae temporibus dolorum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-07T08:59:52.0204073+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 127,
            ProjectId = 26,
            PerformerId = 101,
            Name = "Saint Martin Central",
            TLDR = "Sit et quo incidunt provident adipisci recusandae aut commodi sit.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-18T17:05:25.138202+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 128,
            ProjectId = 26,
            PerformerId = 107,
            Name = "Small Fresh Computer invoice",
            TLDR = "Exercitationem sapiente ut adipisci iusto aspernatur inventore dolore.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-29T08:15:13.2374746+00:00"),
            FinishedAt = DateTime.Parse("2021-01-01T21:27:11.0605468+00:00")
        },
        new TaskModel{
            Id = 129,
            ProjectId = 26,
            PerformerId = 27,
            Name = "New York",
            TLDR = "Voluptate id et molestiae iusto ipsam quas repellat.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-15T23:58:32.4349464+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 130,
            ProjectId = 26,
            PerformerId = 97,
            Name = "envisioneer Grocery zero tolerance",
            TLDR = "Et aut corporis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-01T04:54:37.0351595+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 131,
            ProjectId = 26,
            PerformerId = 45,
            Name = "Sleek hybrid Cambridgeshire",
            TLDR = "Velit voluptatibus blanditiis ut odio quos aspernatur.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-06-05T23:31:01.6335205+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 132,
            ProjectId = 26,
            PerformerId = 76,
            Name = "Research",
            TLDR = "Est voluptate odit temporibus quia.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-20T21:48:10.6803449+00:00"),
            FinishedAt = DateTime.Parse("2020-07-23T02:46:13.12671+00:00")
        },
        new TaskModel{
            Id = 133,
            ProjectId = 26,
            PerformerId = 104,
            Name = "azure RSS",
            TLDR = "Et totam voluptatum delectus iusto consequatur.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-11T21:16:54.1009416+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 134,
            ProjectId = 26,
            PerformerId = 83,
            Name = "incubate Savings Account",
            TLDR = "Tenetur ratione iste dicta autem ab et optio et.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-19T17:19:29.7750992+00:00"),
            FinishedAt = DateTime.Parse("2020-10-03T14:29:07.5054155+00:00")
        },
        new TaskModel{
            Id = 135,
            ProjectId = 27,
            PerformerId = 38,
            Name = "Games & Movies Wisconsin Rufiyaa",
            TLDR = "Nulla aspernatur debitis quas inventore.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-27T13:00:24.6777571+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 136,
            ProjectId = 27,
            PerformerId = 61,
            Name = "circuit relationships web-readiness",
            TLDR = "Consequatur adipisci id quidem quisquam.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-09T19:52:18.9274633+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 137,
            ProjectId = 27,
            PerformerId = 61,
            Name = "Auto Loan Account maximized",
            TLDR = "Labore ad ipsum non rerum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-05T15:15:08.5459242+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 138,
            ProjectId = 28,
            PerformerId = 42,
            Name = "Rustic",
            TLDR = "Expedita non impedit sit commodi sed non vel.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-18T06:08:53.9429925+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 139,
            ProjectId = 28,
            PerformerId = 108,
            Name = "payment Bahamian Dollar",
            TLDR = "Neque aliquam dolore aut et at nulla numquam.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-06-02T03:25:25.9543852+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 140,
            ProjectId = 28,
            PerformerId = 52,
            Name = "reboot Rustic Future",
            TLDR = "Doloremque eveniet doloremque tempora consequatur repellendus atque accusamus.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-28T05:48:26.9685599+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 141,
            ProjectId = 28,
            PerformerId = 68,
            Name = "technologies markets Borders",
            TLDR = "Adipisci unde fuga alias ipsam.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-01T10:06:12.4506433+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 142,
            ProjectId = 28,
            PerformerId = 67,
            Name = "Human Plastic Team-oriented",
            TLDR = "Et ea nisi beatae sint perferendis quisquam nulla.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-10T18:27:56.296025+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 143,
            ProjectId = 28,
            PerformerId = 24,
            Name = "Comoro Franc copying",
            TLDR = "Tempora est id autem sed commodi.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-24T18:40:32.2944189+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 144,
            ProjectId = 28,
            PerformerId = 106,
            Name = "sky blue",
            TLDR = "Voluptate sequi aut fugit aperiam laborum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-16T14:08:41.1669111+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 145,
            ProjectId = 28,
            PerformerId = 47,
            Name = "action-items turquoise access",
            TLDR = "Expedita itaque voluptas maiores tempora quo expedita nostrum harum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-18T21:24:54.9267868+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 146,
            ProjectId = 28,
            PerformerId = 35,
            Name = "Awesome Cotton Fish project",
            TLDR = "Eius qui dolorum pariatur voluptatum odio vel et eos perspiciatis.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-23T23:13:51.7951792+00:00"),
            FinishedAt = DateTime.Parse("2021-02-04T07:17:49.2814263+00:00")
        },
        new TaskModel{
            Id = 147,
            ProjectId = 29,
            PerformerId = 28,
            Name = "interface",
            TLDR = "Voluptatem est eius voluptatum et aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-10T06:21:58.33468+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 148,
            ProjectId = 29,
            PerformerId = 20,
            Name = "Armenian Dram monitor",
            TLDR = "Commodi vel labore numquam repudiandae qui fugit voluptatem exercitationem sint.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-23T01:50:41.1451925+00:00"),
            FinishedAt = DateTime.Parse("2021-05-26T12:40:08.4734603+00:00")
        },
        new TaskModel{
            Id = 149,
            ProjectId = 29,
            PerformerId = 59,
            Name = "plug-and-play Bedfordshire Electronics, Outdoors & Shoes",
            TLDR = "Beatae veniam dolores ut voluptas ea nesciunt.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-31T02:15:27.3044214+00:00"),
            FinishedAt = DateTime.Parse("2021-04-29T03:01:49.1185039+00:00")
        },
        new TaskModel{
            Id = 150,
            ProjectId = 29,
            PerformerId = 57,
            Name = "Fantastic Frozen Computer",
            TLDR = "Eos inventore blanditiis facere.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-09T19:08:40.0966903+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 151,
            ProjectId = 29,
            PerformerId = 99,
            Name = "mindshare Beauty",
            TLDR = "Animi quo dignissimos reiciendis aliquid delectus.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-20T08:41:00.911401+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 152,
            ProjectId = 30,
            PerformerId = 54,
            Name = "bypass",
            TLDR = "Ut omnis illo illum quis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-23T22:03:25.3422179+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 153,
            ProjectId = 30,
            PerformerId = 93,
            Name = "1080p Rustic",
            TLDR = "Et libero error similique voluptatum quia aut ipsum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-22T20:43:50.3930618+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 154,
            ProjectId = 30,
            PerformerId = 87,
            Name = "Small Avon open-source",
            TLDR = "Nobis quia quod.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-28T07:16:56.5943214+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 155,
            ProjectId = 30,
            PerformerId = 62,
            Name = "reinvent invoice Berkshire",
            TLDR = "Et saepe omnis tenetur.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-29T21:09:16.262999+00:00"),
            FinishedAt = DateTime.Parse("2021-02-26T01:30:23.1955303+00:00")
        },
        new TaskModel{
            Id = 156,
            ProjectId = 30,
            PerformerId = 59,
            Name = "transition",
            TLDR = "Iste suscipit soluta voluptatem rerum excepturi dolor laudantium.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-03T07:52:11.3811589+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 157,
            ProjectId = 30,
            PerformerId = 37,
            Name = "parse systemic Lithuanian Litas",
            TLDR = "Et reiciendis neque eaque exercitationem labore consequatur.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-16T11:01:11.3615796+00:00"),
            FinishedAt = DateTime.Parse("2021-02-10T16:45:17.7885284+00:00")
        },
        new TaskModel{
            Id = 158,
            ProjectId = 32,
            PerformerId = 20,
            Name = "models Data Green",
            TLDR = "Vel eaque vel.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-01T07:19:49.7763612+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 159,
            ProjectId = 32,
            PerformerId = 122,
            Name = "cross-platform",
            TLDR = "Doloremque possimus ea sint tempora officia.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-28T22:23:09.7916352+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 160,
            ProjectId = 33,
            PerformerId = 26,
            Name = "Unbranded Plastic Ball revolutionary coherent",
            TLDR = "Quam earum ut et excepturi sed aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-09T03:51:32.2510254+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 161,
            ProjectId = 33,
            PerformerId = 108,
            Name = "Small magenta mint green",
            TLDR = "Qui sint atque placeat quasi et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-05T16:43:30.1942234+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 162,
            ProjectId = 33,
            PerformerId = 45,
            Name = "turquoise killer",
            TLDR = "Voluptatibus architecto voluptatem consequatur.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-20T04:47:41.2896027+00:00"),
            FinishedAt = DateTime.Parse("2021-04-10T03:42:15.9627536+00:00")
        },
        new TaskModel{
            Id = 163,
            ProjectId = 33,
            PerformerId = 95,
            Name = "gold approach Personal Loan Account",
            TLDR = "Nihil maxime quas.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-07T04:54:54.0066644+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 164,
            ProjectId = 33,
            PerformerId = 113,
            Name = "Lithuania plug-and-play",
            TLDR = "Molestias omnis fuga perferendis est quia aut voluptatum.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-06T15:39:06.9207849+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 165,
            ProjectId = 33,
            PerformerId = 40,
            Name = "functionalities deposit",
            TLDR = "Nostrum quas quisquam fugiat sapiente.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-06T15:02:52.4771772+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 166,
            ProjectId = 34,
            PerformerId = 63,
            Name = "invoice Public-key Handcrafted Steel Sausages",
            TLDR = "Aut incidunt accusamus repellendus rerum qui aut totam excepturi.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-06-07T22:55:11.4208607+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 167,
            ProjectId = 34,
            PerformerId = 85,
            Name = "solid state",
            TLDR = "Dignissimos repudiandae earum assumenda inventore qui accusantium accusamus.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-05T13:26:09.8046507+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 168,
            ProjectId = 34,
            PerformerId = 44,
            Name = "orchid black",
            TLDR = "Quae sit maiores quo quo qui id similique nostrum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-29T09:35:54.0638599+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 169,
            ProjectId = 34,
            PerformerId = 32,
            Name = "Checking Account",
            TLDR = "Suscipit ipsum quam possimus beatae autem asperiores est ipsum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-18T19:32:36.9886581+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 170,
            ProjectId = 35,
            PerformerId = 109,
            Name = "THX Circles Assimilated",
            TLDR = "Rerum minus dolor quae eveniet.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-12-01T05:17:15.3630538+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 171,
            ProjectId = 35,
            PerformerId = 39,
            Name = "bypassing Glen Communications",
            TLDR = "Maiores natus nostrum voluptatem dicta ex impedit numquam hic.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-05T22:25:46.9190211+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 172,
            ProjectId = 35,
            PerformerId = 79,
            Name = "connecting",
            TLDR = "Ut sit explicabo.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-09T15:20:32.0747465+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 173,
            ProjectId = 35,
            PerformerId = 21,
            Name = "Shores Montserrat",
            TLDR = "Pariatur incidunt et deleniti doloremque error est dolorem.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-21T08:09:55.983859+00:00"),
            FinishedAt = DateTime.Parse("2020-10-15T20:18:39.3536023+00:00")
        },
        new TaskModel{
            Id = 174,
            ProjectId = 36,
            PerformerId = 107,
            Name = "context-sensitive Illinois Future",
            TLDR = "Minima placeat fugit.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-18T08:37:48.0023948+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 175,
            ProjectId = 36,
            PerformerId = 104,
            Name = "Loaf",
            TLDR = "Expedita corporis optio laboriosam vel illum delectus autem.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-01T18:37:39.0872225+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 176,
            ProjectId = 36,
            PerformerId = 118,
            Name = "users alliance",
            TLDR = "Laborum sit tempore sunt deleniti consequuntur et.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-27T21:09:27.5857263+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 177,
            ProjectId = 36,
            PerformerId = 43,
            Name = "Hollow",
            TLDR = "Ut natus rerum quod.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-27T01:26:58.8717122+00:00"),
            FinishedAt = DateTime.Parse("2020-07-18T05:05:22.2598288+00:00")
        },
        new TaskModel{
            Id = 178,
            ProjectId = 36,
            PerformerId = 119,
            Name = "intuitive",
            TLDR = "Est nam nemo occaecati inventore tenetur eum enim.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-07T09:32:05.0767201+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 179,
            ProjectId = 36,
            PerformerId = 117,
            Name = "Buckinghamshire Licensed tan",
            TLDR = "Dolor et velit esse nemo veritatis nobis.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-23T11:23:02.2561909+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 180,
            ProjectId = 36,
            PerformerId = 121,
            Name = "solution-oriented",
            TLDR = "Deleniti dolorem ad harum doloremque.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-05T06:27:20.608382+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 181,
            ProjectId = 36,
            PerformerId = 101,
            Name = "Common XML",
            TLDR = "Harum ipsa excepturi quibusdam.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-20T18:32:21.8411352+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 182,
            ProjectId = 36,
            PerformerId = 34,
            Name = "grid-enabled Intelligent hard drive",
            TLDR = "Cupiditate mollitia animi et aut qui delectus inventore.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-28T19:58:53.9618051+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 183,
            ProjectId = 36,
            PerformerId = 43,
            Name = "1080p Director",
            TLDR = "Voluptas doloremque nesciunt quis.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-26T04:48:27.70101+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 184,
            ProjectId = 37,
            PerformerId = 57,
            Name = "Cape",
            TLDR = "Vel similique exercitationem.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-24T11:18:49.1888314+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 185,
            ProjectId = 37,
            PerformerId = 76,
            Name = "explicit Inverse Global",
            TLDR = "Nam explicabo officiis in consequatur laborum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-18T14:46:10.8424659+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 186,
            ProjectId = 37,
            PerformerId = 60,
            Name = "Handcrafted Rubber Ball Lodge",
            TLDR = "Repellendus cupiditate eos minus animi nihil.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-28T21:45:58.2893703+00:00"),
            FinishedAt = DateTime.Parse("2021-03-08T09:16:30.856209+00:00")
        },
        new TaskModel{
            Id = 187,
            ProjectId = 37,
            PerformerId = 20,
            Name = "Planner",
            TLDR = "Totam cumque ipsum culpa voluptatum quia velit sed.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-04T10:39:02.823439+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 188,
            ProjectId = 37,
            PerformerId = 23,
            Name = "Handcrafted Steel Cheese Optimization",
            TLDR = "Itaque aut ea ad officiis deserunt quia laudantium.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-28T01:00:51.5776797+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 189,
            ProjectId = 37,
            PerformerId = 100,
            Name = "Administrator Zambian Kwacha",
            TLDR = "Numquam qui qui dolores consequatur.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-03T04:59:12.2492714+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 190,
            ProjectId = 37,
            PerformerId = 44,
            Name = "synergies Unbranded Tokelau",
            TLDR = "Nemo animi laborum debitis id at porro dolor ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-16T16:15:04.6962771+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 191,
            ProjectId = 37,
            PerformerId = 58,
            Name = "Lari Sleek Metal Pants Outdoors",
            TLDR = "Incidunt exercitationem est.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-14T00:37:08.8175116+00:00"),
            FinishedAt = DateTime.Parse("2020-09-22T17:56:30.2567189+00:00")
        },
        new TaskModel{
            Id = 192,
            ProjectId = 37,
            PerformerId = 23,
            Name = "Knoll",
            TLDR = "Quidem laboriosam nulla mollitia esse eum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-16T16:38:24.2935165+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 193,
            ProjectId = 37,
            PerformerId = 121,
            Name = "Money Market Account 1080p",
            TLDR = "Modi praesentium enim.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-04-18T17:34:26.2329575+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 194,
            ProjectId = 38,
            PerformerId = 31,
            Name = "cross-platform Principal",
            TLDR = "Aut qui et earum nemo necessitatibus quaerat cupiditate.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-23T11:35:18.3101999+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 195,
            ProjectId = 38,
            PerformerId = 45,
            Name = "Manager Research Facilitator",
            TLDR = "Consequuntur repellat voluptates perspiciatis.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-18T05:43:00.7474334+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 196,
            ProjectId = 38,
            PerformerId = 113,
            Name = "overriding",
            TLDR = "Sunt modi et pariatur dignissimos.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-03T23:13:54.2476631+00:00"),
            FinishedAt = DateTime.Parse("2020-07-05T03:51:15.2460441+00:00")
        },
        new TaskModel{
            Id = 197,
            ProjectId = 38,
            PerformerId = 116,
            Name = "Tunnel scalable",
            TLDR = "Quam quo magnam.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-16T07:23:11.7311934+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 198,
            ProjectId = 38,
            PerformerId = 61,
            Name = "methodologies",
            TLDR = "Tenetur laboriosam nihil voluptas ad cumque ut dolor assumenda.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-12T13:34:35.7845906+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 199,
            ProjectId = 38,
            PerformerId = 113,
            Name = "maroon",
            TLDR = "Expedita architecto tempora amet voluptas.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-05T11:37:54.0419691+00:00"),
            FinishedAt = DateTime.Parse("2020-09-09T06:13:42.756639+00:00")
        },
        new TaskModel{
            Id = 200,
            ProjectId = 39,
            PerformerId = 107,
            Name = "bypassing Mall access",
            TLDR = "Sint eum voluptas amet ea harum aliquid.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-01T16:25:35.1757923+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 201,
            ProjectId = 40,
            PerformerId = 48,
            Name = "Facilitator circuit",
            TLDR = "Assumenda sunt ex voluptatibus ut animi illo blanditiis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-19T05:50:43.453122+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 202,
            ProjectId = 40,
            PerformerId = 74,
            Name = "Credit Card Account Wyoming Buckinghamshire",
            TLDR = "Quo sit nisi eius voluptas et similique.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-11T11:17:47.2211253+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 203,
            ProjectId = 41,
            PerformerId = 89,
            Name = "Sports",
            TLDR = "Officia corporis dolor ea perspiciatis quasi ex iste corporis expedita.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-11T03:35:02.5255408+00:00"),
            FinishedAt = DateTime.Parse("2021-01-16T18:55:01.9543462+00:00")
        },
        new TaskModel{
            Id = 204,
            ProjectId = 41,
            PerformerId = 47,
            Name = "Computers, Books & Shoes",
            TLDR = "Eligendi atque iure quia et hic harum.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-30T02:04:20.1986313+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 205,
            ProjectId = 41,
            PerformerId = 94,
            Name = "AI",
            TLDR = "Iusto repellendus ad eos veniam animi.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-26T17:35:36.926714+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 206,
            ProjectId = 41,
            PerformerId = 65,
            Name = "withdrawal hack",
            TLDR = "Rem saepe illum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-09T10:12:36.4147914+00:00"),
            FinishedAt = DateTime.Parse("2021-03-12T18:02:50.0018659+00:00")
        },
        new TaskModel{
            Id = 207,
            ProjectId = 41,
            PerformerId = 32,
            Name = "Guarani Practical Metal Pizza drive",
            TLDR = "Dolores blanditiis id eveniet modi sunt sunt.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-22T12:50:40.8000187+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 208,
            ProjectId = 41,
            PerformerId = 43,
            Name = "transparent",
            TLDR = "Harum provident asperiores deserunt et.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-03T14:43:44.3756205+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 209,
            ProjectId = 42,
            PerformerId = 117,
            Name = "Credit Card Account Azerbaijan Checking Account",
            TLDR = "Voluptatem velit consectetur magnam et dolore deleniti unde fuga.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-23T11:30:34.0905971+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 210,
            ProjectId = 42,
            PerformerId = 43,
            Name = "Incredible",
            TLDR = "Quia maiores laboriosam error qui omnis.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-18T04:44:55.7920626+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 211,
            ProjectId = 42,
            PerformerId = 24,
            Name = "Pines Sleek Granite Sausages Gateway",
            TLDR = "Ut ratione est enim.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-05T04:33:36.6834262+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 212,
            ProjectId = 42,
            PerformerId = 84,
            Name = "Falls withdrawal",
            TLDR = "Voluptatum possimus dolorem.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-09T23:18:24.8573713+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 213,
            ProjectId = 42,
            PerformerId = 101,
            Name = "GB FTP",
            TLDR = "Quisquam ratione quidem ipsa sed eum quo minus exercitationem.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-07T11:54:49.1890182+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 214,
            ProjectId = 42,
            PerformerId = 29,
            Name = "Mountain",
            TLDR = "Ratione magni eaque facere magni perferendis tempore sed illum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-28T15:02:13.9827705+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 215,
            ProjectId = 43,
            PerformerId = 98,
            Name = "Regional Officer Borders",
            TLDR = "Maxime laudantium repellendus voluptatem.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-02T02:58:45.1456475+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 216,
            ProjectId = 43,
            PerformerId = 26,
            Name = "scale ivory",
            TLDR = "Perferendis fuga consequatur nam aut deleniti et repudiandae expedita aliquid.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-27T06:18:13.8509257+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 217,
            ProjectId = 43,
            PerformerId = 68,
            Name = "interface Product",
            TLDR = "Ut eligendi fuga ea autem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-30T10:15:01.1108036+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 218,
            ProjectId = 43,
            PerformerId = 22,
            Name = "Unbranded Plastic Pants neural",
            TLDR = "Est fugit omnis consequuntur delectus dolore esse.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-10T20:28:33.5648357+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 219,
            ProjectId = 43,
            PerformerId = 25,
            Name = "holistic",
            TLDR = "Nam non quod voluptatem quidem id deserunt necessitatibus.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-14T05:35:02.9109232+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 220,
            ProjectId = 43,
            PerformerId = 44,
            Name = "Rubber Turkey",
            TLDR = "Sit id modi est.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-03T18:46:22.7293072+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 221,
            ProjectId = 43,
            PerformerId = 66,
            Name = "Data",
            TLDR = "Sunt quaerat est velit illo.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-10T07:36:33.0524126+00:00"),
            FinishedAt = DateTime.Parse("2020-06-30T19:04:06.9517431+00:00")
        },
        new TaskModel{
            Id = 222,
            ProjectId = 43,
            PerformerId = 73,
            Name = "Borders Books, Garden & Health",
            TLDR = "Sed corporis architecto nostrum earum dignissimos.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-10T23:54:01.5131846+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 223,
            ProjectId = 44,
            PerformerId = 34,
            Name = "Denar Security Consultant",
            TLDR = "Molestias voluptatibus itaque quia distinctio architecto quod dicta.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-21T05:51:53.9478934+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 224,
            ProjectId = 44,
            PerformerId = 121,
            Name = "convergence Extended",
            TLDR = "Dolore quia eligendi ut praesentium.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-29T03:15:32.8119441+00:00"),
            FinishedAt = DateTime.Parse("2021-02-03T13:21:00.250607+00:00")
        },
        new TaskModel{
            Id = 225,
            ProjectId = 44,
            PerformerId = 101,
            Name = "Handcrafted",
            TLDR = "Officia quod et.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-21T13:20:31.8175271+00:00"),
            FinishedAt = DateTime.Parse("2021-02-08T21:57:49.2194614+00:00")
        },
        new TaskModel{
            Id = 226,
            ProjectId = 44,
            PerformerId = 85,
            Name = "Brazilian Real Grocery purple",
            TLDR = "Ad sit nihil illum libero.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-04T04:51:37.9151777+00:00"),
            FinishedAt = DateTime.Parse("2020-12-17T08:23:34.6064176+00:00")
        },
        new TaskModel{
            Id = 227,
            ProjectId = 44,
            PerformerId = 93,
            Name = "Home Usability",
            TLDR = "Officia et repellat et tempore.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-21T05:29:02.6093109+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 228,
            ProjectId = 44,
            PerformerId = 66,
            Name = "Handcrafted Assimilated",
            TLDR = "Tempora ut excepturi aspernatur vel fugiat commodi architecto incidunt odit.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-12-12T02:32:48.9595634+00:00"),
            FinishedAt = DateTime.Parse("2020-08-02T03:21:11.6604174+00:00")
        },
        new TaskModel{
            Id = 229,
            ProjectId = 44,
            PerformerId = 56,
            Name = "panel system",
            TLDR = "Numquam voluptates officia velit aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-11T04:23:27.2873198+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 230,
            ProjectId = 44,
            PerformerId = 63,
            Name = "Berkshire",
            TLDR = "Illo minus dignissimos.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-05T14:13:04.3102895+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 231,
            ProjectId = 45,
            PerformerId = 21,
            Name = "Beauty standardization",
            TLDR = "Rerum ut autem nobis possimus ipsam hic aspernatur eius.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-12T20:03:28.7199575+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 232,
            ProjectId = 45,
            PerformerId = 103,
            Name = "brand virtual SQL",
            TLDR = "Dolorem nobis et exercitationem eveniet.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-28T13:23:02.6944847+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 233,
            ProjectId = 45,
            PerformerId = 110,
            Name = "Sleek Wooden Chicken copy",
            TLDR = "Dolor nisi distinctio eos aspernatur iste qui quia.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-20T18:56:31.5638386+00:00"),
            FinishedAt = DateTime.Parse("2020-10-26T07:09:26.5025653+00:00")
        },
        new TaskModel{
            Id = 234,
            ProjectId = 45,
            PerformerId = 93,
            Name = "Analyst deposit Steel",
            TLDR = "Quam non nihil officia quia omnis.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-15T11:54:54.673652+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 235,
            ProjectId = 45,
            PerformerId = 90,
            Name = "Mauritius Tasty Personal Loan Account",
            TLDR = "Aspernatur rem est odio id eum sed.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-15T22:16:04.5753387+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 236,
            ProjectId = 45,
            PerformerId = 21,
            Name = "transmitter solution",
            TLDR = "Quasi et quia est saepe debitis possimus aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-28T21:48:26.8312523+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 237,
            ProjectId = 45,
            PerformerId = 75,
            Name = "Sleek Frozen Chips",
            TLDR = "Corrupti delectus possimus sunt aut quam magnam non.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-12T23:01:17.5884793+00:00"),
            FinishedAt = DateTime.Parse("2021-05-13T12:25:55.4696704+00:00")
        },
        new TaskModel{
            Id = 238,
            ProjectId = 45,
            PerformerId = 91,
            Name = "Fully-configurable",
            TLDR = "Ipsam voluptatem voluptas in consequatur soluta voluptas.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-20T07:30:11.1190334+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 239,
            ProjectId = 45,
            PerformerId = 33,
            Name = "backing up neural Islands",
            TLDR = "Enim error odio ex rerum aliquid rerum et.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-28T22:32:04.0139807+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 240,
            ProjectId = 45,
            PerformerId = 42,
            Name = "AGP Lakes",
            TLDR = "Consequatur sed aperiam ipsum velit aut sed autem reiciendis et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-09T23:51:26.7918979+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 241,
            ProjectId = 46,
            PerformerId = 43,
            Name = "parsing Applications circuit",
            TLDR = "Esse veniam maxime repudiandae quia aut dolor.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-28T01:24:42.8073106+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 242,
            ProjectId = 46,
            PerformerId = 68,
            Name = "web-readiness",
            TLDR = "Cupiditate iste ut et tenetur quidem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-18T11:38:56.1081521+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 243,
            ProjectId = 46,
            PerformerId = 31,
            Name = "Investment Account Circles",
            TLDR = "Dignissimos facere consequuntur tempora quia.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-17T14:45:24.8231923+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 244,
            ProjectId = 46,
            PerformerId = 66,
            Name = "challenge Identity multi-byte",
            TLDR = "Et assumenda aspernatur quidem dolor facere.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-06T06:03:33.2939226+00:00"),
            FinishedAt = DateTime.Parse("2021-02-20T13:48:20.2550478+00:00")
        },
        new TaskModel{
            Id = 245,
            ProjectId = 46,
            PerformerId = 108,
            Name = "Washington turquoise",
            TLDR = "Quo rerum eveniet voluptatem.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-08T03:12:31.2380206+00:00"),
            FinishedAt = DateTime.Parse("2020-07-03T18:56:30.9348948+00:00")
        },
        new TaskModel{
            Id = 246,
            ProjectId = 46,
            PerformerId = 73,
            Name = "sky blue Massachusetts enterprise",
            TLDR = "Ipsa quasi iste nemo sed aut aut autem.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-01T15:37:35.238332+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 247,
            ProjectId = 46,
            PerformerId = 117,
            Name = "San Marino backing up",
            TLDR = "Quo saepe odio.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-13T17:13:05.5504838+00:00"),
            FinishedAt = DateTime.Parse("2020-07-16T03:34:09.8053894+00:00")
        },
        new TaskModel{
            Id = 248,
            ProjectId = 47,
            PerformerId = 89,
            Name = "grey Customer",
            TLDR = "Est sint eos.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-11T19:13:06.5487327+00:00"),
            FinishedAt = DateTime.Parse("2021-04-20T05:03:01.848295+00:00")
        },
        new TaskModel{
            Id = 249,
            ProjectId = 47,
            PerformerId = 23,
            Name = "South Africa Glen generating",
            TLDR = "Expedita assumenda porro ea et ipsa facere.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-01T15:45:18.1332727+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 250,
            ProjectId = 47,
            PerformerId = 83,
            Name = "orchestrate",
            TLDR = "Earum dolores eaque quaerat sint nam consequuntur est eaque beatae.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-03T07:53:18.5087646+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 251,
            ProjectId = 47,
            PerformerId = 86,
            Name = "Automotive Port French Southern Territories",
            TLDR = "Recusandae repudiandae rerum.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-24T11:50:07.9060918+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 252,
            ProjectId = 47,
            PerformerId = 50,
            Name = "XML Licensed Plastic Soap",
            TLDR = "Rerum quasi commodi dolorem asperiores eum doloribus suscipit aut cupiditate.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-04T01:33:00.7983252+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 253,
            ProjectId = 47,
            PerformerId = 52,
            Name = "parsing clicks-and-mortar",
            TLDR = "Aut voluptatibus est dolor placeat aut placeat eos et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-19T07:32:38.4404178+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 254,
            ProjectId = 48,
            PerformerId = 79,
            Name = "Steel",
            TLDR = "Molestiae omnis illo qui placeat et et.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-01T11:43:54.2812843+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 255,
            ProjectId = 48,
            PerformerId = 103,
            Name = "Savings Account California invoice",
            TLDR = "Autem labore adipisci voluptatum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-30T05:44:59.0350071+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 256,
            ProjectId = 48,
            PerformerId = 115,
            Name = "neutral Forward Intelligent",
            TLDR = "Quo earum ad.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-29T10:34:28.3367464+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 257,
            ProjectId = 48,
            PerformerId = 38,
            Name = "circuit port",
            TLDR = "Corrupti voluptatem qui quia nobis ex.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-28T01:03:15.0030733+00:00"),
            FinishedAt = DateTime.Parse("2020-08-27T22:08:31.4764283+00:00")
        },
        new TaskModel{
            Id = 258,
            ProjectId = 48,
            PerformerId = 64,
            Name = "Trinidad and Tobago Dollar quantifying",
            TLDR = "Sed provident doloribus quos aliquam minus quo commodi asperiores eaque.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-13T22:28:04.8619006+00:00"),
            FinishedAt = DateTime.Parse("2021-03-06T01:39:17.3720791+00:00")
        },
        new TaskModel{
            Id = 259,
            ProjectId = 48,
            PerformerId = 35,
            Name = "feed real-time",
            TLDR = "Sunt animi qui accusamus rem sit vitae.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-12-15T15:42:00.9517963+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 260,
            ProjectId = 48,
            PerformerId = 42,
            Name = "Borders scale",
            TLDR = "Incidunt possimus odio blanditiis sit facilis non minima aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-07T11:22:15.5586944+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 261,
            ProjectId = 48,
            PerformerId = 76,
            Name = "Licensed",
            TLDR = "Eos sed sit ut et officiis accusamus.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-15T11:53:48.5754627+00:00"),
            FinishedAt = DateTime.Parse("2020-10-29T10:07:14.7386866+00:00")
        },
        new TaskModel{
            Id = 262,
            ProjectId = 49,
            PerformerId = 66,
            Name = "silver Lead",
            TLDR = "Aut harum quam.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-23T09:50:35.2177791+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 263,
            ProjectId = 49,
            PerformerId = 58,
            Name = "quantifying hacking Buckinghamshire",
            TLDR = "Labore harum placeat asperiores odit error qui accusamus repellat ad.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-13T08:40:20.3895428+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 264,
            ProjectId = 49,
            PerformerId = 86,
            Name = "Bahamas",
            TLDR = "Dolor amet iure blanditiis eum aspernatur minima voluptates voluptatem expedita.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-20T22:30:23.7710181+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 265,
            ProjectId = 49,
            PerformerId = 91,
            Name = "orchestrate Beauty, Movies & Toys navigate",
            TLDR = "Nihil quis ut velit sed enim beatae atque.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-19T22:04:23.9699455+00:00"),
            FinishedAt = DateTime.Parse("2020-09-25T03:31:36.3660233+00:00")
        },
        new TaskModel{
            Id = 266,
            ProjectId = 49,
            PerformerId = 38,
            Name = "Internal payment mint green",
            TLDR = "Aliquam nam reprehenderit possimus veniam a.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-18T20:28:58.3387243+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 267,
            ProjectId = 49,
            PerformerId = 85,
            Name = "Rustic Cotton Chips invoice",
            TLDR = "Dolores impedit quibusdam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-04T15:21:15.3126079+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 268,
            ProjectId = 49,
            PerformerId = 44,
            Name = "Refined",
            TLDR = "Suscipit natus eveniet in ipsa saepe.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-13T04:24:03.9442483+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 269,
            ProjectId = 49,
            PerformerId = 114,
            Name = "transform",
            TLDR = "Odit quod quas fugiat.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-06-09T19:51:58.9897509+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 270,
            ProjectId = 50,
            PerformerId = 67,
            Name = "Radial Handmade",
            TLDR = "Dicta error facilis corporis beatae.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-16T17:57:30.9177664+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 271,
            ProjectId = 50,
            PerformerId = 34,
            Name = "Ohio 1080p Internal",
            TLDR = "Vitae et fuga et accusamus quod et eos sed.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-30T07:17:03.8574375+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 272,
            ProjectId = 50,
            PerformerId = 104,
            Name = "Stream Buckinghamshire back-end",
            TLDR = "Delectus sequi minus officiis voluptas omnis consequatur maiores nihil quas.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-23T09:43:24.9921121+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 273,
            ProjectId = 50,
            PerformerId = 97,
            Name = "European Monetary Unit (E.M.U.-6) Intelligent Steel Pizza Philippine Peso",
            TLDR = "Beatae soluta aliquam sapiente veniam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-23T20:56:45.4956387+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 274,
            ProjectId = 50,
            PerformerId = 74,
            Name = "Zambia International",
            TLDR = "Dolores ut eos debitis eaque vel rerum soluta voluptatibus.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-20T13:53:20.3455568+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 275,
            ProjectId = 50,
            PerformerId = 110,
            Name = "Corner",
            TLDR = "Omnis accusamus facere accusantium quam laboriosam provident.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-07T13:22:32.4251905+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 276,
            ProjectId = 50,
            PerformerId = 55,
            Name = "Garden & Automotive Home Loan Account compress",
            TLDR = "Eaque sunt reiciendis quas nisi.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-19T02:30:20.8719848+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 277,
            ProjectId = 50,
            PerformerId = 21,
            Name = "platforms primary",
            TLDR = "Vel et ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-27T12:47:20.1289892+00:00"),
            FinishedAt = DateTime.Parse("2020-07-08T11:18:56.8999775+00:00")
        },
        new TaskModel{
            Id = 278,
            ProjectId = 51,
            PerformerId = 86,
            Name = "cross-platform",
            TLDR = "Sapiente sint rerum sit ipsam dolores fugiat.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-08T17:43:29.4926408+00:00"),
            FinishedAt = DateTime.Parse("2021-06-05T12:35:17.0158297+00:00")
        },
        new TaskModel{
            Id = 279,
            ProjectId = 51,
            PerformerId = 122,
            Name = "maximized",
            TLDR = "Voluptatem molestiae veniam et unde.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-14T07:53:36.7828805+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 280,
            ProjectId = 51,
            PerformerId = 62,
            Name = "architecture",
            TLDR = "Suscipit molestiae non recusandae.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-01T21:29:50.0515347+00:00"),
            FinishedAt = DateTime.Parse("2021-03-02T02:51:18.8492747+00:00")
        },
        new TaskModel{
            Id = 281,
            ProjectId = 51,
            PerformerId = 33,
            Name = "Generic",
            TLDR = "Et veritatis eveniet aut consequuntur nihil consequatur soluta dolores porro.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-05T13:21:08.0954939+00:00"),
            FinishedAt = DateTime.Parse("2020-07-10T23:33:28.6939354+00:00")
        },
        new TaskModel{
            Id = 282,
            ProjectId = 52,
            PerformerId = 35,
            Name = "Frozen Shoes Health, Health & Home",
            TLDR = "Veniam eum magni.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-12-07T00:55:58.1689669+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 283,
            ProjectId = 52,
            PerformerId = 54,
            Name = "Principal matrices Markets",
            TLDR = "Dolorum quidem in.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-26T11:47:57.7944157+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 284,
            ProjectId = 52,
            PerformerId = 21,
            Name = "Practical",
            TLDR = "Sint aliquid voluptate laboriosam.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-12-13T20:33:38.529098+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 285,
            ProjectId = 54,
            PerformerId = 82,
            Name = "zero administration applications Generic Cotton Bike",
            TLDR = "Ut sunt pariatur necessitatibus dolor.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-18T01:20:19.1353213+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 286,
            ProjectId = 54,
            PerformerId = 90,
            Name = "copy",
            TLDR = "In corrupti dolorum ducimus et vero.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-27T14:23:13.285903+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 287,
            ProjectId = 54,
            PerformerId = 107,
            Name = "Mobility cutting-edge Niger",
            TLDR = "Necessitatibus rerum voluptatem natus.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-17T15:26:02.4207296+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 288,
            ProjectId = 54,
            PerformerId = 28,
            Name = "tan Usability Alabama",
            TLDR = "Perspiciatis sit non iure et.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-26T11:45:56.8447544+00:00"),
            FinishedAt = DateTime.Parse("2021-02-18T21:09:55.3200905+00:00")
        },
        new TaskModel{
            Id = 289,
            ProjectId = 54,
            PerformerId = 23,
            Name = "Cotton AGP",
            TLDR = "Blanditiis natus molestiae rem.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-25T04:06:58.2853961+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 290,
            ProjectId = 54,
            PerformerId = 51,
            Name = "Toys Trinidad and Tobago AI",
            TLDR = "Aut corporis ut sed qui est consequatur et id.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-29T00:37:30.6514214+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 291,
            ProjectId = 54,
            PerformerId = 41,
            Name = "Tools Belgium",
            TLDR = "Ea autem in eius quos eius quibusdam assumenda ullam.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-27T09:05:01.3641211+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 292,
            ProjectId = 54,
            PerformerId = 108,
            Name = "cross-platform application Bedfordshire",
            TLDR = "Omnis laudantium voluptas nemo.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-12T20:46:51.0719339+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 293,
            ProjectId = 54,
            PerformerId = 72,
            Name = "visionary Personal Loan Account",
            TLDR = "Eveniet sit et quia sequi iusto minima et maxime reiciendis.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-01T08:04:22.3157521+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 294,
            ProjectId = 55,
            PerformerId = 96,
            Name = "deposit Personal Loan Account",
            TLDR = "Sit ea ad incidunt dolorem fuga.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-22T17:51:39.4206463+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 295,
            ProjectId = 55,
            PerformerId = 23,
            Name = "Lake global Handmade Granite Soap",
            TLDR = "Sit praesentium accusamus.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-11T01:11:01.4284243+00:00"),
            FinishedAt = DateTime.Parse("2020-09-16T13:33:53.1915947+00:00")
        },
        new TaskModel{
            Id = 296,
            ProjectId = 55,
            PerformerId = 84,
            Name = "Intuitive access Cambodia",
            TLDR = "Non et est.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-05T10:59:08.4261842+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 297,
            ProjectId = 55,
            PerformerId = 30,
            Name = "incentivize Integrated",
            TLDR = "Mollitia ducimus magni suscipit quis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-28T14:57:39.794551+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 298,
            ProjectId = 55,
            PerformerId = 26,
            Name = "Rest Cambridgeshire bus",
            TLDR = "Voluptatem voluptatem sit eos voluptatem dolorem vel placeat vitae ex.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-17T18:36:19.120983+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 299,
            ProjectId = 55,
            PerformerId = 44,
            Name = "Personal Loan Account magenta",
            TLDR = "Nostrum dicta cupiditate consequatur.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-02T17:01:44.5035437+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 300,
            ProjectId = 55,
            PerformerId = 97,
            Name = "Global",
            TLDR = "Aut beatae voluptatem blanditiis ullam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-23T18:55:44.4252479+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 301,
            ProjectId = 56,
            PerformerId = 35,
            Name = "Connecticut Fundamental SCSI",
            TLDR = "Dolorem optio aut incidunt consequatur magni id.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-04T17:51:56.3239506+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 302,
            ProjectId = 56,
            PerformerId = 30,
            Name = "Dam deposit",
            TLDR = "Sit expedita debitis nesciunt eligendi.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-30T23:07:31.639839+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 303,
            ProjectId = 56,
            PerformerId = 120,
            Name = "Incredible PCI facilitate",
            TLDR = "Alias nulla non voluptas eligendi quis aperiam dignissimos.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-27T06:33:14.1468683+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 304,
            ProjectId = 56,
            PerformerId = 61,
            Name = "Small Metal Bike deposit",
            TLDR = "Qui minus odio.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-22T14:19:08.982094+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 305,
            ProjectId = 56,
            PerformerId = 100,
            Name = "driver Sleek",
            TLDR = "Enim quia vero sapiente quas consequuntur magnam vitae.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-06-12T02:12:54.9418228+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 306,
            ProjectId = 56,
            PerformerId = 47,
            Name = "concept",
            TLDR = "Optio nisi quidem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-30T16:59:29.2041901+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 307,
            ProjectId = 58,
            PerformerId = 87,
            Name = "Producer redundant",
            TLDR = "Quo facere aut qui ad sequi aut facere quis est.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-20T04:21:59.559763+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 308,
            ProjectId = 58,
            PerformerId = 54,
            Name = "Polarised portal",
            TLDR = "Aspernatur sed aliquid laborum eveniet nemo voluptatum qui dicta aliquam.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-11T22:14:51.348088+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 309,
            ProjectId = 59,
            PerformerId = 64,
            Name = "connecting Trail",
            TLDR = "Eum voluptates nobis perferendis.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-03T03:53:21.4468248+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 310,
            ProjectId = 59,
            PerformerId = 34,
            Name = "Afghani hard drive",
            TLDR = "Non vel pariatur voluptate dolorem quis totam.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-30T15:12:21.6249059+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 311,
            ProjectId = 59,
            PerformerId = 25,
            Name = "synthesizing",
            TLDR = "Veniam repellat molestiae illum dignissimos non aut aut neque.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-04-26T23:22:47.9552321+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 312,
            ProjectId = 60,
            PerformerId = 89,
            Name = "circuit turquoise Azerbaijanian Manat",
            TLDR = "Rerum sint fugiat eius est dolorem voluptatem ipsum.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-16T17:37:38.5928532+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 313,
            ProjectId = 60,
            PerformerId = 21,
            Name = "bandwidth hybrid overriding",
            TLDR = "Ut ex omnis veritatis est omnis nemo consequuntur nam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-08T01:27:17.6198882+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 314,
            ProjectId = 60,
            PerformerId = 23,
            Name = "Credit Card Account Digitized Dynamic",
            TLDR = "Minus sunt repellat nam debitis perferendis.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-09T16:14:10.9604806+00:00"),
            FinishedAt = DateTime.Parse("2020-07-28T17:43:37.9224013+00:00")
        },
        new TaskModel{
            Id = 315,
            ProjectId = 60,
            PerformerId = 108,
            Name = "Factors Arizona Tasty Steel Sausages",
            TLDR = "Quaerat sunt nam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-07T10:48:34.6162268+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 316,
            ProjectId = 61,
            PerformerId = 78,
            Name = "withdrawal",
            TLDR = "Similique dolorum quia veritatis voluptatem ut exercitationem.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-17T05:42:05.9575803+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 317,
            ProjectId = 61,
            PerformerId = 62,
            Name = "Plastic Nevada",
            TLDR = "Eos rerum quia qui facilis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-04T03:46:15.142995+00:00"),
            FinishedAt = DateTime.Parse("2020-08-28T03:41:17.3482721+00:00")
        },
        new TaskModel{
            Id = 318,
            ProjectId = 61,
            PerformerId = 30,
            Name = "frictionless",
            TLDR = "Quia rem consequatur aliquid earum nulla explicabo recusandae.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-15T11:09:47.2264611+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 319,
            ProjectId = 61,
            PerformerId = 99,
            Name = "navigate withdrawal",
            TLDR = "Aut repellat excepturi qui reprehenderit molestiae reiciendis corporis dolore sunt.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-28T14:22:07.1043732+00:00"),
            FinishedAt = DateTime.Parse("2021-05-13T04:47:45.1738402+00:00")
        },
        new TaskModel{
            Id = 320,
            ProjectId = 61,
            PerformerId = 64,
            Name = "Representative Consultant",
            TLDR = "Impedit laudantium suscipit asperiores nostrum dignissimos.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-13T01:50:38.8802521+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 321,
            ProjectId = 62,
            PerformerId = 65,
            Name = "e-services Solomon Islands Dollar Tasty",
            TLDR = "Delectus perspiciatis nulla nulla ut unde possimus veritatis voluptatem laudantium.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-12-30T12:11:19.6811584+00:00"),
            FinishedAt = DateTime.Parse("2021-01-27T00:46:35.7912475+00:00")
        },
        new TaskModel{
            Id = 322,
            ProjectId = 62,
            PerformerId = 28,
            Name = "North Dakota",
            TLDR = "Odit rerum dolor aut vero velit sed.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-29T10:45:54.9380774+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 323,
            ProjectId = 62,
            PerformerId = 114,
            Name = "TCP",
            TLDR = "Iusto neque et corrupti est quod est qui est.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-13T11:03:10.2732862+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 324,
            ProjectId = 62,
            PerformerId = 83,
            Name = "back up",
            TLDR = "Delectus doloremque qui eos.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-30T21:50:35.8849306+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 325,
            ProjectId = 62,
            PerformerId = 34,
            Name = "Integration",
            TLDR = "A qui ipsa est.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-23T10:25:23.1931194+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 326,
            ProjectId = 62,
            PerformerId = 57,
            Name = "grey",
            TLDR = "Aut eum et temporibus ea suscipit officiis quia et reiciendis.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-13T02:54:36.6630933+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 327,
            ProjectId = 63,
            PerformerId = 87,
            Name = "Borders Buckinghamshire",
            TLDR = "Possimus excepturi ut qui nulla.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-11T02:12:19.0013098+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 328,
            ProjectId = 64,
            PerformerId = 70,
            Name = "invoice pink generating",
            TLDR = "Ipsa illo esse laboriosam architecto.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-25T08:59:40.4011357+00:00"),
            FinishedAt = DateTime.Parse("2020-07-06T22:37:55.6168603+00:00")
        },
        new TaskModel{
            Id = 329,
            ProjectId = 64,
            PerformerId = 41,
            Name = "quantify HTTP Nuevo Sol",
            TLDR = "Illum numquam corrupti alias consequatur.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-25T08:57:06.1308997+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 330,
            ProjectId = 64,
            PerformerId = 88,
            Name = "holistic",
            TLDR = "Est consequatur dolores omnis numquam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-15T17:07:25.7591187+00:00"),
            FinishedAt = DateTime.Parse("2021-04-20T14:57:38.1054805+00:00")
        },
        new TaskModel{
            Id = 331,
            ProjectId = 64,
            PerformerId = 78,
            Name = "RAM parsing maximize",
            TLDR = "Delectus corporis eveniet provident dolorum dolores ea incidunt placeat.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-07T21:30:58.6321177+00:00"),
            FinishedAt = DateTime.Parse("2020-11-21T10:50:57.5299898+00:00")
        },
        new TaskModel{
            Id = 332,
            ProjectId = 64,
            PerformerId = 114,
            Name = "monitor Brazil",
            TLDR = "Placeat minima quos quis sed sint et.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-28T06:59:59.3984081+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 333,
            ProjectId = 64,
            PerformerId = 41,
            Name = "Cambridgeshire collaborative",
            TLDR = "Quisquam accusantium officiis est perspiciatis laudantium magni.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-16T07:17:42.5456952+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 334,
            ProjectId = 64,
            PerformerId = 33,
            Name = "user-centric transmitting",
            TLDR = "Rem perferendis omnis laborum ut non.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-10T15:18:03.328701+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 335,
            ProjectId = 64,
            PerformerId = 41,
            Name = "Jewelery Licensed Wooden Chicken",
            TLDR = "Id accusantium sequi provident aut consequatur eum asperiores sit eos.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-16T08:44:21.4574521+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 336,
            ProjectId = 64,
            PerformerId = 108,
            Name = "primary Incredible Granite Chair purple",
            TLDR = "Qui rerum saepe suscipit quia sed.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-19T21:38:23.3749334+00:00"),
            FinishedAt = DateTime.Parse("2021-05-03T23:55:12.418724+00:00")
        },
        new TaskModel{
            Id = 337,
            ProjectId = 64,
            PerformerId = 95,
            Name = "AGP Vista",
            TLDR = "Labore velit qui expedita.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-03T10:24:01.3202367+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 338,
            ProjectId = 65,
            PerformerId = 57,
            Name = "Incredible Cotton Fish backing up Generic Cotton Shoes",
            TLDR = "Quia id natus esse qui nihil esse quasi.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-01T09:33:16.893468+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 339,
            ProjectId = 65,
            PerformerId = 50,
            Name = "Sleek Metal Hat Practical Cambridgeshire",
            TLDR = "Quod quibusdam blanditiis perferendis animi nulla aspernatur asperiores mollitia.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-11T12:26:14.0026832+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 340,
            ProjectId = 65,
            PerformerId = 29,
            Name = "Refined Steel Bacon",
            TLDR = "Dolor itaque dolores magni voluptates et provident molestiae aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-13T10:30:49.3283975+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 341,
            ProjectId = 65,
            PerformerId = 97,
            Name = "Money Market Account New Taiwan Dollar Monitored",
            TLDR = "Et iste porro id molestias iure atque.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-24T06:50:15.0505306+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 342,
            ProjectId = 65,
            PerformerId = 21,
            Name = "bluetooth scalable virtual",
            TLDR = "Aut enim culpa in autem repellendus ad et.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-16T21:06:48.1980331+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 343,
            ProjectId = 65,
            PerformerId = 58,
            Name = "Pass initiatives budgetary management",
            TLDR = "Dolor et optio quas voluptatem beatae perferendis sapiente tenetur deserunt.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-06-05T19:55:58.4625854+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 344,
            ProjectId = 65,
            PerformerId = 39,
            Name = "throughput bandwidth ADP",
            TLDR = "Eos fugit est.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-06-19T21:13:14.4246946+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 345,
            ProjectId = 65,
            PerformerId = 115,
            Name = "Handmade Wooden Bike SCSI",
            TLDR = "Impedit voluptatem expedita earum sunt eius expedita.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-12T20:01:33.59859+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 346,
            ProjectId = 65,
            PerformerId = 29,
            Name = "Burg",
            TLDR = "Laboriosam aut natus.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-04-20T18:42:44.4113281+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 347,
            ProjectId = 66,
            PerformerId = 24,
            Name = "Rwanda Sleek Frozen Computer",
            TLDR = "Esse perferendis pariatur quia dolores atque.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-21T23:29:01.6613633+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 348,
            ProjectId = 66,
            PerformerId = 32,
            Name = "wireless Marketing",
            TLDR = "Omnis ut quia quas repudiandae harum et omnis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-12T00:56:33.1820309+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 349,
            ProjectId = 66,
            PerformerId = 75,
            Name = "instruction set invoice",
            TLDR = "Dolor officiis nulla non facilis recusandae natus.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-18T06:06:53.7412068+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 350,
            ProjectId = 66,
            PerformerId = 118,
            Name = "Lakes synthesize",
            TLDR = "Molestiae quas eum ut et animi similique sed ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-29T11:31:04.0424456+00:00"),
            FinishedAt = DateTime.Parse("2021-01-08T13:33:14.4187426+00:00")
        },
        new TaskModel{
            Id = 351,
            ProjectId = 66,
            PerformerId = 118,
            Name = "PCI",
            TLDR = "Voluptas sint impedit soluta similique aliquid sint explicabo consequatur tempore.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-11T07:20:31.0237704+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 352,
            ProjectId = 66,
            PerformerId = 96,
            Name = "monitor circuit",
            TLDR = "Dignissimos dolore a odio qui ipsa distinctio cumque ducimus beatae.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-05T14:03:36.1299258+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 353,
            ProjectId = 66,
            PerformerId = 107,
            Name = "Fantastic",
            TLDR = "Et qui veritatis non.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-17T17:33:00.0048136+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 354,
            ProjectId = 66,
            PerformerId = 93,
            Name = "Intuitive",
            TLDR = "Adipisci sit quia tempore facere labore ipsum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-19T23:16:22.139782+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 355,
            ProjectId = 66,
            PerformerId = 81,
            Name = "Quality Cambridgeshire Ports",
            TLDR = "Nesciunt enim aut vel ipsam laboriosam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-12T11:28:09.1645879+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 356,
            ProjectId = 67,
            PerformerId = 43,
            Name = "Avon Money Market Account white",
            TLDR = "Amet nulla perferendis placeat qui suscipit minus dolores sed.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-03T18:49:27.684004+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 357,
            ProjectId = 67,
            PerformerId = 22,
            Name = "SSL Identity",
            TLDR = "Accusamus asperiores libero ut non est.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-10T04:28:56.1458918+00:00"),
            FinishedAt = DateTime.Parse("2021-01-31T11:14:03.5148843+00:00")
        },
        new TaskModel{
            Id = 358,
            ProjectId = 67,
            PerformerId = 26,
            Name = "Lempira",
            TLDR = "Velit ipsam ullam illo labore voluptatibus est ut rerum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-11T02:37:30.596648+00:00"),
            FinishedAt = DateTime.Parse("2021-05-05T20:43:17.6952993+00:00")
        },
        new TaskModel{
            Id = 359,
            ProjectId = 67,
            PerformerId = 89,
            Name = "Rustic niches",
            TLDR = "Voluptas voluptatibus consectetur quia aut qui qui voluptate nisi.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-03T10:12:31.6620993+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 360,
            ProjectId = 67,
            PerformerId = 53,
            Name = "bypassing Dam Bhutanese Ngultrum",
            TLDR = "Harum aliquid delectus est consequuntur perferendis quis enim quibusdam.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-02T17:58:09.8322066+00:00"),
            FinishedAt = DateTime.Parse("2020-09-16T07:09:50.599085+00:00")
        },
        new TaskModel{
            Id = 361,
            ProjectId = 68,
            PerformerId = 104,
            Name = "Concrete",
            TLDR = "Repellendus in ut accusantium.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-19T15:22:44.824019+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 362,
            ProjectId = 68,
            PerformerId = 115,
            Name = "Designer Progressive Camp",
            TLDR = "Voluptas non laboriosam eligendi iusto.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-10T22:29:05.7288581+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 363,
            ProjectId = 68,
            PerformerId = 22,
            Name = "programming",
            TLDR = "Delectus reprehenderit molestiae sint autem sed est sit quae.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-14T10:07:32.115712+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 364,
            ProjectId = 68,
            PerformerId = 54,
            Name = "partnerships",
            TLDR = "Dignissimos voluptatum dolorem non pariatur ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-05T16:26:31.3559361+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 365,
            ProjectId = 68,
            PerformerId = 56,
            Name = "calculate Strategist Garden",
            TLDR = "Nam quas quod expedita neque culpa ipsa ut est.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-26T10:34:21.3401967+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 366,
            ProjectId = 69,
            PerformerId = 61,
            Name = "deposit Maryland",
            TLDR = "Laboriosam corporis repellat eum sequi maiores voluptatem.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-09T05:22:41.8469326+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 367,
            ProjectId = 69,
            PerformerId = 29,
            Name = "reboot firewall",
            TLDR = "Ut dolores alias aut perspiciatis dignissimos hic sapiente hic.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-15T20:50:53.4645533+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 368,
            ProjectId = 69,
            PerformerId = 82,
            Name = "pink ivory backing up",
            TLDR = "Voluptatem magnam ipsum ea et ipsam animi.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-04T04:42:09.9736011+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 369,
            ProjectId = 69,
            PerformerId = 47,
            Name = "COM",
            TLDR = "Possimus iste debitis adipisci eaque autem omnis.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-16T03:35:58.8319419+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 370,
            ProjectId = 69,
            PerformerId = 35,
            Name = "matrix invoice",
            TLDR = "Voluptatum facere quidem sit autem quidem ea debitis.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-05T15:41:54.8266529+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 371,
            ProjectId = 69,
            PerformerId = 54,
            Name = "Auto Loan Account",
            TLDR = "Amet tenetur vel iste incidunt optio.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-26T06:28:23.704698+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 372,
            ProjectId = 69,
            PerformerId = 66,
            Name = "monitor Borders",
            TLDR = "Velit vel saepe in dolor et fugiat.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-27T04:06:58.3309108+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 373,
            ProjectId = 69,
            PerformerId = 86,
            Name = "Sleek",
            TLDR = "Nostrum aut est rerum fugit et illo laborum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-10T06:29:55.3111469+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 374,
            ProjectId = 69,
            PerformerId = 43,
            Name = "Handmade",
            TLDR = "Minus quidem aut et.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-13T11:09:01.8369435+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 375,
            ProjectId = 70,
            PerformerId = 40,
            Name = "Fantastic Cotton Car Lead Valleys",
            TLDR = "Natus corrupti dolorem possimus minima in vel assumenda.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-11T17:35:15.7965555+00:00"),
            FinishedAt = DateTime.Parse("2020-07-16T00:28:19.6166754+00:00")
        },
        new TaskModel{
            Id = 376,
            ProjectId = 70,
            PerformerId = 84,
            Name = "Landing",
            TLDR = "Quisquam quisquam nihil quibusdam aut sapiente.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-23T08:43:16.6279244+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 377,
            ProjectId = 70,
            PerformerId = 104,
            Name = "blockchains plum",
            TLDR = "Delectus facilis et totam.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-29T17:52:15.1569297+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 378,
            ProjectId = 70,
            PerformerId = 117,
            Name = "Field",
            TLDR = "Dolorem sint repellat mollitia quia.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-30T05:50:21.7518905+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 379,
            ProjectId = 71,
            PerformerId = 98,
            Name = "payment",
            TLDR = "Alias ea rerum qui eum dignissimos ad.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-30T23:48:40.9299114+00:00"),
            FinishedAt = DateTime.Parse("2021-04-17T17:54:35.9090674+00:00")
        },
        new TaskModel{
            Id = 380,
            ProjectId = 71,
            PerformerId = 89,
            Name = "Directives",
            TLDR = "Et voluptatem dolorem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-12T20:05:01.3833528+00:00"),
            FinishedAt = DateTime.Parse("2020-10-01T12:51:14.7411275+00:00")
        },
        new TaskModel{
            Id = 381,
            ProjectId = 71,
            PerformerId = 58,
            Name = "reboot Causeway Automated",
            TLDR = "Nisi quis necessitatibus saepe fuga qui.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-20T09:28:54.639957+00:00"),
            FinishedAt = DateTime.Parse("2020-09-29T02:08:24.7032785+00:00")
        },
        new TaskModel{
            Id = 382,
            ProjectId = 71,
            PerformerId = 113,
            Name = "Fresh Mountains",
            TLDR = "Veritatis a libero eligendi.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-20T01:32:55.2466931+00:00"),
            FinishedAt = DateTime.Parse("2021-03-04T07:37:30.3956038+00:00")
        },
        new TaskModel{
            Id = 383,
            ProjectId = 71,
            PerformerId = 21,
            Name = "Home Loan Account Licensed Soft Shirt Handmade Metal Tuna",
            TLDR = "Quia inventore ut atque autem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-16T01:57:59.1351757+00:00"),
            FinishedAt = DateTime.Parse("2020-06-25T00:35:52.5885304+00:00")
        },
        new TaskModel{
            Id = 384,
            ProjectId = 71,
            PerformerId = 38,
            Name = "Hawaii Reactive Bermuda",
            TLDR = "Ex reiciendis ipsa molestiae aut numquam alias.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-08T05:06:38.4765817+00:00"),
            FinishedAt = DateTime.Parse("2020-07-12T16:36:38.2319306+00:00")
        },
        new TaskModel{
            Id = 385,
            ProjectId = 73,
            PerformerId = 71,
            Name = "Frozen Vision-oriented",
            TLDR = "Quam amet eveniet.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-26T08:40:09.5346771+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 386,
            ProjectId = 73,
            PerformerId = 112,
            Name = "plug-and-play",
            TLDR = "Enim nostrum atque.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-04-17T05:02:59.8191029+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 387,
            ProjectId = 73,
            PerformerId = 53,
            Name = "Minnesota quantifying Paradigm",
            TLDR = "Velit odio voluptatem consequatur impedit cupiditate cum autem.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-05T00:50:46.465583+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 388,
            ProjectId = 73,
            PerformerId = 24,
            Name = "Assurance",
            TLDR = "Temporibus voluptas tempora.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-06-05T11:21:22.0348671+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 389,
            ProjectId = 73,
            PerformerId = 57,
            Name = "Refined Soft Car virtual back-end",
            TLDR = "Fuga quasi et velit placeat et.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-22T18:32:34.7050543+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 390,
            ProjectId = 74,
            PerformerId = 48,
            Name = "Row transmitting",
            TLDR = "Vitae distinctio iusto quae.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-12T01:30:52.519245+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 391,
            ProjectId = 74,
            PerformerId = 101,
            Name = "bandwidth target",
            TLDR = "Voluptas ipsam rerum aspernatur autem sit est et ipsa soluta.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-02-04T16:03:17.041273+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 392,
            ProjectId = 74,
            PerformerId = 48,
            Name = "tan",
            TLDR = "Minus et ipsa in.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-17T12:46:01.1128665+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 393,
            ProjectId = 74,
            PerformerId = 104,
            Name = "Aruba Avon",
            TLDR = "Soluta quis sunt id laudantium quia.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-21T02:26:10.3435886+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 394,
            ProjectId = 74,
            PerformerId = 67,
            Name = "payment Nauru mint green",
            TLDR = "Possimus iure quibusdam id.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-25T01:56:56.8421749+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 395,
            ProjectId = 74,
            PerformerId = 100,
            Name = "New Caledonia Assurance",
            TLDR = "A asperiores et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-15T06:17:08.124149+00:00"),
            FinishedAt = DateTime.Parse("2021-06-07T08:32:19.1831962+00:00")
        },
        new TaskModel{
            Id = 396,
            ProjectId = 75,
            PerformerId = 96,
            Name = "olive Row",
            TLDR = "Enim et quia rerum minima voluptatem alias aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-29T03:57:33.829562+00:00"),
            FinishedAt = DateTime.Parse("2020-11-17T17:07:41.9814929+00:00")
        },
        new TaskModel{
            Id = 397,
            ProjectId = 75,
            PerformerId = 67,
            Name = "Croatian Kuna USB",
            TLDR = "Quia rerum assumenda occaecati eligendi quisquam quia.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-11T10:09:58.0114102+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 398,
            ProjectId = 76,
            PerformerId = 104,
            Name = "zero administration Borders",
            TLDR = "Placeat nobis ea alias.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-29T10:59:06.2274598+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 399,
            ProjectId = 76,
            PerformerId = 79,
            Name = "Lakes connect",
            TLDR = "Enim sunt necessitatibus autem.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-17T00:10:43.2506206+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 400,
            ProjectId = 76,
            PerformerId = 104,
            Name = "driver generate",
            TLDR = "Natus numquam assumenda nobis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-15T23:48:33.7446132+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 401,
            ProjectId = 76,
            PerformerId = 91,
            Name = "orchestrate",
            TLDR = "Repellendus et incidunt vel eaque est quasi.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-04T06:38:23.48404+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 402,
            ProjectId = 76,
            PerformerId = 80,
            Name = "Home Loan Account withdrawal interfaces",
            TLDR = "Qui rerum ea.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-18T00:32:19.4852119+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 403,
            ProjectId = 77,
            PerformerId = 66,
            Name = "transmitter",
            TLDR = "Modi quaerat eveniet quia aliquid maiores.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-05T00:38:07.0284139+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 404,
            ProjectId = 77,
            PerformerId = 74,
            Name = "Tasty purple",
            TLDR = "Nisi et consequatur qui qui eveniet error officia exercitationem provident.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-05T13:27:30.1255228+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 405,
            ProjectId = 77,
            PerformerId = 119,
            Name = "Licensed Cotton Chicken Zimbabwe",
            TLDR = "Molestiae omnis qui esse dolorem voluptatem.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-06-26T22:36:16.0266618+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 406,
            ProjectId = 77,
            PerformerId = 67,
            Name = "Tasty Plastic Car harness Arizona",
            TLDR = "Fugiat asperiores enim qui sunt et.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-26T00:57:19.1743673+00:00"),
            FinishedAt = DateTime.Parse("2021-04-01T11:41:01.2412609+00:00")
        },
        new TaskModel{
            Id = 407,
            ProjectId = 77,
            PerformerId = 67,
            Name = "one-to-one engage",
            TLDR = "Atque quod voluptas ullam sit quia voluptatem ad.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-15T10:25:01.4891724+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 408,
            ProjectId = 77,
            PerformerId = 90,
            Name = "rich",
            TLDR = "Voluptatibus mollitia iste consequuntur eum magnam praesentium illo nostrum quis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-06-28T09:17:34.9697006+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 409,
            ProjectId = 78,
            PerformerId = 93,
            Name = "HDD",
            TLDR = "Omnis iusto quo aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-06-26T05:43:16.0901353+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 410,
            ProjectId = 78,
            PerformerId = 29,
            Name = "back-end Berkshire",
            TLDR = "Dolor aperiam fuga et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-03T16:08:15.2427489+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 411,
            ProjectId = 78,
            PerformerId = 97,
            Name = "intangible",
            TLDR = "Sapiente eos ex.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-27T09:30:58.7476858+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 412,
            ProjectId = 78,
            PerformerId = 76,
            Name = "Creek logistical Versatile",
            TLDR = "Culpa aut quas aut est quo soluta impedit tempora.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-28T18:22:42.5827497+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 413,
            ProjectId = 78,
            PerformerId = 118,
            Name = "Armenian Dram Money Market Account Directives",
            TLDR = "Ea nihil tempora eligendi eveniet ducimus dolores quis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-24T10:14:36.7694515+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 414,
            ProjectId = 78,
            PerformerId = 37,
            Name = "index",
            TLDR = "Quia praesentium facilis et est iusto.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-06-25T21:24:46.7321511+00:00"),
            FinishedAt = DateTime.Parse("2020-08-20T15:18:02.8585327+00:00")
        },
        new TaskModel{
            Id = 415,
            ProjectId = 79,
            PerformerId = 62,
            Name = "Metrics",
            TLDR = "Similique non cupiditate.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-28T19:20:21.4345198+00:00"),
            FinishedAt = DateTime.Parse("2020-08-07T11:19:58.1523429+00:00")
        },
        new TaskModel{
            Id = 416,
            ProjectId = 79,
            PerformerId = 116,
            Name = "fuchsia",
            TLDR = "Quo labore minus autem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-28T11:53:02.3899403+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 417,
            ProjectId = 79,
            PerformerId = 34,
            Name = "Pennsylvania Pound Sterling",
            TLDR = "Vero aut aut beatae aut ut et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-30T13:16:41.9996783+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 418,
            ProjectId = 79,
            PerformerId = 120,
            Name = "China Mandatory",
            TLDR = "Praesentium est molestiae.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-01T13:12:37.7684998+00:00"),
            FinishedAt = DateTime.Parse("2021-05-04T23:09:11.7364289+00:00")
        },
        new TaskModel{
            Id = 419,
            ProjectId = 79,
            PerformerId = 69,
            Name = "program Antarctica (the territory South of 60 deg S)",
            TLDR = "Vel non enim inventore.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-25T23:59:27.5163701+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 420,
            ProjectId = 79,
            PerformerId = 42,
            Name = "Director",
            TLDR = "Dolor tenetur ipsa expedita a enim odit.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-21T02:41:04.0767911+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 421,
            ProjectId = 79,
            PerformerId = 97,
            Name = "Planner Falls 1080p",
            TLDR = "Veritatis consequatur qui et rem non.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-27T07:20:34.6341199+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 422,
            ProjectId = 80,
            PerformerId = 67,
            Name = "Prairie Nepal Refined",
            TLDR = "Et atque natus reiciendis.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-14T12:21:51.2707168+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 423,
            ProjectId = 80,
            PerformerId = 75,
            Name = "Electronics",
            TLDR = "Quos consequatur et et sit placeat rem vel iure perferendis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-27T12:29:17.4339726+00:00"),
            FinishedAt = DateTime.Parse("2020-10-18T17:58:35.4640499+00:00")
        },
        new TaskModel{
            Id = 424,
            ProjectId = 80,
            PerformerId = 46,
            Name = "Refined Wooden Sausages",
            TLDR = "Qui in dolore rem et ipsum vitae.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-15T21:13:36.3570239+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 425,
            ProjectId = 80,
            PerformerId = 63,
            Name = "primary",
            TLDR = "Accusantium illum et consequuntur est.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-22T01:17:11.2299374+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 426,
            ProjectId = 80,
            PerformerId = 71,
            Name = "GB",
            TLDR = "Laudantium voluptates corporis eligendi et repudiandae.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-12T09:50:13.908407+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 427,
            ProjectId = 80,
            PerformerId = 88,
            Name = "Fantastic Soft Shoes",
            TLDR = "Neque dolorem consequatur ipsa perferendis amet.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-15T10:06:47.7912176+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 428,
            ProjectId = 80,
            PerformerId = 92,
            Name = "Texas mint green",
            TLDR = "Unde enim sed natus voluptatem.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-09T20:27:27.2708387+00:00"),
            FinishedAt = DateTime.Parse("2021-04-18T19:27:02.0688152+00:00")
        },
        new TaskModel{
            Id = 429,
            ProjectId = 82,
            PerformerId = 67,
            Name = "Handmade Soft Tuna Rustic",
            TLDR = "Sequi molestiae qui est quos hic nesciunt et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-24T14:35:17.153504+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 430,
            ProjectId = 82,
            PerformerId = 67,
            Name = "transmit Berkshire IB",
            TLDR = "Eum quae non minima aliquid aut quis voluptas tenetur maxime.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-29T06:06:26.5476575+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 431,
            ProjectId = 82,
            PerformerId = 108,
            Name = "Plaza",
            TLDR = "Nesciunt maxime omnis ut sint id alias.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-25T00:52:43.0066669+00:00"),
            FinishedAt = DateTime.Parse("2021-03-09T00:18:00.5533071+00:00")
        },
        new TaskModel{
            Id = 432,
            ProjectId = 82,
            PerformerId = 47,
            Name = "orange",
            TLDR = "Voluptas eum occaecati dignissimos qui pariatur officiis libero.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-31T20:27:50.5338156+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 433,
            ProjectId = 82,
            PerformerId = 118,
            Name = "Legacy SMS needs-based",
            TLDR = "Et optio perferendis ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-05T10:54:29.3198791+00:00"),
            FinishedAt = DateTime.Parse("2021-01-24T14:13:59.1086722+00:00")
        },
        new TaskModel{
            Id = 434,
            ProjectId = 82,
            PerformerId = 110,
            Name = "Gateway extend Sri Lanka Rupee",
            TLDR = "Qui corrupti omnis quis beatae ullam dignissimos.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-28T19:24:22.0260918+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 435,
            ProjectId = 82,
            PerformerId = 59,
            Name = "incubate",
            TLDR = "Quidem voluptatum quaerat est quas ut autem odio officiis autem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-18T00:27:51.5819307+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 436,
            ProjectId = 82,
            PerformerId = 37,
            Name = "Dale",
            TLDR = "Tempora quos qui neque assumenda magnam est nostrum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-25T16:46:04.0206337+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 437,
            ProjectId = 82,
            PerformerId = 51,
            Name = "parallelism help-desk primary",
            TLDR = "Optio voluptatem velit veniam.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-03T03:40:36.2836035+00:00"),
            FinishedAt = DateTime.Parse("2020-06-23T18:37:53.7206809+00:00")
        },
        new TaskModel{
            Id = 438,
            ProjectId = 83,
            PerformerId = 110,
            Name = "Solutions Money Market Account",
            TLDR = "Labore ipsam quos iusto minus laborum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-04T15:29:21.3615678+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 439,
            ProjectId = 84,
            PerformerId = 78,
            Name = "metrics",
            TLDR = "Facilis beatae quas provident ipsum maiores debitis magnam aliquam.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-09-20T05:34:40.7691724+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 440,
            ProjectId = 84,
            PerformerId = 89,
            Name = "payment indexing Representative",
            TLDR = "Nisi et quia sint sequi.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-08T13:19:07.2835909+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 441,
            ProjectId = 84,
            PerformerId = 95,
            Name = "copy",
            TLDR = "Consequuntur qui ullam aliquam iusto labore sit vel qui inventore.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-09T22:23:37.850054+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 442,
            ProjectId = 84,
            PerformerId = 24,
            Name = "Human",
            TLDR = "Nostrum id debitis quia et eum tempore non eum.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-27T14:25:22.0902211+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 443,
            ProjectId = 84,
            PerformerId = 58,
            Name = "Oklahoma Idaho Oklahoma",
            TLDR = "Non quia voluptatem consequatur.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-07T18:44:25.1573324+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 444,
            ProjectId = 85,
            PerformerId = 105,
            Name = "Lead",
            TLDR = "Magnam dolores facilis nihil.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-07T09:28:12.4084392+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 445,
            ProjectId = 85,
            PerformerId = 22,
            Name = "architect Fork",
            TLDR = "Nam tempore voluptatem eos sed.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-04T22:04:08.880719+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 446,
            ProjectId = 85,
            PerformerId = 46,
            Name = "Architect New Jersey global",
            TLDR = "Id exercitationem quia voluptates consequatur.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-06T14:54:54.5763626+00:00"),
            FinishedAt = DateTime.Parse("2020-10-19T10:12:09.8482967+00:00")
        },
        new TaskModel{
            Id = 447,
            ProjectId = 85,
            PerformerId = 107,
            Name = "Jersey",
            TLDR = "Enim reiciendis eaque et nesciunt.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-01-05T10:23:37.5309289+00:00"),
            FinishedAt = DateTime.Parse("2020-09-12T12:35:08.3242226+00:00")
        },
        new TaskModel{
            Id = 448,
            ProjectId = 85,
            PerformerId = 120,
            Name = "wireless Unbranded Steel Salad",
            TLDR = "Et ea itaque.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-24T10:37:06.0382696+00:00"),
            FinishedAt = DateTime.Parse("2020-07-03T18:44:47.3209291+00:00")
        },
        new TaskModel{
            Id = 449,
            ProjectId = 85,
            PerformerId = 33,
            Name = "EXE Green mesh",
            TLDR = "In voluptates natus asperiores esse nam odio praesentium.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-07T00:08:54.9919132+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 450,
            ProjectId = 86,
            PerformerId = 31,
            Name = "generating Legacy",
            TLDR = "Voluptatem molestias quos ullam sit et voluptatem ullam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-05T10:35:12.375081+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 451,
            ProjectId = 86,
            PerformerId = 122,
            Name = "ROI streamline",
            TLDR = "Qui asperiores eveniet veniam reprehenderit laboriosam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-01T07:11:57.7916547+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 452,
            ProjectId = 86,
            PerformerId = 89,
            Name = "benchmark connecting ADP",
            TLDR = "Aut excepturi excepturi eos dolorem.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-13T02:26:11.8866977+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 453,
            ProjectId = 86,
            PerformerId = 43,
            Name = "supply-chains Minnesota object-oriented",
            TLDR = "Tempora aut sapiente aut expedita.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-15T01:35:07.3421885+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 454,
            ProjectId = 86,
            PerformerId = 42,
            Name = "Rustic Granite Pizza Steel Representative",
            TLDR = "Minus earum sint quia laudantium voluptatem earum illum adipisci.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-01T22:53:26.7428877+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 455,
            ProjectId = 86,
            PerformerId = 47,
            Name = "encoding Refined Steel Car",
            TLDR = "Debitis et aperiam officia dicta consectetur iste.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-27T22:48:19.7373142+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 456,
            ProjectId = 86,
            PerformerId = 109,
            Name = "maximized Money Market Account",
            TLDR = "Similique ut molestiae dolorem exercitationem rerum illo debitis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-12-03T11:36:22.9592035+00:00"),
            FinishedAt = DateTime.Parse("2020-12-12T14:29:15.2567614+00:00")
        },
        new TaskModel{
            Id = 457,
            ProjectId = 86,
            PerformerId = 114,
            Name = "Data",
            TLDR = "Veritatis et quis debitis.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-31T12:38:53.9276709+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 458,
            ProjectId = 86,
            PerformerId = 106,
            Name = "Shoes innovate",
            TLDR = "Eum modi odio repellendus sed quo ea qui.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-15T21:27:02.8038829+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 459,
            ProjectId = 87,
            PerformerId = 75,
            Name = "markets one-to-one",
            TLDR = "Aliquid laudantium optio illum consequatur officia sit voluptas.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-23T02:25:02.309618+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 460,
            ProjectId = 87,
            PerformerId = 102,
            Name = "Unbranded",
            TLDR = "Perferendis et ut molestias saepe nisi.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-18T12:50:57.9493834+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 461,
            ProjectId = 87,
            PerformerId = 50,
            Name = "Drive",
            TLDR = "Nesciunt aut corporis consequatur cupiditate dolorum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-18T20:47:13.717402+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 462,
            ProjectId = 87,
            PerformerId = 97,
            Name = "Refined Horizontal Manor",
            TLDR = "Illo ut officiis non praesentium sed aut ea est veniam.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-05-11T18:08:51.6961969+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 463,
            ProjectId = 87,
            PerformerId = 93,
            Name = "optical",
            TLDR = "Commodi officiis ut ut delectus asperiores eos fuga.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-07-15T19:36:04.2674228+00:00"),
            FinishedAt = DateTime.Parse("2020-12-06T20:12:05.4477361+00:00")
        },
        new TaskModel{
            Id = 464,
            ProjectId = 88,
            PerformerId = 106,
            Name = "deposit Gorgeous Soft Pizza",
            TLDR = "Omnis numquam consequatur et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-11T09:26:21.9456115+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 465,
            ProjectId = 88,
            PerformerId = 57,
            Name = "Mill bandwidth",
            TLDR = "Qui est et.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-25T18:28:11.4979179+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 466,
            ProjectId = 88,
            PerformerId = 109,
            Name = "mission-critical Fantastic Granite Towels Books, Health & Jewelery",
            TLDR = "Incidunt exercitationem fuga ut et perspiciatis veniam voluptatibus.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-10-25T20:11:56.4048278+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 467,
            ProjectId = 88,
            PerformerId = 85,
            Name = "niches",
            TLDR = "Cupiditate placeat tempora aliquid incidunt eveniet ratione.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-08-31T22:34:59.3734454+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 468,
            ProjectId = 88,
            PerformerId = 74,
            Name = "Bedfordshire function",
            TLDR = "Molestiae veniam voluptatibus necessitatibus hic quae aut.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-10-23T04:32:40.6187956+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 469,
            ProjectId = 88,
            PerformerId = 96,
            Name = "Handcrafted Cotton Shoes",
            TLDR = "Qui corporis repellat qui voluptatum harum aspernatur asperiores.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-05T23:27:20.7831322+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 470,
            ProjectId = 88,
            PerformerId = 108,
            Name = "next generation",
            TLDR = "Explicabo mollitia quidem quibusdam sit rem aut repellat veritatis itaque.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-11T16:46:26.5112287+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 471,
            ProjectId = 88,
            PerformerId = 24,
            Name = "Puerto Rico Generic Garden, Jewelery & Industrial",
            TLDR = "Molestiae consequuntur itaque modi voluptatum corrupti cupiditate.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-03T14:31:09.7121421+00:00"),
            FinishedAt = DateTime.Parse("2020-12-02T02:38:32.3633933+00:00")
        },
        new TaskModel{
            Id = 472,
            ProjectId = 88,
            PerformerId = 108,
            Name = "applications Coordinator models",
            TLDR = "Libero voluptas sunt quae voluptatibus nisi laudantium.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-12-16T22:43:47.9514945+00:00"),
            FinishedAt = DateTime.Parse("2021-05-12T03:17:17.6098709+00:00")
        },
        new TaskModel{
            Id = 473,
            ProjectId = 88,
            PerformerId = 49,
            Name = "transmitting",
            TLDR = "Ipsa aut veritatis quo laudantium aut et.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-12-31T20:19:22.3606318+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 474,
            ProjectId = 89,
            PerformerId = 52,
            Name = "Point",
            TLDR = "Expedita minima quis porro vero beatae.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-10T21:43:33.8070372+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 475,
            ProjectId = 89,
            PerformerId = 62,
            Name = "local",
            TLDR = "Sunt dolor temporibus.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-02-29T04:13:00.6778906+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 476,
            ProjectId = 89,
            PerformerId = 61,
            Name = "24/7 optical hack",
            TLDR = "Molestias quia eveniet officia similique omnis et amet et odit.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-08T09:30:24.7334417+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 477,
            ProjectId = 89,
            PerformerId = 50,
            Name = "deposit",
            TLDR = "Sed blanditiis quas exercitationem vel nihil.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-03T11:15:33.7261723+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 478,
            ProjectId = 89,
            PerformerId = 47,
            Name = "Fresh",
            TLDR = "Ut officiis consequatur voluptatibus iusto ut.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-05T17:43:00.1424804+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 479,
            ProjectId = 89,
            PerformerId = 83,
            Name = "Investment Account Intelligent Wooden Fish mission-critical",
            TLDR = "Consequatur doloremque dicta dolorem eligendi dolores.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-15T08:12:31.7653504+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 480,
            ProjectId = 90,
            PerformerId = 80,
            Name = "Berkshire Rwanda Franc",
            TLDR = "Tenetur distinctio vero velit et consectetur facilis voluptatem.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-08-30T17:10:47.0658393+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 481,
            ProjectId = 90,
            PerformerId = 57,
            Name = "upward-trending Internal secondary",
            TLDR = "Voluptate nisi accusantium voluptas modi temporibus adipisci eius.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-06-28T01:35:37.9487195+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 482,
            ProjectId = 90,
            PerformerId = 25,
            Name = "Communications",
            TLDR = "Sapiente saepe occaecati molestiae velit numquam qui quam est.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-07-31T10:01:14.0168289+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 483,
            ProjectId = 90,
            PerformerId = 79,
            Name = "XSS Kansas",
            TLDR = "Iusto voluptas in qui ipsa earum nobis earum est.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-07-25T09:33:35.9783371+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 484,
            ProjectId = 90,
            PerformerId = 29,
            Name = "empower concept Fresh",
            TLDR = "In dolorem vero maxime voluptate.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-27T23:57:42.7182712+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 485,
            ProjectId = 90,
            PerformerId = 97,
            Name = "navigate Granite",
            TLDR = "Iure aliquam provident.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-12T07:30:03.793091+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 486,
            ProjectId = 90,
            PerformerId = 23,
            Name = "Buckinghamshire",
            TLDR = "Sunt dolorum suscipit.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-21T17:25:08.0205316+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 487,
            ProjectId = 90,
            PerformerId = 79,
            Name = "internet solution Auto Loan Account",
            TLDR = "Sit aut tempore.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-26T00:04:52.0192024+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 488,
            ProjectId = 90,
            PerformerId = 43,
            Name = "Communications Mexican Peso Managed",
            TLDR = "Culpa repudiandae aliquid et accusantium quas quam iusto voluptas.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-24T20:47:25.4073508+00:00"),
            FinishedAt = DateTime.Parse("2020-08-14T04:31:29.7250544+00:00")
        },
        new TaskModel{
            Id = 489,
            ProjectId = 91,
            PerformerId = 28,
            Name = "National panel Island",
            TLDR = "Voluptas earum sed ut est vel animi voluptatem.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-11-28T04:00:48.453574+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 490,
            ProjectId = 91,
            PerformerId = 94,
            Name = "project Tools, Computers & Clothing",
            TLDR = "Ut voluptatibus placeat nulla ut cumque.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-06-22T02:34:49.090581+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 491,
            ProjectId = 91,
            PerformerId = 68,
            Name = "Fantastic Frozen Chicken auxiliary generating",
            TLDR = "Qui voluptas impedit commodi aspernatur est iste cumque atque.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-03T08:03:22.7758007+00:00"),
            FinishedAt = DateTime.Parse("2021-05-11T03:22:22.0364685+00:00")
        },
        new TaskModel{
            Id = 492,
            ProjectId = 91,
            PerformerId = 52,
            Name = "Rubber",
            TLDR = "Ratione ut non cum sapiente quia exercitationem repellendus rem.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-11-02T17:41:29.3362061+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 493,
            ProjectId = 91,
            PerformerId = 55,
            Name = "Handcrafted Intelligent",
            TLDR = "Deleniti dolores sunt.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-02-19T08:23:42.7811262+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 494,
            ProjectId = 92,
            PerformerId = 120,
            Name = "engineer Tasty Chief",
            TLDR = "Beatae eos odio esse culpa et qui quo.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-31T09:37:07.8547776+00:00"),
            FinishedAt = DateTime.Parse("2020-11-08T07:28:26.9116206+00:00")
        },
        new TaskModel{
            Id = 495,
            ProjectId = 92,
            PerformerId = 93,
            Name = "Personal Loan Account Customer implement",
            TLDR = "Voluptatem non quis impedit earum aut rerum excepturi accusantium.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-06T02:12:12.1567594+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 496,
            ProjectId = 92,
            PerformerId = 67,
            Name = "Robust Personal Loan Account",
            TLDR = "Commodi et autem ut placeat facilis consequuntur.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-11-04T13:33:15.8471063+00:00"),
            FinishedAt = DateTime.Parse("2021-01-23T22:44:05.3487293+00:00")
        },
        new TaskModel{
            Id = 497,
            ProjectId = 93,
            PerformerId = 82,
            Name = "Granite Missouri sensor",
            TLDR = "Maiores eius nobis sunt qui esse ut reprehenderit alias.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-05T12:32:18.0000983+00:00"),
            FinishedAt = DateTime.Parse("2021-05-02T17:19:27.4468671+00:00")
        },
        new TaskModel{
            Id = 498,
            ProjectId = 93,
            PerformerId = 67,
            Name = "Cliffs interface",
            TLDR = "Exercitationem cum et aut magni quod veniam similique ut sapiente.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-04-08T02:31:24.5815982+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 499,
            ProjectId = 93,
            PerformerId = 32,
            Name = "Cotton Nakfa",
            TLDR = "Voluptate minus quia tempore omnis.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-09-08T01:10:32.5839098+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 500,
            ProjectId = 93,
            PerformerId = 62,
            Name = "Licensed yellow Colorado",
            TLDR = "Ut dignissimos nulla necessitatibus et recusandae alias.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-03-13T19:34:36.7903561+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 501,
            ProjectId = 93,
            PerformerId = 51,
            Name = "Kids XSS Managed",
            TLDR = "Qui eos consectetur nam unde.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-15T21:23:31.7074527+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 502,
            ProjectId = 95,
            PerformerId = 22,
            Name = "impactful Borders definition",
            TLDR = "Libero qui quibusdam qui vel impedit dolor sint.",
            State = 2,
            CreatedAt = DateTime.Parse("2020-01-14T00:43:36.9873119+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 503,
            ProjectId = 95,
            PerformerId = 91,
            Name = "orange",
            TLDR = "Tempora aut cum aliquid illum et voluptas sit.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-08-20T05:55:58.1890017+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 504,
            ProjectId = 95,
            PerformerId = 33,
            Name = "Personal Loan Account synthesize",
            TLDR = "Nisi quibusdam quis voluptas voluptatum.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-11T15:34:50.7385514+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 505,
            ProjectId = 95,
            PerformerId = 85,
            Name = "Savings Account Cambridgeshire",
            TLDR = "Nihil mollitia ipsam at sit.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-01-23T20:23:28.4890886+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 506,
            ProjectId = 95,
            PerformerId = 30,
            Name = "Beauty, Tools & Automotive Cambridgeshire",
            TLDR = "Vero itaque eaque sed dolorum nobis consequuntur totam eum rerum.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-09-30T10:17:44.0651408+00:00"),
            FinishedAt = DateTime.Parse("2020-12-10T16:45:38.6321711+00:00")
        },
        new TaskModel{
            Id = 507,
            ProjectId = 95,
            PerformerId = 50,
            Name = "Grass-roots",
            TLDR = "Pariatur nemo officiis quam consectetur perferendis.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-05-07T21:27:12.7822662+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 508,
            ProjectId = 95,
            PerformerId = 54,
            Name = "platforms",
            TLDR = "Provident voluptatibus ex eum qui.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-22T02:15:19.9553952+00:00"),
            FinishedAt = DateTime.Parse("2021-04-22T20:16:04.6434159+00:00")
        },
        new TaskModel{
            Id = 509,
            ProjectId = 95,
            PerformerId = 20,
            Name = "Awesome Rubber Chicken bus algorithm",
            TLDR = "Harum in sunt odio placeat accusantium.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-12-29T19:12:20.8786417+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 510,
            ProjectId = 95,
            PerformerId = 33,
            Name = "California",
            TLDR = "Nisi enim porro fugiat a.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-03-22T02:26:31.1450539+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 511,
            ProjectId = 95,
            PerformerId = 63,
            Name = "networks",
            TLDR = "In saepe doloremque eius molestiae occaecati.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-05-27T17:38:48.2889828+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 512,
            ProjectId = 96,
            PerformerId = 78,
            Name = "Mission",
            TLDR = "Vero est dolores nemo laboriosam voluptate eos voluptatum architecto.",
            State = 2,
            CreatedAt = DateTime.Parse("2018-04-14T12:00:27.0772674+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 513,
            ProjectId = 96,
            PerformerId = 21,
            Name = "Brook Directives Avon",
            TLDR = "Et similique sunt ut voluptas aut voluptatum iure dolores sunt.",
            State = 2,
            CreatedAt = DateTime.Parse("2017-10-11T07:32:33.3610579+00:00"),
            FinishedAt = null
        },
        new TaskModel{
            Id = 514,
            ProjectId = 96,
            PerformerId = 115,
            Name = "Tasty Soft Towels Concrete",
            TLDR = "Non modi provident eveniet omnis unde ipsam.",
            State = 2,
            CreatedAt = DateTime.Parse("2019-03-13T03:38:22.7741198+00:00"),
            FinishedAt = null
        }

            };
#endregion

    }
}