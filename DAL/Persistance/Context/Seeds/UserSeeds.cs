using System;
using System.Collections.Generic;
using DAL.Domain.Models;

namespace DAL.Persistance.Context.Seeds
{
    public static class UserSeeds
    {
        #region Seeds
        public static IList<UserModel> Users { get; set; } = new List<UserModel>{
        new UserModel{
            Id = 20,
            TeamId = 1,
            FirstName = "Vivian",
            SurName = "Mertz",
            Email = "Vivian99@yahoo.com",
            RegisteredAt = DateTime.Parse("2018-10-18T22:37:40.7562662+00:00"),
            BirthDay = DateTime.Parse("1953-12-23T02:31:55.625118+00:00")
        },
        new UserModel{
            Id = 21,
            TeamId = 1,
            FirstName = "Theresa",
            SurName = "Gottlieb",
            Email = "Theresa_Gottlieb66@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-12-04T16:52:04.3727619+00:00"),
            BirthDay = DateTime.Parse("1992-09-27T20:27:03.5236015+00:00")
        },
        new UserModel{
            Id = 22,
            TeamId = 1,
            FirstName = "Brandy",
            SurName = "Witting",
            Email = "Brandy.Witting@gmail.com",
            RegisteredAt = DateTime.Parse("2019-01-10T02:51:56.7148896+00:00"),
            BirthDay = DateTime.Parse("2007-01-01T05:10:18.898869+00:00")
        },
        new UserModel{
            Id = 23,
            TeamId = 1,
            FirstName = "Theresa",
            SurName = "Ebert",
            Email = "Theresa82@hotmail.com",
            RegisteredAt = DateTime.Parse("2017-05-14T02:37:44.4486766+00:00"),
            BirthDay = DateTime.Parse("1980-02-20T16:32:12.6358667+00:00")
        },
        new UserModel{
            Id = 24,
            TeamId = 1,
            FirstName = "Alfredo",
            SurName = "Simonis",
            Email = "Alfredo_Simonis@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-10-14T16:40:52.2772028+00:00"),
            BirthDay = DateTime.Parse("1954-04-09T05:04:50.4709098+00:00")
        },
        new UserModel{
            Id = 25,
            TeamId = 1,
            FirstName = "Joanna",
            SurName = "Botsford",
            Email = "Joanna25@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-05-14T18:02:44.0995821+00:00"),
            BirthDay = DateTime.Parse("2009-05-12T02:09:16.2373321+00:00")
        },
        new UserModel{
            Id = 26,
            TeamId = 1,
            FirstName = "Ann",
            SurName = "Langworth",
            Email = "Ann.Langworth@hotmail.com",
            RegisteredAt = DateTime.Parse("2017-08-09T05:47:42.3699036+00:00"),
            BirthDay = DateTime.Parse("1962-02-26T08:08:59.071182+00:00")
        },
        new UserModel{
            Id = 27,
            TeamId = 1,
            FirstName = "Christie",
            SurName = "Gusikowski",
            Email = "Christie.Gusikowski@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-10-07T07:41:43.2460699+00:00"),
            BirthDay = DateTime.Parse("1981-01-10T11:38:30.5290222+00:00")
        },
        new UserModel{
            Id = 28,
            TeamId = 1,
            FirstName = "Lori",
            SurName = "Vandervort",
            Email = "Lori_Vandervort@hotmail.com",
            RegisteredAt = DateTime.Parse("2018-08-31T22:41:07.6888922+00:00"),
            BirthDay = DateTime.Parse("1963-09-19T17:24:20.7110674+00:00")
        },
        new UserModel{
            Id = 29,
            TeamId = 1,
            FirstName = "Micheal",
            SurName = "Franecki",
            Email = "Micheal71@hotmail.com",
            RegisteredAt = DateTime.Parse("2020-07-06T19:33:41.938034+00:00"),
            BirthDay = DateTime.Parse("1994-05-09T08:53:45.9218955+00:00")
        },
        new UserModel{
            Id = 30,
            TeamId = 1,
            FirstName = "Felicia",
            SurName = "Kirlin",
            Email = "Felicia.Kirlin74@yahoo.com",
            RegisteredAt = DateTime.Parse("2020-12-05T20:40:46.1133753+00:00"),
            BirthDay = DateTime.Parse("1961-09-11T05:38:14.8382487+00:00")
        },
        new UserModel{
            Id = 31,
            TeamId = 1,
            FirstName = "Kelvin",
            SurName = "Gleichner",
            Email = "Kelvin.Gleichner70@yahoo.com",
            RegisteredAt = DateTime.Parse("2020-05-12T03:13:21.2821256+00:00"),
            BirthDay = DateTime.Parse("1979-07-21T10:29:24.2311143+00:00")
        },
        new UserModel{
            Id = 32,
            TeamId = 1,
            FirstName = "Cody",
            SurName = "Morissette",
            Email = "Cody_Morissette@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-06-18T17:42:09.1841517+00:00"),
            BirthDay = DateTime.Parse("1997-08-01T09:17:18.266399+00:00")
        },
        new UserModel{
            Id = 33,
            TeamId = 1,
            FirstName = "Latoya",
            SurName = "Bernhard",
            Email = "Latoya53@gmail.com",
            RegisteredAt = DateTime.Parse("2019-07-09T19:50:57.7157167+00:00"),
            BirthDay = DateTime.Parse("1974-08-12T22:43:03.8405988+00:00")
        },
        new UserModel{
            Id = 34,
            TeamId = 1,
            FirstName = "Kurt",
            SurName = "Bahringer",
            Email = "Kurt39@gmail.com",
            RegisteredAt = DateTime.Parse("2019-01-19T02:07:06.5864758+00:00"),
            BirthDay = DateTime.Parse("1990-09-04T20:20:01.4467559+00:00")
        },
        new UserModel{
            Id = 35,
            TeamId = 1,
            FirstName = "Angelo",
            SurName = "Homenick",
            Email = "Angelo.Homenick@hotmail.com",
            RegisteredAt = DateTime.Parse("2017-09-19T10:24:45.8258661+00:00"),
            BirthDay = DateTime.Parse("1970-08-22T19:31:46.7827196+00:00")
        },
        new UserModel{
            Id = 36,
            TeamId = 1,
            FirstName = "Traci",
            SurName = "Wunsch",
            Email = "Traci38@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-09-16T09:21:35.9976958+00:00"),
            BirthDay = DateTime.Parse("2015-07-07T08:27:52.3070313+00:00")
        },
        new UserModel{
            Id = 37,
            TeamId = 1,
            FirstName = "Jared",
            SurName = "Anderson",
            Email = "Jared_Anderson50@yahoo.com",
            RegisteredAt = DateTime.Parse("2017-07-31T01:55:48.6759553+00:00"),
            BirthDay = DateTime.Parse("2014-01-14T21:13:26.0259005+00:00")
        },
        new UserModel{
            Id = 38,
            TeamId = 1,
            FirstName = "Bonnie",
            SurName = "Donnelly",
            Email = "Bonnie.Donnelly4@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-01-30T21:19:51.635656+00:00"),
            BirthDay = DateTime.Parse("1981-06-23T08:22:10.4378304+00:00")
        },
        new UserModel{
            Id = 39,
            TeamId = 1,
            FirstName = "Jared",
            SurName = "Reichert",
            Email = "Jared_Reichert@gmail.com",
            RegisteredAt = DateTime.Parse("2019-06-16T03:24:19.7792006+00:00"),
            BirthDay = DateTime.Parse("1965-10-25T04:43:35.82333+00:00")
        },
        new UserModel{
            Id = 40,
            TeamId = 1,
            FirstName = "Regina",
            SurName = "Swaniawski",
            Email = "Regina.Swaniawski95@gmail.com",
            RegisteredAt = DateTime.Parse("2018-12-30T19:31:54.1422198+00:00"),
            BirthDay = DateTime.Parse("1997-04-13T12:59:17.3003976+00:00")
        },
        new UserModel{
            Id = 41,
            TeamId = 1,
            FirstName = "Francis",
            SurName = "Bode",
            Email = "Francis_Bode@gmail.com",
            RegisteredAt = DateTime.Parse("2018-09-16T23:33:33.113265+00:00"),
            BirthDay = DateTime.Parse("1978-08-31T01:24:17.1781745+00:00")
        },
        new UserModel{
            Id = 42,
            TeamId = 1,
            FirstName = "Gail",
            SurName = "Kuhlman",
            Email = "Gail92@hotmail.com",
            RegisteredAt = DateTime.Parse("2018-03-09T19:50:49.3269783+00:00"),
            BirthDay = DateTime.Parse("1959-12-10T06:49:27.657347+00:00")
        },
        new UserModel{
            Id = 43,
            TeamId = 1,
            FirstName = "Ruth",
            SurName = "Towne",
            Email = "Ruth.Towne58@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-03-24T15:19:54.0925316+00:00"),
            BirthDay = DateTime.Parse("1967-09-09T15:39:22.1793752+00:00")
        },
        new UserModel{
            Id = 44,
            TeamId = 1,
            FirstName = "Alfredo",
            SurName = "Bosco",
            Email = "Alfredo_Bosco74@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-09-12T02:53:33.3803685+00:00"),
            BirthDay = DateTime.Parse("2002-12-10T05:08:46.6907795+00:00")
        },
        new UserModel{
            Id = 45,
            TeamId = 1,
            FirstName = "Raymond",
            SurName = "Reilly",
            Email = "Raymond_Reilly63@gmail.com",
            RegisteredAt = DateTime.Parse("2018-06-16T04:30:06.1819252+00:00"),
            BirthDay = DateTime.Parse("1951-10-02T04:04:59.0561607+00:00")
        },
        new UserModel{
            Id = 46,
            TeamId = 1,
            FirstName = "Donnie",
            SurName = "Kemmer",
            Email = "Donnie.Kemmer@hotmail.com",
            RegisteredAt = DateTime.Parse("2020-05-28T11:00:37.1814147+00:00"),
            BirthDay = DateTime.Parse("2013-07-04T11:47:56.1716888+00:00")
        },
        new UserModel{
            Id = 47,
            TeamId = 1,
            FirstName = "Carmen",
            SurName = "Rowe",
            Email = "Carmen62@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-03-04T02:41:01.5218276+00:00"),
            BirthDay = DateTime.Parse("1979-11-15T03:28:59.5753379+00:00")
        },
        new UserModel{
            Id = 48,
            TeamId = 1,
            FirstName = "Deanna",
            SurName = "Hodkiewicz",
            Email = "Deanna75@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-04-21T17:11:48.5766282+00:00"),
            BirthDay = DateTime.Parse("1962-09-05T16:44:04.2419398+00:00")
        },
        new UserModel{
            Id = 49,
            TeamId = 1,
            FirstName = "Kerry",
            SurName = "Smith",
            Email = "Kerry.Smith8@yahoo.com",
            RegisteredAt = DateTime.Parse("2020-10-05T19:31:18.5660893+00:00"),
            BirthDay = DateTime.Parse("1976-11-14T00:11:37.3719932+00:00")
        },
        new UserModel{
            Id = 50,
            TeamId = 1,
            FirstName = "Cecelia",
            SurName = "Purdy",
            Email = "Cecelia_Purdy27@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-12-22T11:50:48.6492489+00:00"),
            BirthDay = DateTime.Parse("1978-12-26T06:05:21.2029844+00:00")
        },
        new UserModel{
            Id = 51,
            TeamId = 2,
            FirstName = "Arnold",
            SurName = "Kuhn",
            Email = "Arnold_Kuhn3@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-04-16T15:40:05.5499205+00:00"),
            BirthDay = DateTime.Parse("1991-12-28T15:06:08.2066741+00:00")
        },
        new UserModel{
            Id = 52,
            TeamId = 2,
            FirstName = "Ethel",
            SurName = "Berge",
            Email = "Ethel.Berge5@gmail.com",
            RegisteredAt = DateTime.Parse("2017-10-23T23:56:45.831197+00:00"),
            BirthDay = DateTime.Parse("1960-09-15T10:26:58.8719793+00:00")
        },
        new UserModel{
            Id = 53,
            TeamId = 2,
            FirstName = "Terrance",
            SurName = "Yundt",
            Email = "Terrance1@gmail.com",
            RegisteredAt = DateTime.Parse("2019-11-12T22:36:19.996537+00:00"),
            BirthDay = DateTime.Parse("1956-09-10T14:33:58.3850454+00:00")
        },
        new UserModel{
            Id = 54,
            TeamId = 2,
            FirstName = "Tammy",
            SurName = "Sawayn",
            Email = "Tammy.Sawayn@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-11-16T09:03:21.5007796+00:00"),
            BirthDay = DateTime.Parse("1980-05-17T11:25:45.3654839+00:00")
        },
        new UserModel{
            Id = 55,
            TeamId = 2,
            FirstName = "Jay",
            SurName = "Bashirian",
            Email = "Jay.Bashirian37@hotmail.com",
            RegisteredAt = DateTime.Parse("2018-01-23T16:06:30.7679633+00:00"),
            BirthDay = DateTime.Parse("2015-05-31T22:59:03.3739277+00:00")
        },
        new UserModel{
            Id = 56,
            TeamId = 2,
            FirstName = "Faith",
            SurName = "Gaylord",
            Email = "Faith9@hotmail.com",
            RegisteredAt = DateTime.Parse("2018-07-22T18:43:27.1301853+00:00"),
            BirthDay = DateTime.Parse("2005-10-19T00:22:26.8342335+00:00")
        },
        new UserModel{
            Id = 57,
            TeamId = 2,
            FirstName = "Samuel",
            SurName = "Pouros",
            Email = "Samuel_Pouros39@hotmail.com",
            RegisteredAt = DateTime.Parse("2020-03-15T00:01:07.6336612+00:00"),
            BirthDay = DateTime.Parse("2008-10-07T16:58:56.996551+00:00")
        },
        new UserModel{
            Id = 58,
            TeamId = 2,
            FirstName = "Laura",
            SurName = "Jacobson",
            Email = "Laura_Jacobson20@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-07-08T07:50:11.246355+00:00"),
            BirthDay = DateTime.Parse("1955-01-24T00:17:45.848739+00:00")
        },
        new UserModel{
            Id = 59,
            TeamId = 2,
            FirstName = "Heidi",
            SurName = "Boehm",
            Email = "Heidi_Boehm55@gmail.com",
            RegisteredAt = DateTime.Parse("2018-06-07T06:09:38.325958+00:00"),
            BirthDay = DateTime.Parse("1982-07-16T02:30:02.3121231+00:00")
        },
        new UserModel{
            Id = 60,
            TeamId = 2,
            FirstName = "Tara",
            SurName = "Morar",
            Email = "Tara.Morar@gmail.com",
            RegisteredAt = DateTime.Parse("2019-11-11T20:17:11.7183127+00:00"),
            BirthDay = DateTime.Parse("1995-06-21T17:36:22.4459621+00:00")
        },
        new UserModel{
            Id = 61,
            TeamId = 2,
            FirstName = "Virgil",
            SurName = "Hauck",
            Email = "Virgil_Hauck@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-05-13T08:24:18.0129537+00:00"),
            BirthDay = DateTime.Parse("1989-03-23T03:09:17.7853449+00:00")
        },
        new UserModel{
            Id = 62,
            TeamId = 3,
            FirstName = "Lynda",
            SurName = "Grant",
            Email = "Lynda_Grant85@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-06-10T13:08:42.8719538+00:00"),
            BirthDay = DateTime.Parse("1966-07-20T16:09:45.6877503+00:00")
        },
        new UserModel{
            Id = 63,
            TeamId = 3,
            FirstName = "Monica",
            SurName = "Rodriguez",
            Email = "Monica24@hotmail.com",
            RegisteredAt = DateTime.Parse("2017-09-21T05:15:29.6478821+00:00"),
            BirthDay = DateTime.Parse("1973-06-24T02:21:54.3922787+00:00")
        },
        new UserModel{
            Id = 64,
            TeamId = 3,
            FirstName = "Elias",
            SurName = "Jacobi",
            Email = "Elias_Jacobi@gmail.com",
            RegisteredAt = DateTime.Parse("2017-01-25T02:21:43.1462865+00:00"),
            BirthDay = DateTime.Parse("1991-02-22T18:04:10.3835514+00:00")
        },
        new UserModel{
            Id = 65,
            TeamId = 3,
            FirstName = "Linda",
            SurName = "Nicolas",
            Email = "Linda_Nicolas@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-03-26T23:08:20.5250962+00:00"),
            BirthDay = DateTime.Parse("2013-06-10T11:26:40.9247392+00:00")
        },
        new UserModel{
            Id = 66,
            TeamId = 3,
            FirstName = "Jana",
            SurName = "Walter",
            Email = "Jana.Walter12@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-10-26T08:51:16.9830832+00:00"),
            BirthDay = DateTime.Parse("1961-03-18T07:16:52.612757+00:00")
        },
        new UserModel{
            Id = 67,
            TeamId = 3,
            FirstName = "Darrell",
            SurName = "Beier",
            Email = "Darrell_Beier71@gmail.com",
            RegisteredAt = DateTime.Parse("2019-01-13T16:54:23.6449812+00:00"),
            BirthDay = DateTime.Parse("1986-08-03T13:31:36.1551871+00:00")
        },
        new UserModel{
            Id = 68,
            TeamId = 4,
            FirstName = "Lucia",
            SurName = "Mraz",
            Email = "Lucia_Mraz@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-04-09T07:38:52.9231187+00:00"),
            BirthDay = DateTime.Parse("2008-01-28T01:50:59.5852294+00:00")
        },
        new UserModel{
            Id = 69,
            TeamId = 4,
            FirstName = "Velma",
            SurName = "Daniel",
            Email = "Velma_Daniel31@gmail.com",
            RegisteredAt = DateTime.Parse("2019-10-25T03:29:14.7706539+00:00"),
            BirthDay = DateTime.Parse("1983-10-16T10:46:41.1117473+00:00")
        },
        new UserModel{
            Id = 70,
            TeamId = 4,
            FirstName = "Arturo",
            SurName = "Howell",
            Email = "Arturo_Howell3@gmail.com",
            RegisteredAt = DateTime.Parse("2019-06-02T01:39:53.4880582+00:00"),
            BirthDay = DateTime.Parse("1958-01-21T22:39:04.5640346+00:00")
        },
        new UserModel{
            Id = 71,
            TeamId = 4,
            FirstName = "Wayne",
            SurName = "Balistreri",
            Email = "Wayne57@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-01-24T19:49:09.5971016+00:00"),
            BirthDay = DateTime.Parse("1951-08-11T04:56:57.1317749+00:00")
        },
        new UserModel{
            Id = 72,
            TeamId = 4,
            FirstName = "Stanley",
            SurName = "Lang",
            Email = "Stanley.Lang92@yahoo.com",
            RegisteredAt = DateTime.Parse("2018-06-17T17:19:12.3603431+00:00"),
            BirthDay = DateTime.Parse("2014-12-31T22:41:16.9774806+00:00")
        },
        new UserModel{
            Id = 73,
            TeamId = 4,
            FirstName = "Joshua",
            SurName = "Brekke",
            Email = "Joshua67@gmail.com",
            RegisteredAt = DateTime.Parse("2020-01-04T17:50:58.9399965+00:00"),
            BirthDay = DateTime.Parse("1965-10-21T01:50:34.8734456+00:00")
        },
        new UserModel{
            Id = 74,
            TeamId = 4,
            FirstName = "Sherry",
            SurName = "Stamm",
            Email = "Sherry_Stamm@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-12-26T19:12:17.6191299+00:00"),
            BirthDay = DateTime.Parse("1958-05-25T17:41:37.1967743+00:00")
        },
        new UserModel{
            Id = 75,
            TeamId = 4,
            FirstName = "Guadalupe",
            SurName = "Willms",
            Email = "Guadalupe_Willms95@yahoo.com",
            RegisteredAt = DateTime.Parse("2018-12-13T17:26:01.6768983+00:00"),
            BirthDay = DateTime.Parse("1994-07-09T02:11:59.4339644+00:00")
        },
        new UserModel{
            Id = 76,
            TeamId = 4,
            FirstName = "Maxine",
            SurName = "Hoeger",
            Email = "Maxine.Hoeger88@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-05-10T17:06:05.366492+00:00"),
            BirthDay = DateTime.Parse("1978-04-28T16:29:44.0665115+00:00")
        },
        new UserModel{
            Id = 77,
            TeamId = 5,
            FirstName = "Kim",
            SurName = "Hermann",
            Email = "Kim.Hermann18@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-03-07T10:09:24.30995+00:00"),
            BirthDay = DateTime.Parse("1954-04-26T21:34:44.6823812+00:00")
        },
        new UserModel{
            Id = 78,
            TeamId = 5,
            FirstName = "Nathaniel",
            SurName = "Jast",
            Email = "Nathaniel_Jast@hotmail.com",
            RegisteredAt = DateTime.Parse("2016-12-17T11:36:45.469846+00:00"),
            BirthDay = DateTime.Parse("2016-05-14T06:44:20.0832492+00:00")
        },
        new UserModel{
            Id = 79,
            TeamId = 6,
            FirstName = "Darrel",
            SurName = "Bosco",
            Email = "Darrel65@hotmail.com",
            RegisteredAt = DateTime.Parse("2018-03-29T18:00:00.6949134+00:00"),
            BirthDay = DateTime.Parse("1997-02-11T15:29:32.385962+00:00")
        },
        new UserModel{
            Id = 80,
            TeamId = 6,
            FirstName = "Steve",
            SurName = "Schamberger",
            Email = "Steve.Schamberger@gmail.com",
            RegisteredAt = DateTime.Parse("2020-06-20T15:48:49.9308181+00:00"),
            BirthDay = DateTime.Parse("1977-03-26T13:45:38.6939773+00:00")
        },
        new UserModel{
            Id = 81,
            TeamId = 6,
            FirstName = "Leslie",
            SurName = "Kub",
            Email = "Leslie.Kub11@gmail.com",
            RegisteredAt = DateTime.Parse("2018-01-14T14:03:15.5482193+00:00"),
            BirthDay = DateTime.Parse("2012-10-17T00:32:36.2040923+00:00")
        },
        new UserModel{
            Id = 82,
            TeamId = 6,
            FirstName = "Phillip",
            SurName = "Jast",
            Email = "Phillip_Jast63@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-10-05T14:41:33.0390269+00:00"),
            BirthDay = DateTime.Parse("1993-05-16T06:54:36.8889014+00:00")
        },
        new UserModel{
            Id = 83,
            TeamId = 6,
            FirstName = "Terrence",
            SurName = "Feil",
            Email = "Terrence_Feil@yahoo.com",
            RegisteredAt = DateTime.Parse("2017-07-26T07:29:48.7256737+00:00"),
            BirthDay = DateTime.Parse("1965-01-29T23:28:46.3719704+00:00")
        },
        new UserModel{
            Id = 84,
            TeamId = 7,
            FirstName = "Cedric",
            SurName = "Schulist",
            Email = "Cedric_Schulist@gmail.com",
            RegisteredAt = DateTime.Parse("2020-01-25T20:34:39.3978003+00:00"),
            BirthDay = DateTime.Parse("1983-09-28T02:23:13.3717168+00:00")
        },
        new UserModel{
            Id = 85,
            TeamId = 7,
            FirstName = "Adrian",
            SurName = "Schumm",
            Email = "Adrian28@gmail.com",
            RegisteredAt = DateTime.Parse("2017-07-17T12:39:41.2480761+00:00"),
            BirthDay = DateTime.Parse("1956-10-29T10:40:16.078728+00:00")
        },
        new UserModel{
            Id = 86,
            TeamId = 7,
            FirstName = "Johnnie",
            SurName = "Morar",
            Email = "Johnnie_Morar@hotmail.com",
            RegisteredAt = DateTime.Parse("2017-12-28T20:38:56.372673+00:00"),
            BirthDay = DateTime.Parse("1984-12-07T01:06:38.1803491+00:00")
        },
        new UserModel{
            Id = 87,
            TeamId = 7,
            FirstName = "Henrietta",
            SurName = "Durgan",
            Email = "Henrietta14@gmail.com",
            RegisteredAt = DateTime.Parse("2020-08-01T17:37:21.0502033+00:00"),
            BirthDay = DateTime.Parse("1993-09-10T04:15:05.3354909+00:00")
        },
        new UserModel{
            Id = 88,
            TeamId = 7,
            FirstName = "Hubert",
            SurName = "Kshlerin",
            Email = "Hubert_Kshlerin10@gmail.com",
            RegisteredAt = DateTime.Parse("2019-06-04T17:50:30.0572265+00:00"),
            BirthDay = DateTime.Parse("2005-06-08T08:09:38.2312402+00:00")
        },
        new UserModel{
            Id = 89,
            TeamId = 7,
            FirstName = "Hugo",
            SurName = "Haag",
            Email = "Hugo_Haag7@hotmail.com",
            RegisteredAt = DateTime.Parse("2018-05-30T18:12:20.4189273+00:00"),
            BirthDay = DateTime.Parse("1993-05-02T18:41:35.7419746+00:00")
        },
        new UserModel{
            Id = 90,
            TeamId = 7,
            FirstName = "Eloise",
            SurName = "Conn",
            Email = "Eloise_Conn@gmail.com",
            RegisteredAt = DateTime.Parse("2020-11-05T19:27:02.0036446+00:00"),
            BirthDay = DateTime.Parse("1958-02-23T05:15:10.6871664+00:00")
        },
        new UserModel{
            Id = 91,
            TeamId = 7,
            FirstName = "Kellie",
            SurName = "Graham",
            Email = "Kellie.Graham@gmail.com",
            RegisteredAt = DateTime.Parse("2017-05-06T00:53:37.2184006+00:00"),
            BirthDay = DateTime.Parse("2006-08-18T04:05:57.1022215+00:00")
        },
        new UserModel{
            Id = 92,
            TeamId = 7,
            FirstName = "Tricia",
            SurName = "Buckridge",
            Email = "Tricia.Buckridge@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-11-12T21:55:18.0318373+00:00"),
            BirthDay = DateTime.Parse("2000-06-30T17:41:42.401706+00:00")
        },
        new UserModel{
            Id = 93,
            TeamId = 7,
            FirstName = "Edith",
            SurName = "Spinka",
            Email = "Edith68@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-03-15T23:47:05.779173+00:00"),
            BirthDay = DateTime.Parse("1995-10-01T16:23:59.256297+00:00")
        },
        new UserModel{
            Id = 94,
            TeamId = 7,
            FirstName = "Erma",
            SurName = "Lindgren",
            Email = "Erma6@yahoo.com",
            RegisteredAt = DateTime.Parse("2018-07-08T07:34:56.2200178+00:00"),
            BirthDay = DateTime.Parse("1993-03-28T04:01:22.960643+00:00")
        },
        new UserModel{
            Id = 95,
            TeamId = 7,
            FirstName = "Jordan",
            SurName = "Ebert",
            Email = "Jordan50@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-05-31T16:33:47.1236178+00:00"),
            BirthDay = DateTime.Parse("1996-11-01T08:57:52.0299757+00:00")
        },
        new UserModel{
            Id = 96,
            TeamId = 7,
            FirstName = "Rebecca",
            SurName = "Reynolds",
            Email = "Rebecca53@gmail.com",
            RegisteredAt = DateTime.Parse("2020-09-05T11:42:16.0457335+00:00"),
            BirthDay = DateTime.Parse("1980-07-02T09:22:41.3229778+00:00")
        },
        new UserModel{
            Id = 97,
            TeamId = 7,
            FirstName = "Gerald",
            SurName = "Emmerich",
            Email = "Gerald.Emmerich@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-11-30T23:04:41.6906599+00:00"),
            BirthDay = DateTime.Parse("2006-07-29T09:51:09.6379634+00:00")
        },
        new UserModel{
            Id = 98,
            TeamId = 7,
            FirstName = "Kristin",
            SurName = "Harvey",
            Email = "Kristin_Harvey@hotmail.com",
            RegisteredAt = DateTime.Parse("2016-12-17T05:42:10.2447694+00:00"),
            BirthDay = DateTime.Parse("1983-10-12T00:02:01.9103128+00:00")
        },
        new UserModel{
            Id = 99,
            TeamId = 7,
            FirstName = "Johnnie",
            SurName = "Wuckert",
            Email = "Johnnie32@hotmail.com",
            RegisteredAt = DateTime.Parse("2020-02-15T20:48:35.8799264+00:00"),
            BirthDay = DateTime.Parse("2014-03-07T02:51:25.8184223+00:00")
        },
        new UserModel{
            Id = 100,
            TeamId = 7,
            FirstName = "Clayton",
            SurName = "Pfannerstill",
            Email = "Clayton82@gmail.com",
            RegisteredAt = DateTime.Parse("2017-12-12T11:24:53.6742868+00:00"),
            BirthDay = DateTime.Parse("2007-10-14T11:09:13.415213+00:00")
        },
        new UserModel{
            Id = 101,
            TeamId = 7,
            FirstName = "Roland",
            SurName = "Fadel",
            Email = "Roland.Fadel@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-09-13T02:54:24.6093134+00:00"),
            BirthDay = DateTime.Parse("1980-05-31T15:03:37.8426622+00:00")
        },
        new UserModel{
            Id = 102,
            TeamId = 8,
            FirstName = "Noah",
            SurName = "Goyette",
            Email = "Noah.Goyette@gmail.com",
            RegisteredAt = DateTime.Parse("2018-09-05T22:27:05.8908668+00:00"),
            BirthDay = DateTime.Parse("1959-09-05T22:16:51.6758099+00:00")
        },
        new UserModel{
            Id = 103,
            TeamId = 8,
            FirstName = "Jared",
            SurName = "Pollich",
            Email = "Jared34@gmail.com",
            RegisteredAt = DateTime.Parse("2020-06-06T12:56:11.0729258+00:00"),
            BirthDay = DateTime.Parse("1989-12-18T04:25:08.8611911+00:00")
        },
        new UserModel{
            Id = 104,
            TeamId = 8,
            FirstName = "Janice",
            SurName = "Brekke",
            Email = "Janice_Brekke@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-06-06T01:04:37.1076381+00:00"),
            BirthDay = DateTime.Parse("1952-12-14T05:20:26.6709768+00:00")
        },
        new UserModel{
            Id = 105,
            TeamId = 8,
            FirstName = "Carol",
            SurName = "Larson",
            Email = "Carol_Larson62@gmail.com",
            RegisteredAt = DateTime.Parse("2019-06-21T05:59:19.5473273+00:00"),
            BirthDay = DateTime.Parse("1977-12-10T13:37:47.3756041+00:00")
        },
        new UserModel{
            Id = 106,
            TeamId = 8,
            FirstName = "Gregory",
            SurName = "Corwin",
            Email = "Gregory.Corwin58@gmail.com",
            RegisteredAt = DateTime.Parse("2018-03-16T16:40:54.6943735+00:00"),
            BirthDay = DateTime.Parse("2011-05-18T12:46:36.2437239+00:00")
        },
        new UserModel{
            Id = 107,
            TeamId = 8,
            FirstName = "Aubrey",
            SurName = "Luettgen",
            Email = "Aubrey.Luettgen48@hotmail.com",
            RegisteredAt = DateTime.Parse("2016-10-12T00:51:55.0428877+00:00"),
            BirthDay = DateTime.Parse("2007-10-12T18:43:57.858621+00:00")
        },
        new UserModel{
            Id = 108,
            TeamId = 8,
            FirstName = "Marilyn",
            SurName = "O'Hara",
            Email = "Marilyn_OHara77@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-04-28T06:31:48.0982897+00:00"),
            BirthDay = DateTime.Parse("2003-11-18T08:02:47.897001+00:00")
        },
        new UserModel{
            Id = 109,
            TeamId = 8,
            FirstName = "Gary",
            SurName = "Jones",
            Email = "Gary.Jones@hotmail.com",
            RegisteredAt = DateTime.Parse("2020-12-27T19:00:14.0243114+00:00"),
            BirthDay = DateTime.Parse("1958-11-23T17:20:25.453611+00:00")
        },
        new UserModel{
            Id = 110,
            TeamId = 8,
            FirstName = "Abraham",
            SurName = "White",
            Email = "Abraham31@yahoo.com",
            RegisteredAt = DateTime.Parse("2018-06-14T08:31:05.4962897+00:00"),
            BirthDay = DateTime.Parse("1965-04-03T05:23:03.3513225+00:00")
        },
        new UserModel{
            Id = 111,
            TeamId = 8,
            FirstName = "Rachel",
            SurName = "Schneider",
            Email = "Rachel.Schneider@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-06-04T09:05:22.884538+00:00"),
            BirthDay = DateTime.Parse("1953-05-12T23:46:32.2205672+00:00")
        },
        new UserModel{
            Id = 112,
            TeamId = 8,
            FirstName = "Roy",
            SurName = "Lebsack",
            Email = "Roy83@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-11-15T04:11:58.5170636+00:00"),
            BirthDay = DateTime.Parse("1956-01-19T23:14:13.3617325+00:00")
        },
        new UserModel{
            Id = 113,
            TeamId = 9,
            FirstName = "Natasha",
            SurName = "Orn",
            Email = "Natasha.Orn@gmail.com",
            RegisteredAt = DateTime.Parse("2017-10-29T20:34:50.864427+00:00"),
            BirthDay = DateTime.Parse("1969-07-24T04:48:40.7406598+00:00")
        },
        new UserModel{
            Id = 114,
            TeamId = 9,
            FirstName = "Nina",
            SurName = "Braun",
            Email = "Nina4@yahoo.com",
            RegisteredAt = DateTime.Parse("2016-08-25T16:07:07.846186+00:00"),
            BirthDay = DateTime.Parse("1976-01-11T03:42:27.4730629+00:00")
        },
        new UserModel{
            Id = 115,
            TeamId = 9,
            FirstName = "Donald",
            SurName = "Beier",
            Email = "Donald_Beier@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-09-22T06:12:06.4894323+00:00"),
            BirthDay = DateTime.Parse("1993-10-11T08:13:06.0116355+00:00")
        },
        new UserModel{
            Id = 116,
            TeamId = 9,
            FirstName = "Domingo",
            SurName = "Yundt",
            Email = "Domingo.Yundt97@hotmail.com",
            RegisteredAt = DateTime.Parse("2018-10-07T17:04:50.7867959+00:00"),
            BirthDay = DateTime.Parse("1987-12-02T08:30:54.809719+00:00")
        },
        new UserModel{
            Id = 117,
            TeamId = 9,
            FirstName = "Zachary",
            SurName = "Mayert",
            Email = "Zachary10@gmail.com",
            RegisteredAt = DateTime.Parse("2019-12-23T02:02:10.7715539+00:00"),
            BirthDay = DateTime.Parse("1971-11-08T20:39:38.4512451+00:00")
        },
        new UserModel{
            Id = 118,
            TeamId = 9,
            FirstName = "Otis",
            SurName = "O'Connell",
            Email = "Otis.OConnell41@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-03-30T01:11:27.091885+00:00"),
            BirthDay = DateTime.Parse("1978-12-30T22:35:24.7842622+00:00")
        },
        new UserModel{
            Id = 119,
            TeamId = 9,
            FirstName = "Kenneth",
            SurName = "Bergnaum",
            Email = "Kenneth12@hotmail.com",
            RegisteredAt = DateTime.Parse("2017-03-24T18:25:54.0862325+00:00"),
            BirthDay = DateTime.Parse("1963-07-31T15:02:58.0368395+00:00")
        },
        new UserModel{
            Id = 120,
            TeamId = 9,
            FirstName = "Cathy",
            SurName = "Ernser",
            Email = "Cathy.Ernser74@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-12-12T08:07:41.8625101+00:00"),
            BirthDay = DateTime.Parse("1986-11-12T15:03:24.8904951+00:00")
        },
        new UserModel{
            Id = 121,
            TeamId = 9,
            FirstName = "Kendra",
            SurName = "Hand",
            Email = "Kendra97@hotmail.com",
            RegisteredAt = DateTime.Parse("2017-12-29T00:31:22.7392771+00:00"),
            BirthDay = DateTime.Parse("1959-02-10T12:25:28.4646129+00:00")
        },
        new UserModel{
            Id = 122,
            TeamId = 9,
            FirstName = "Sophia",
            SurName = "Corwin",
            Email = "Sophia.Corwin18@hotmail.com",
            RegisteredAt = DateTime.Parse("2018-06-14T14:11:50.2684324+00:00"),
            BirthDay = DateTime.Parse("1978-08-23T12:22:23.5524873+00:00")
        },
        new UserModel{
            Id = 1,
            TeamId = null,
            FirstName = "Anne",
            SurName = "Collier",
            Email = "Anne.Collier@hotmail.com",
            RegisteredAt = DateTime.Parse("2020-04-24T03:13:17.101291+00:00"),
            BirthDay = DateTime.Parse("1987-03-12T09:11:01.3536142+00:00")
        },
        new UserModel{
            Id = 2,
            TeamId = null,
            FirstName = "Erin",
            SurName = "Pacocha",
            Email = "Erin.Pacocha14@hotmail.com",
            RegisteredAt = DateTime.Parse("2020-08-14T06:01:52.6925066+00:00"),
            BirthDay = DateTime.Parse("2005-03-07T01:42:11.4669734+00:00")
        },
        new UserModel{
            Id = 3,
            TeamId = null,
            FirstName = "Jennifer",
            SurName = "Haag",
            Email = "Jennifer37@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-11-14T18:01:12.835198+00:00"),
            BirthDay = DateTime.Parse("1963-01-27T12:03:01.0831205+00:00")
        },
        new UserModel{
            Id = 4,
            TeamId = null,
            FirstName = "Elizabeth",
            SurName = "Koepp",
            Email = "Elizabeth94@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-06-11T19:05:36.352357+00:00"),
            BirthDay = DateTime.Parse("1965-05-26T18:27:15.2255076+00:00")
        },
        new UserModel{
            Id = 5,
            TeamId = null,
            FirstName = "Sadie",
            SurName = "Bernhard",
            Email = "Sadie15@yahoo.com",
            RegisteredAt = DateTime.Parse("2021-04-15T21:08:32.6239838+00:00"),
            BirthDay = DateTime.Parse("2010-12-05T21:48:07.2846901+00:00")
        },
        new UserModel{
            Id = 6,
            TeamId = null,
            FirstName = "Geraldine",
            SurName = "Mann",
            Email = "Geraldine_Mann@gmail.com",
            RegisteredAt = DateTime.Parse("2016-08-20T14:00:01.2473422+00:00"),
            BirthDay = DateTime.Parse("2000-02-22T04:35:15.9816737+00:00")
        },
        new UserModel{
            Id = 7,
            TeamId = null,
            FirstName = "Hugh",
            SurName = "Nolan",
            Email = "Hugh_Nolan95@yahoo.com",
            RegisteredAt = DateTime.Parse("2019-01-17T20:07:12.3581276+00:00"),
            BirthDay = DateTime.Parse("2010-06-08T20:10:05.7503463+00:00")
        },
        new UserModel{
            Id = 8,
            TeamId = null,
            FirstName = "Veronica",
            SurName = "Cronin",
            Email = "Veronica_Cronin@gmail.com",
            RegisteredAt = DateTime.Parse("2019-11-27T21:24:01.6671222+00:00"),
            BirthDay = DateTime.Parse("1982-12-21T00:36:19.7584357+00:00")
        },
        new UserModel{
            Id = 9,
            TeamId = null,
            FirstName = "Jay",
            SurName = "Haag",
            Email = "Jay70@yahoo.com",
            RegisteredAt = DateTime.Parse("2017-03-11T21:56:08.6567705+00:00"),
            BirthDay = DateTime.Parse("2016-01-02T18:44:53.6833985+00:00")
        },
        new UserModel{
            Id = 10,
            TeamId = null,
            FirstName = "Lula",
            SurName = "Reilly",
            Email = "Lula_Reilly9@gmail.com",
            RegisteredAt = DateTime.Parse("2018-10-05T09:54:40.7020247+00:00"),
            BirthDay = DateTime.Parse("2009-01-21T03:44:21.0876712+00:00")
        },
        new UserModel{
            Id = 11,
            TeamId = null,
            FirstName = "Emilio",
            SurName = "Larson",
            Email = "Emilio_Larson67@hotmail.com",
            RegisteredAt = DateTime.Parse("2017-12-04T03:08:03.1432358+00:00"),
            BirthDay = DateTime.Parse("1964-12-13T14:58:53.757097+00:00")
        },
        new UserModel{
            Id = 12,
            TeamId = null,
            FirstName = "Eloise",
            SurName = "DuBuque",
            Email = "Eloise_DuBuque11@gmail.com",
            RegisteredAt = DateTime.Parse("2017-12-18T00:01:27.9004966+00:00"),
            BirthDay = DateTime.Parse("1973-04-15T22:33:00.6695329+00:00")
        },
        new UserModel{
            Id = 13,
            TeamId = null,
            FirstName = "Bobbie",
            SurName = "Mante",
            Email = "Bobbie.Mante@hotmail.com",
            RegisteredAt = DateTime.Parse("2021-06-22T00:26:06.8446591+00:00"),
            BirthDay = DateTime.Parse("1962-01-01T03:47:11.1081739+00:00")
        },
        new UserModel{
            Id = 14,
            TeamId = null,
            FirstName = "Agnes",
            SurName = "Gottlieb",
            Email = "Agnes_Gottlieb6@hotmail.com",
            RegisteredAt = DateTime.Parse("2018-10-27T03:32:42.6385244+00:00"),
            BirthDay = DateTime.Parse("1994-01-10T04:13:23.6039206+00:00")
        },
        new UserModel{
            Id = 15,
            TeamId = null,
            FirstName = "Dominick",
            SurName = "Kling",
            Email = "Dominick.Kling73@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-05-17T15:33:00.8689932+00:00"),
            BirthDay = DateTime.Parse("2002-03-11T22:18:09.8332679+00:00")
        },
        new UserModel{
            Id = 16,
            TeamId = null,
            FirstName = "Kathleen",
            SurName = "Carter",
            Email = "Kathleen12@hotmail.com",
            RegisteredAt = DateTime.Parse("2020-01-26T02:20:26.5992257+00:00"),
            BirthDay = DateTime.Parse("1959-03-08T11:26:07.7481837+00:00")
        },
        new UserModel{
            Id = 17,
            TeamId = null,
            FirstName = "Omar",
            SurName = "Cole",
            Email = "Omar_Cole@hotmail.com",
            RegisteredAt = DateTime.Parse("2019-07-03T11:26:16.6656402+00:00"),
            BirthDay = DateTime.Parse("1992-11-25T00:50:55.0278567+00:00")
        },
        new UserModel{
            Id = 18,
            TeamId = null,
            FirstName = "Hugh",
            SurName = "Rippin",
            Email = "Hugh.Rippin2@hotmail.com",
            RegisteredAt = DateTime.Parse("2018-05-08T17:21:41.9189355+00:00"),
            BirthDay = DateTime.Parse("1996-08-17T15:26:34.7350726+00:00")
        },
        new UserModel{
            Id = 19,
            TeamId = null,
            FirstName = "Howard",
            SurName = "Ankunding",
            Email = "Howard89@yahoo.com",
            RegisteredAt = DateTime.Parse("2018-10-13T00:00:09.3109532+00:00"),
            BirthDay = DateTime.Parse("1968-07-17T00:25:35.210783+00:00")
        }
             };
        #endregion
    }
}