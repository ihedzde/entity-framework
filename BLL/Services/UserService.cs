using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using Common.DTO.User;
using DAL.Domain.Repositories;
using DAL.Domain.Models;

namespace BLL.Services
{
    public class UserService
    {
        private readonly IRepository<UserModel> _userRepo;
        private readonly IMapper _mapper;
        public UserService(IMapper mapper, IRepository<UserModel> userRepo)
        {
            _userRepo = userRepo;
            _mapper = mapper;
        }
        public IEnumerable<UserDTO> GetAllUsers()
        {
            return _userRepo.ReadAll().Select(model => _mapper.Map<UserModel, UserDTO>(model));
        }
        public UserDTO GetById(int id){
            return _mapper.Map<UserModel, UserDTO>(_userRepo.Read(id));
        }
        public UserDTO CreateUser(CreateUserDTO createUserDTO)
        {
            return _mapper.Map<UserModel, UserDTO>(_userRepo.Create(_mapper.Map<UserDTO, UserModel>(_mapper.Map<CreateUserDTO, UserDTO>(createUserDTO))));
        }
        public void DeleteUser(int id){
            _userRepo.Delete(id);
        }
        public void UpdateUser(UserDTO updateUser)
        {
            _userRepo.Update(_mapper.Map<UserDTO, UserModel>(updateUser));
        }

    }
}